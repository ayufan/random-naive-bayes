#include "FoldedChain.h"
#include "FoldedMatrix.h"

#if defined(_MSC_VER) && (_MSC_VER >= 1400)
//visual studio 8.0 - konwersja pomiedzy unsigned int a size_t
#pragma warning(disable:4267)
//visual studio 8.0 - arytmetyka dla iteratorow przy konwersji na inta
#pragma warning(disable:4244)

#endif


using namespace std;
using namespace boost;

namespace faif {
    namespace dna {


        namespace {

            /** strategy to calculate FoldedMatrix, when one chain (single chain) is calculated */
            class SingleMatrixStrategy : public FoldedMatrixStrategy {
            public:
                SingleMatrixStrategy( const EnergyNucleo& energy, const Chain& chain )
					: FoldedMatrixStrategy(energy), chain_(chain) {}
                virtual ~SingleMatrixStrategy() {}

                /** return the iterator for nuleotide of i-th index in matrix */
                virtual Chain::const_iterator getNucleotide(int index) const {
                    Chain::const_iterator it = chain_.begin();
                    it += index;
                    return it;
                }

                /** returns the split index (the first index belonging to the second chain) */
                virtual int getSplitIndex() const { return chain_.getLength(); }

                /** returns the last valid index */
                virtual int getLength() const { return chain_.getLength(); }

            private:
                const Chain& chain_;
            };

        } //namespace

        FoldedChain::FoldedChain(const Chain& chain, const EnergyNucleo& energy, unsigned int max_foldings)
            : chain_(chain),
              energy_(energy),
              max_foldings_(max_foldings),
              strategy_( new SingleMatrixStrategy(energy_, chain_) ),
              proxy_(0L)
        { }

        FoldedChain::~FoldedChain(){}

        /** returns the collection of secondary structures; collection created after first call. */
        const SecStructures& FoldedChain::getStructures() const {
            return lazyCreate()->getStructures();
        }

        /** returns the single secondary structure (search in-depth the matrix) */
        SecStruct FoldedChain::findInDepth() const {
            return lazyCreate()->findInDepth();
        }

        /** returns the secondary structure energy (score), calculated on first call. */
        EnergyValue FoldedChain::getSecStructEnergy() const {
            return lazyCreate()->getSecStructEnergy();
        }

        /**print the matrix on ostream (to debug) */
        std::ostream& FoldedChain::printMatrix(std::ostream& os, int print_width) const {
            if( proxy_.get() != 0L)
                proxy_->printMatrix(os, print_width);
            else
                os << "energy matrix not created" << endl;
            return os;
        }

        /**print the secondary structures on stream (to debug) */
        std::ostream& FoldedChain::printStructures(std::ostream& os, int print_width) const {
            if( proxy_.get() != 0L)
                proxy_->printStructures(os, print_width);
            else
                os << "energy matrix not created" << endl;
            return os;
        }

        /** create the folded matrix in first call */
        FoldedChain::FoldedMatrixPtr& FoldedChain::lazyCreate() const {
            if( proxy_.get() == 0L )
                proxy_.reset( new FoldedMatrix(*strategy_,  max_foldings_ ) );
            return proxy_;
        }

		// ostream operator - print the chain and single secondary structure
		std::ostream& operator<<(std::ostream& os, const FoldedChain& folded) {
			os << folded.getChain().getString();
			os << folded.findInDepth();
			return os;
		}

    } //namespace dna
} //namespace faif
