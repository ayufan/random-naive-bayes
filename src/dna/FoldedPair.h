// DNA two single stranded chain with second structures
//  @author Robert Nowak

#ifndef FOLDED_PAIR_H
#define FOLDED_PAIR_H

#include "Chain.h"
#include "SecStruct.h"

#include <boost/scoped_ptr.hpp>

namespace faif {
    namespace dna {


		/** matrix with energy */
		class FoldedMatrix;
		/** startegy to calculate matrix */
		class FoldedMatrixStrategy;


        /** \brief Two DNA chains with secondary structures.

			Two DNA chains with secondary structures
			(created between them as well as the self-secondary structures)
		*/
        class FoldedPair : boost::noncopyable  {
        public:
            explicit FoldedPair(const Chain& first_chain, const Chain& second_chain,
								const EnergyNucleo& energy, unsigned int max_foldings = 100);
			~FoldedPair();

            /** accessor */
            const Chain& getFirstChain() const { return first_; }

            /** accessor */
            const Chain& getSecondChain() const { return second_; }

			/** the second structure energy. Calculated on the first time, when method is called. */
			EnergyValue getSecStructEnergy() const;

			/** single second structure, algorithm search graph in depth */
			SecStruct findInDepth() const;

            /** collection of second structures, algorithm search graph in width,
				the max_foldings parameters restricts the number of structures */
            const SecStructures& getStructures() const;

            /** print the matrix (for testing and debugging) */
            std::ostream& printMatrix(std::ostream& os, int print_width = 4) const;

			/** print the secondary structures (for testing and debugging) */
            std::ostream& printStructures(std::ostream& os, int print_width = 4) const;

			/** the support class - holding the matrix with energy and secondary structures */
			typedef boost::scoped_ptr<FoldedMatrix> FoldedMatrixPtr;
			/** the strategy to access for FoldedMatrix (one or two DNA chains) */
			typedef boost::scoped_ptr<FoldedMatrixStrategy> FoldedMatrixStrategyPtr;
        private:
            /** the analyzed DNA chains */
            Chain first_;
            Chain second_;
			/** the energy of nucleotide pairs */
			const EnergyNucleo& energy_;

			/** maximum number of returned second structures */
			unsigned int max_foldings_;

			/** strategy for FoldedMatrix to index two DNA chains */
			FoldedMatrixStrategyPtr strategy_;

			/** obiekt, ktory przechowuje obliczone wartosci */
			mutable FoldedMatrixPtr proxy_;

			/** creates the proxy if necessary */
			FoldedMatrixPtr& lazyCreate() const;
        };

        //print the chain sequences and one secondary structure
        std::ostream& operator<<(std::ostream& os, const FoldedPair& folded);

    } //namespace dna
} //namespace faif

#endif //FOLDER_PAIR_H
