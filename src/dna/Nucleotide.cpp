// implementacja klasy reprezentujacej pojedynczy nukleotyd

#include "Nucleotide.h"

namespace faif {
	namespace dna {

		/** produkuje nukleotyd, kt�ry jest komplementarny do danego */
		Nucleotide Nucleotide::complementary() const {

			switch(val_) {
			case ADENINE:
				return Nucleotide(THYMINE);
			case CYTOSINE:
				return Nucleotide(GUANINE);
			case GUANINE:
				return Nucleotide(CYTOSINE);
			case THYMINE:
				return Nucleotide(ADENINE);
			default:
				throw NucleotideBadCharException(static_cast<char>(val_) );
			}
		}

		//operator strumieniowy, nie zaprzyjazniony, korzysta z metody get()
		std::ostream& operator<<(std::ostream& os, const Nucleotide& n) {
			os << static_cast<char>(n.get());
			return os;
		}

		// tworzy nukleotyd na podstawie znaku
		Nucleotide create(const char& n) {
			switch(n) {
			case 'A':
			case 'a':
				return Nucleotide(ADENINE);
			case 'C':
			case 'c':
				return Nucleotide(CYTOSINE);
			case 'G':
			case 'g':
				return Nucleotide(GUANINE);
			case 'T':
			case 't':
				return Nucleotide(THYMINE);
			}
			throw NucleotideBadCharException(n);
		}

	} //namespace dna

} //namespace faif
