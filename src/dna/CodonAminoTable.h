// singleton , represents the genetic code (corresponding amino acid and codons)

#ifndef CODON_AMINO_TABLE_H
#define CODON_AMINO_TABLE_H

#include"Codon.h"

#include<set>
#include<map>

namespace faif {
    namespace dna {


		/** the enum represents amino */
		enum AminoAcid {
			PHENYLALANINE = 'F',
			LEUCINE = 'L',
			ISOLEUCINE = 'I',
			METHIONINE = 'M',
			VALINE = 'V',
			SERINE = 'S',
			PROLINE = 'P',
			THREONINE = 'T',
			ALANINE = 'A',
			TYROSINE = 'Y',
			STOP_CODON = '.',
			HISTIDYNE = 'H',
			GLUTAMINE = 'Q',
			ASPARAGINE = 'N',
			LYSINE = 'K',
			ASPARTIC = 'D',
			GLUTAMIC = 'E',
			CYSTEINE = 'C',
			TRYPTOPHAN = 'W',
			ARGININE = 'R',
			GLYCINE = 'G',
			UNKNOWN = '?'};

        /** \brief codons for given amino, amino for given codon, singleton

			The objects store the maps between codons and aminos.
		*/
        class CodonAminoTable {
        public:
            static CodonAminoTable& getInstance() {
                static CodonAminoTable instance;
                return instance;
            }

            /** return amino for given codon */
            AminoAcid getAmino(const Codon& codon) const;

            /** return set of codons for given amino (all codons)  */
            std::set<Codon> getCodons(AminoAcid amino) const;

            /** return the set of codons for given codon which are equivalent (codes the same amino) */
            std::set<Codon> getCodons(const Codon& codon) const;

        private:
			/** singleton, c-tor not allowed */
            CodonAminoTable();
			/** singleton, copy c-tor not allowed */
            CodonAminoTable(CodonAminoTable&);

            void makeInvertedMap();

            typedef std::map<Codon, AminoAcid> CodonMap;
            typedef std::multimap<AminoAcid, Codon> InvertedCodonMap;
            CodonMap codonMap_;
            InvertedCodonMap invertedCodonMap_;
        };

        /** the stream operator (support ) */
        std::ostream& operator<<(std::ostream& os, const AminoAcid& n);


    } //namespace dna
} //namespace faif

#endif
