#include <algorithm>
#include <iterator>
#include <cassert>
#include <limits>

#include "Chain.h"

using namespace std;

namespace faif {
	namespace dna {

        Chain::Chain(const string& seq) {
            for(string::const_iterator it = seq.begin(); it != seq.end(); ++it)
                operator+=( create(*it) );
        }

		/** copy constructor from range */
		Chain::Chain(const const_iterator& begin, const const_iterator& end) {
			copy( begin, end, back_inserter(nucleos_) );
		}


        Chain& Chain::operator=(const Chain& ch) {
            nucleos_=ch.nucleos_;
            return *this;
        }

        Chain& Chain::operator+=(const Nucleotide& val) {
            nucleos_.push_back(val);
			return *this;
        }

        Chain& Chain::operator+=(const Chain& ch) {
			copy(ch.nucleos_.begin(), ch.nucleos_.end(), back_insert_iterator<NucleotideChain>(nucleos_) );
			return *this;
        }

        string Chain::getString()const {
            string str;
            for(NucleotideChain::const_iterator it = nucleos_.begin(); it != nucleos_.end(); ++it) {
				str += static_cast<char>(it->get() );
            }
            return str;
        }

        Chain Chain::complementary() const {
            Chain complChain;
            for(NucleotideChain::const_reverse_iterator it = nucleos_.rbegin(); it != nucleos_.rend(); ++it)
				complChain += it->complementary();
            return complChain;
        }

		//stream operator (support for debugging)
		std::ostream& operator<<(std::ostream& os, const Chain& chain) {
			os << chain.getString();
			return os;
		}

		/** creator (support): sub-chain, using index).
			Returns the largest chain starting from given index and having length less or equal given length. */
		Chain createSubChain(const Chain& chain, int start, int length) {
			if(start >= chain.getLength() )
				start = chain.getLength();

			int finish = start + length;
			if( start + length >= chain.getLength() )
				finish = chain.getLength();

			Chain::const_iterator beg = chain.begin();
			beg += start;
			Chain::const_iterator end = chain.begin();
			end += finish;
			return Chain(beg, end);
		}

	}
} //namespace faif
