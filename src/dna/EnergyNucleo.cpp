//  EnergyNucleo class

#include "EnergyNucleo.h"

using namespace std;

namespace faif {
    namespace dna {

        /** \brief less operator for complementary pairs */
        bool lessComplementary::operator()(const Complementary& a, const Complementary& b) const {
            if(a.first == b.first )
                return a.second.get() < b.second.get();
            else
                return a.first.get() < b.first.get();
        }

        EnergyValue EnergyNucleo::getEnergy ( const faif::dna::Nucleotide& a, const faif::dna::Nucleotide& b ) const {

            EnergyMap::const_iterator it = data_.find( Complementary(a,b) );
            if( it != data_.end() )
                return it->second;
            else
                return defaultEnergy_;
        }

		/** add pair of nucleotides and its energy */
		void EnergyNucleo::addPair( const faif::dna::Nucleotide& a,
									const faif::dna::Nucleotide& b,
									const EnergyValue& val) {

			data_.insert( EnergyMap::value_type( Complementary(a,b), val ) );
		}


        /** default energy object: GC pair: energy = 3, AT pair: energy = 2, GT pair: energy = 1 */
        EnergyNucleo createDefaultEnergy(EnergyValue defaultEnergy) {

            EnergyNucleo energy(defaultEnergy);
            energy.addPair( Nucleotide(GUANINE),  Nucleotide(CYTOSINE), 3 );
			energy.addPair( Nucleotide(CYTOSINE), Nucleotide(GUANINE),  3 );
			energy.addPair( Nucleotide(ADENINE),  Nucleotide(THYMINE),  2 );
			energy.addPair( Nucleotide(THYMINE),  Nucleotide(ADENINE),  2 );
			energy.addPair( Nucleotide(GUANINE),  Nucleotide(THYMINE),  1 );
			energy.addPair( Nucleotide(THYMINE),  Nucleotide(GUANINE),  1 );
            return energy;
        }

    } //namespace dna
} //namespace faif

