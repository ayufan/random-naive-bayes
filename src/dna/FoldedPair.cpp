#include "FoldedPair.h"
#include "FoldedMatrix.h"

#if defined(_MSC_VER) && (_MSC_VER >= 1400)
//visual studio 8.0 - konwersja pomiedzy unsigned int a size_t
#pragma warning(disable:4267)
//visual studio 8.0 - arytmetyka dla iteratorow przy konwersji na inta
#pragma warning(disable:4244)

#endif

using namespace std;
using namespace boost;

namespace faif {
    namespace dna {

        namespace {

            /** strategy to calculate FoldedMatrix, when one chain (single chain) is calculated */
            class DoubleMatrixStrategy : public FoldedMatrixStrategy {
            public:
                DoubleMatrixStrategy( const EnergyNucleo& energy, const Chain& first, const Chain& second )
                    : FoldedMatrixStrategy(energy),
                      first_(first),
                      second_(second),
                      splitIdx_(first_.getLength() )
                {}

                virtual ~DoubleMatrixStrategy() {}

                /** return the iterator for nuleotide of i-th index in matrix */
                virtual Chain::const_iterator getNucleotide(int index) const {

                    if(index < splitIdx_) {
						Chain::const_iterator it = first_.begin();
						it += index;
						return it;
                    }
                    else {
                        index -= splitIdx_;
                        Chain::const_iterator it = second_.begin();
                        it += index;
                        return it;
                    }
                }

                /** returns the split index (the first index belonging to the second chain) */
                virtual int getSplitIndex() const { return splitIdx_; }

                /** returns the last valid index */
                virtual int getLength() const { return first_.getLength() + second_.getLength(); }
            private:
                const Chain& first_;
                const Chain& second_;
                int splitIdx_;
            };

        } //namespace


        FoldedPair::FoldedPair(const Chain& first_chain, const Chain& second_chain,
                               const EnergyNucleo& energy, unsigned int max_foldings)
            : first_(first_chain),
              second_(second_chain),
              energy_(energy),
              max_foldings_(max_foldings),
              strategy_( new DoubleMatrixStrategy(energy_, first_, second_) ),
              proxy_(0L)
        { }

        FoldedPair::~FoldedPair(){}

        /** Zwraca liste struktur drugorzedowych. Za pierwszym razem tworzy taka liste. */
        const SecStructures& FoldedPair::getStructures() const {
            return lazyCreate()->getStructures();
        }

        /** Zwraca pojedyncza strukture drugorzedowa */
        SecStruct FoldedPair::findInDepth() const {
            return lazyCreate()->findInDepth();
        }

        /** Zwraca energie struktury drugorzedowej. Za pierwszym razem oblicza energie. */
        EnergyValue FoldedPair::getSecStructEnergy() const {
            return lazyCreate()->getSecStructEnergy();
        }

        /**drukuje macierz na wskazany strumien (do testow) */
        std::ostream& FoldedPair::printMatrix(std::ostream& os, int print_width) const {
            if( proxy_.get() != 0L)
                proxy_->printMatrix(os, print_width);
            else
                os << "energy matrix not created" << endl;
            return os;
        }

        /**drukuje struktury na wskazany strumien (do testow) */
        std::ostream& FoldedPair::printStructures(std::ostream& os, int print_width) const {
            if( proxy_.get() != 0L)
                proxy_->printStructures(os, print_width);
            else
                os << "energy matrix not created" << endl;
            return os;
        }

        /** tworzy odpowiedni obiekt */
		FoldedPair::FoldedMatrixPtr& FoldedPair::lazyCreate() const {
            if( proxy_.get() == 0L )
                proxy_.reset( new FoldedMatrix(*strategy_, max_foldings_) );
            return proxy_;
        }

        //drukuje sekwencje oraz jedna (pierwsza) strukture drugorzedowa
        std::ostream& operator<<(std::ostream& os, const FoldedPair& folded) {
            os << folded.getFirstChain().getString() << endl;
            os << folded.getSecondChain().getString() << endl;
            os << folded.findInDepth();
            return os;
        }


    } //namespace dna
} //namespace faif
