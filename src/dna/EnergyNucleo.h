// singleton, pair of dna nucleotides energies
//  @author Robert Nowak

#ifndef ENERGY_NUCLEO_H
#define ENERGY_NUCLEO_H

#include <utility>
#include <map>

#include "Nucleotide.h"

namespace faif {
    namespace dna {
		/** the energy value */
		typedef int EnergyValue;

		/** the pair of nucleotides */
		typedef std::pair<faif::dna::Nucleotide, faif::dna::Nucleotide> Complementary;

		/** comparison of pair of nucleotides (support) */
		struct lessComplementary {
			bool operator()(const Complementary& a, const Complementary& b) const;
		};

		/** the default energy value */
		static const EnergyValue DEFAULT_NUCLEO_ENERGY = -1000;

		/** \brief the maps between pair of nucleotides and its energy

			Gives energy for two nucleotides, used f.e. in secondary structure calculation
		*/
		class EnergyNucleo
		{
		public:
			/** c-tor */
			explicit EnergyNucleo(EnergyValue defaultEnergy) : defaultEnergy_(defaultEnergy) {}
			/** copy c-tor */
			EnergyNucleo(const EnergyNucleo& energy) : data_(energy.data_), defaultEnergy_(energy.defaultEnergy_) {}
			~EnergyNucleo(){}
			/** \brief return the energy of given pair */
			EnergyValue getEnergy ( const faif::dna::Nucleotide& a, const faif::dna::Nucleotide& b ) const;

			/** \brief stores the pair and its energy */
			void addPair( const faif::dna::Nucleotide& a, const faif::dna::Nucleotide& b, const EnergyValue& val);
		private:
			typedef std::map<Complementary, EnergyValue, lessComplementary > EnergyMap;
			EnergyMap data_;

			// default energy - returned when pair is not found
			const EnergyValue defaultEnergy_;

			// assignment not allowed
			EnergyNucleo& operator=(const EnergyNucleo& energy);
		};

        /** default energy object: GC pair: energy = 3, AT pair: energy = 2, GT pair: energy = 1,
		 otherwise energy = -1000 */
		EnergyNucleo createDefaultEnergy(EnergyValue defaultEnergy = DEFAULT_NUCLEO_ENERGY);

    } //namespace dna
} //namespace faif


#endif //ENERGY_NUCLEO
