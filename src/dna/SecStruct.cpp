// implementacja klas i metod dotyczacych struktur drugorzedowych

#include "SecStruct.h"


#include <numeric>
#include <iterator>
#include <boost/bind.hpp>
#include <boost/noncopyable.hpp>

#if defined(_MSC_VER) && (_MSC_VER >= 1400)
//visual studio 8.0 - arytmetyka dla iteratorow przy konwersji na inta
#pragma warning(disable:4244)
#endif

using namespace std;

namespace faif {
    namespace dna {

        //funkcja pomocnicza - operator strumieniowy
        std::ostream& operator<<(std::ostream& os, const ConnectPair& p) {
            os << '(' << p.first - p.first.getChain().begin()
               << ',' << p.second - p.second.getChain().begin() << ')';
            return os;
        }

        void SecStruct::append(const SecStruct& s) {
            foldings_.insert(s.foldings_.begin(), s.foldings_.end() );
        }

        namespace {
            //funktor pomocniczy - zwraca energie pojedynczego wiazania
			struct EnergyFunctor  {
                EnergyFunctor(const EnergyNucleo& energy_matrix) : energy_(energy_matrix) {}
                EnergyValue operator()(EnergyValue sum, const ConnectPair& p) {
                    return sum + energy_.getEnergy( *(p.first), *(p.second) );
                }
                const EnergyNucleo& energy_;
			private:
				//zabroniony operator= (warning w msvc)
				EnergyFunctor& operator=(const EnergyFunctor&);
            };
        } //namespace

        EnergyValue SecStruct::energy(const EnergyNucleo& energy_matrix) const {
            EnergyFunctor functor(energy_matrix);
            return accumulate( foldings_.begin(),  foldings_.end(), 0, functor );
        }

        //drukuje strukture drugorzedowa
        std::ostream& operator<<(std::ostream& os,const SecStruct& sec_struct) {
            copy(sec_struct.foldings_.begin(), sec_struct.foldings_.end(), ostream_iterator<ConnectPair>(os,"") );
            return os;
        }


    } //namespace
} //namespace

