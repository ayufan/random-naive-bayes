#ifndef FOLDED_MATRIX_H
#define FOLDED_MATRIX_H

/* the classes and functions to calculate and store energy matrix (in Nussinov algorithm).
   The classes and function for internal use, by FoldedChain.h and FoldedPair.h (are not the library interface).
*/

#include "Chain.h"
#include "SecStruct.h"

#include <algorithm>
#include <boost/noncopyable.hpp>
#include <boost/scoped_ptr.hpp>

namespace faif {
    namespace dna {

        /** \brief matrix - 2D array with indexing */
        class Matrix : boost::noncopyable {
        public:
			Matrix(unsigned int length) : len_(length) {
                unsigned int size_ = len_*len_;
                m_ = new EnergyValue[size_];
				std::fill(m_, m_ + size_, 0);
				// for(unsigned int i=0;i<size_;++i) m_[i] = 0;
            }
            ~Matrix() { delete [] m_; }
            const EnergyValue& get(int x, int y) const{ return m_[x*len_+y]; }
            void set(int x, int y, EnergyValue val) { m_[x*len_ + y] = val; }
            unsigned int getLength() const { return len_; }
        private:
            unsigned int len_;
            EnergyValue* m_;
        };

		class SecStructProxy;
        typedef boost::scoped_ptr<SecStructProxy> SecStructProxyPtr;

		/** strategy to use folded matrix algorithm with single chain and with two chains */
		class FoldedMatrixStrategy {
		public:
			FoldedMatrixStrategy(const EnergyNucleo& energy) : energy_(energy) {}
			virtual ~FoldedMatrixStrategy() {}

			/** return the iterator for nuleotide of i-th index in matrix */
			virtual Chain::const_iterator getNucleotide(int index) const = 0;

			/** returns the split index (the first index belonging to the second chain) */
			virtual int getSplitIndex() const = 0;

			/** returns the number of nucleotides in chain (chains) */
			virtual int getLength() const = 0;

			/** calculates energy between two nucleotides of given indexes */
			int getEnergy(int index_A, int index_B) const {
				return energy_.getEnergy( *getNucleotide(index_A), *getNucleotide(index_B) );
			}
		private:
			FoldedMatrixStrategy& operator=(const FoldedMatrixStrategy&); //!< private, reference members
			const EnergyNucleo& energy_;
		};

        /** dna chain or two dna chains with energy matrix */
        class FoldedMatrix : boost::noncopyable {
        public:
			// c-tor for single strand
            FoldedMatrix(const FoldedMatrixStrategy& strategy, unsigned int max_foldings);

			~FoldedMatrix();

			/** calculates secondary structures (backtracking in energy matrix) */
            const SecStructures& getStructures();

            EnergyValue getSecStructEnergy() { return structure_energy_; }

			/** calculates one secondary structure (energy matrix view in depth) */
            SecStruct findInDepth() const;

            std::ostream& printMatrix(std::ostream& os, int print_width) const ;

            std::ostream& printStructures(std::ostream& os, int print_width) const;
        private:
			const FoldedMatrixStrategy& strategy_;
            unsigned int max_foldings_;

            Matrix matrix_;
            /** secondary structure energy */
            EnergyValue structure_energy_;
            SecStructProxyPtr proxy_;

            void makeMatrix();

            //SecStruct& findDepthRecur(SecStruct& structure, const ConnectPair& curr_point) const;
            SecStruct& findDepthRecur(SecStruct& structure, int first_idx, int second_idx ) const;
        };

	} //namespace dna
} //namespace faif

#endif //FOLDED_MATRIX_H
