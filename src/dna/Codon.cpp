#include "Codon.h"
#include "ExceptionsDna.h"

namespace faif {
	namespace dna {
		Codon::Codon(const std::string& str) {
			if (str.size() < 3)
				throw CodonStringTooShortException();
			get<0>() = create(str[0]);
			get<1>() = create(str[1]);
			get<2>() = create(str[2]);
		}


        std::ostream& operator<<(std::ostream& os, const Codon& codon) {
			os << "(" << codon.getFirst() << "," << codon.getSecond() << "," << codon.getThird() << ")";
			return os;
		}

	} //namespace dna
} //namespace faif


