#include "VectorIndividual.hpp"
#include "../utils/Random.h"

namespace faif {
    namespace search {

		/** generation random value for boolean variable (uniform distribution) */
		BooleanGene::value_type BooleanGene::generateRandom() {
			RandomDouble random;
			return random() < 0.5;
		}

		/** the mutation operator for boolean variable  */
		BooleanGene::value_type BooleanGene::mutate(double prob_mutation, const value_type& val) {
			RandomDouble random;
			if( random() < prob_mutation ) {
				return !val;
			}
			else {
				return val;
			}
		}

	} //namespace search
} //namespace faif
