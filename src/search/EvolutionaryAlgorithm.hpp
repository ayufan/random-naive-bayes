#ifndef FAIF_SEARCH_EA_H
#define FAIF_SEARCH_EA_H

//
//file with Evolutionary Algorithm implementation
//

#include <iterator>
#include <algorithm>
#include <vector>
#include <boost/bind.hpp>
#include <boost/concept_check.hpp>

#include "Space.hpp"

namespace faif {
    namespace search {

        /** \brief the typedef-s for space for evolutionary algorithm, where the population is a vector of individuals,
			and the fitness is the double */
        template<typename Ind> struct EvolutionaryAlgorithmSpace : public Space<Ind> {
            typedef std::vector<Ind> Population;
        };

		/** \brief the concept for evolutionary algorithm space
		*/
        template<typename Space>
		struct EvolutionaryAlgorithmSpaceConcept {
			typedef typename Space::Individual Individual;
			typedef typename Space::Population Population;
			typedef typename Space::Fitness Fitness;

			BOOST_CONCEPT_USAGE(EvolutionaryAlgorithmSpaceConcept)
			{
				//the method to generate the fitness is required
				Space::fitness(ind);
			}
			Individual ind;
		};

		/** \brief the concept for evolutionary algorithm space
		*/
        template<typename Space>
		struct EvolutionaryAlgorithmSpaceWithMutationConcept : public EvolutionaryAlgorithmSpaceConcept<Space> {
			typedef typename Space::Individual Individual;
			typedef typename Space::Population Population;
			typedef typename Space::Fitness Fitness;

			BOOST_CONCEPT_USAGE(EvolutionaryAlgorithmSpaceWithMutationConcept)
			{
				//the method to generate the mutation of individual is required
				Space::mutation(ind);
			}
			Individual ind;
		};

        /** \brief mutation policy - no mutation */
        template<typename Space> struct MutationNone {

            /** \brief mutation is empty operation */
            static typename Space::Individual& mutation(typename Space::Individual& ind) {
                return ind;
            }
        };

        /** \brief mutation policy - mutation from Space
         */
        template<typename Space> struct MutationCustom {

			//the methods form mutation are required
			BOOST_CONCEPT_ASSERT((EvolutionaryAlgorithmSpaceWithMutationConcept<Space>));

            /** \brief calls the mutation function from Space */
            static typename Space::Individual& mutation(typename Space::Individual& ind) {
                return Space::mutation(ind);
            }
        };

        /** \brief succession and selection policy - the n-th best individuals survive

         */
        template<typename Space> struct SelectionRanking {

            /** \brief sort (partial) the population and removes the worst elements.
                Population is returned having n elements. The function 'Space::compare_individuals' is used.
            */
            static typename Space::Population& selection(typename Space::Population& population, int size) {

                typename Space::Population::iterator middle = population.begin();
                std::advance(middle, size);

                //the n-th best elements is moved to the beginning of contener
                std::nth_element(population.begin(), middle, population.end(),
                                 boost::bind(Space::fitness, _1) > boost::bind(Space::fitness, _2) );

                population.erase( middle, population.end() );
                return population;
            }
        };


        /** \brief the evolutionary algorithm

            \param Mutation - MutationNone, MutationCustom
            \param Selection - SelectionRanking
			\param StopCondition - StopAfterNSteps
        */
        template<  typename Space,
                   template <typename> class Mutation = MutationNone,
                   template <typename> class Selection = SelectionRanking,
				   typename StopCondition = StopAfterNSteps<100>
                   >
        class EvolutionaryAlgorithm {
        public:
            typedef typename Space::Individual Individual;
            typedef typename Space::Population Population;
			typedef typename Space::Fitness Fitness;

			//the methods for fitness calculation are required
			BOOST_CONCEPT_ASSERT((EvolutionaryAlgorithmSpaceConcept<Space>));

            EvolutionaryAlgorithm( ) { }

            /** \brief the evolutionary algorithm - until stop repeat mutation, cross-over, selection, succession.
                Modifies the initial population.
            */
            Individual& solve(Population& init_population, StopCondition stop = StopCondition() ) {

                int pop_size = init_population.size();
                Population& current = init_population;
				//StopCondition stop;

				do {
                    // std::cout << "population " << i << std::endl;
                    // std::copy( current.begin(), current.end(), std::ostream_iterator<Individual>(std::cout,"\n") );
                    Population old_population(current);
                    //mutation
                    std::transform( current.begin(), current.end(), current.begin(),
                                    boost::bind( &Mutation<Space>::mutation, _1 ) );
                    //join the old and the current population
                    std::copy(old_population.begin(), old_population.end(), back_inserter(current) );
                    //selection on join populations
                    Selection<Space>::selection(current, pop_size);
					stop.update(current);
                }
				while(! stop.isFinished() );

                typename Population::iterator best =
                    std::max_element(current.begin(), current.end(),
                                     boost::bind(Space::fitness, _1) > boost::bind(Space::fitness, _2) );
                return *best;
            }
        };

	} //namespace search
} //namespace faif

#endif // FAIF_SEARCH_EA_H
