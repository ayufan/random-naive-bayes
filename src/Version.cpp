#include "Version.h"

namespace faif {

	//! \brief major version of library
	int getVersionMajor() {
#ifndef FAIF_VER_MAJOR
#define FAIF_VER_MAJOR 0
#endif
        return FAIF_VER_MAJOR;

	}

	//! \brief minor version of library
	int getVersionMinor() {
#ifndef FAIF_VER_MINOR
#define FAIF_VER_MINOR 0
#endif
        return FAIF_VER_MINOR;

	}

	//! \brief compilation version of library
	int getVersionCompilation() {
#ifndef FAIF_VER_COMPILATION
#define FAIF_VER_COMPILATION 0
#endif
        return FAIF_VER_COMPILATION;

	}


}
