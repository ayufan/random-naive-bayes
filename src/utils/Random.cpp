// implementacja generatora liczb losowych (ktory jest fasada na boost::random)

#if defined(_MSC_VER) && (_MSC_VER >= 1400)
//msvc9.0 generuje smieci dla boost/random
#pragma warning(disable:4512)
#endif

#include "Random.h"
#include <ctime>
#include <algorithm>

using namespace std;

namespace faif {

    namespace {

		/** \brief the singleton, synchronized proxy to boost::Random */
        class RandomSingleton {
        public:
            static RandomSingleton& getInstance() {
                if(!pInstance_) {
                    boost::mutex::scoped_lock scoped_lock(createMutex_);
                    if(!pInstance_) {
                        static RandomSingleton singleton;
                        pInstance_ = &singleton;
                    }
                }
                return *pInstance_;
            }
			//accessor - mutex, access to generator
			boost::mutex& getAccess(){ return access_; }

			//accessor - random generator from boost
			boost::mt19937& getRng(){ return rng_;}
		private:
			//private constructor
			RandomSingleton() {
				//init the random generator
				rng_.seed( static_cast<unsigned int>( time( 0L ) ) );
			}
			//noncopyable
			RandomSingleton(const RandomSingleton&);
			//noncopyable
			RandomSingleton& operator=(const RandomSingleton&);
			//private destructor
			~RandomSingleton() {
				pInstance_ = 0L;
			}


			//mutex, access to generator
			boost::mutex access_;

			//random generator from boost
			boost::mt19937 rng_;

			//the singleton instance
			static RandomSingleton* pInstance_;
			//the singleton creation mutex
			static boost::mutex createMutex_;
		};

		//the static pointer to singleton instance
		RandomSingleton* RandomSingleton::pInstance_ = 0L;

		//the static mutex
		boost::mutex RandomSingleton::createMutex_;

	} //namespace

	RandomDouble::RandomDouble()
		: access_(RandomSingleton::getInstance().getAccess() ),
		  gen_(RandomSingleton::getInstance().getRng(),
			   boost::uniform_real<double>(0.0, 1.0) )
	{ }

	/** \brief the c-tor random variable generator in range <min,max), uniform distribution */
	RandomDouble::RandomDouble(double min_val, double max_val)
		: access_(RandomSingleton::getInstance().getAccess() ),
		  gen_(RandomSingleton::getInstance().getRng(),
			   boost::uniform_real<double>(min(min_val, max_val), max(min_val,max_val) ) )
	{ }

	/** \brief the c-tor random variable generator in range <0,1), uniform distribution */
	RandomDouble::RandomDouble(const RandomDouble& r)
		: access_(r.access_), gen_(r.gen_)
	{ }

	/** the method to generate the random variable in range <0,1), uniform distribution */
	double RandomDouble::operator()() {
		boost::mutex::scoped_lock scoped_lock(access_);
		return gen_();
	}

	RandomInt::RandomInt(int min, int max)
		: access_(RandomSingleton::getInstance().getAccess() ),
		  gen_( RandomSingleton::getInstance().getRng(), boost::uniform_int<int>(min, max) )
	{ }

	RandomInt::RandomInt(const RandomInt& r)
		: access_(r.access_), gen_(r.gen_)
	{ }


	/**  the method to generate the random variable in range <min, max>, uniform distribution */
	int RandomInt::operator()() {
		boost::mutex::scoped_lock scoped_lock(access_);
		return gen_();
		//		return uni_(rng_);
	}


	/** \brief the c-tor random variable generator, normal distribution */
	RandomNormal::RandomNormal(double mi, double sigma)
		: access_(RandomSingleton::getInstance().getAccess() ),
		  gen_( RandomSingleton::getInstance().getRng(), boost::normal_distribution<double>(mi, sigma) )
	{}

	/** \brief copy c-tor */
	RandomNormal::RandomNormal(const RandomNormal& r)
		: access_(r.access_), gen_(r.gen_)
	{}

	/** \brief the method to generate the random variable with normal distribution */
	double RandomNormal::operator()() {
		boost::mutex::scoped_lock scoped_lock(access_);
		return gen_();
	}

} //namespace faif
