#ifndef FAIF_RANDOM_H
#define FAIF_RANDOM_H

// the random generators based on boost::random ( mt19937),
// the generator is a synchronized (boost::mutex) singleton

#if defined(_MSC_VER) && (_MSC_VER >= 1400)
//msvc9.0 generuje smieci dla boost/date_time
#pragma warning(disable:4244)
//msvc9.0 generuje smieci dla boost/random
#pragma warning(disable:4512)

#endif

#include <boost/random.hpp>
#include <boost/thread/mutex.hpp>

namespace faif {

    /** \brief the uniform distribution for double, in given range, e.g. <0,1), uses RandomSingleton */
    class RandomDouble  {
    public:
		/** \brief the c-tor random variable generator in range <0,1), uniform distribution */
        explicit RandomDouble();
		/** \brief the c-tor random variable generator in range <min,max), uniform distribution */
		explicit RandomDouble(double min_val, double max_val);
		RandomDouble(const RandomDouble&);
        ~RandomDouble(){}
		/** \brief the method to generate the random variable in given range, uniform distribution */
        double operator()();
    private:
		typedef boost::variate_generator<boost::mt19937&, boost::uniform_real<double> > Generator;

		RandomDouble& operator=(const RandomDouble&); //!< assignment not allowed
        boost::mutex& access_;
		Generator gen_;
    };

    /** \brief the uniform distribution for int, in range <min,max>, uses RandomSingleton */
    class RandomInt {
	public:
        explicit RandomInt(int min, int max);
		RandomInt(const RandomInt&);
        ~RandomInt(){}

		/** \brief the method to generate the random variable in range <min, max>, uniform distribution */
        int operator()();
    private:
		typedef boost::variate_generator<boost::mt19937&, boost::uniform_int<int> > Generator;

		RandomInt& operator=(const RandomInt&); //!< assignment not allowed
        boost::mutex& access_;
		Generator gen_;
    };

    /** \brief the normal distribution for double, for given mean (mi) and standard deviation (sigma),
		uses RandomSingleton
	*/
    class RandomNormal  {
    public:
		/** \brief the c-tor random variable generator, normal distribution */
        explicit RandomNormal(double mi, double sigma);
		RandomNormal(const RandomNormal&);
        ~RandomNormal(){}
		/** \brief the method to generate the random variable with normal distribution */
        double operator()();
    private:
		typedef boost::variate_generator<boost::mt19937&, boost::normal_distribution<double> > Generator;

		RandomDouble& operator=(const RandomNormal&); //!< assignment not allowed
        boost::mutex& access_;
		Generator gen_;
    };


} //namespace faif

#endif //FAIF_RANDOM_H
