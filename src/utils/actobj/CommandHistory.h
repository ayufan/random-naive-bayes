#ifndef FAIF_COMMAND_HISTORY_H
#define FAIF_COMMAND_HISTORY_H

#include <map>

#include <boost/noncopyable.hpp>

#include "Command.h"

namespace faif {
    namespace actobj {
		/** the collection of commands, it can find the command by Id to give the command status
			or progress etc. It is synchronized associative memory (std::map)
		*/
		class CommandHistory : boost::noncopyable {
		public:
			CommandHistory() {}
			~CommandHistory() {}

			/** add the command to collection with given ID  */
			void insert(CommandID id, PCommand cmd);

			/** try to find the Command of CommandId. If there is no command it returns the null pointer */
			PCommand find(CommandID id) const;

			/** clear the CommandHistory */
			void clear();
		private:
			mutable boost::mutex m_;
			std::map<CommandID, PCommand> history_;

		};

		/** \brief find the command descriptor of given ID.
			If the command is not found in history returns default Descriptor (id=0, status=NONE)
		*/
		CommandDesc findCommandDescriptor(const CommandHistory& history, CommandID id);

		/** helpers, first executes in Scheduler and insterts cmd to CommandHistory */
		extern CommandID executeAsynchronouslyAndRemember(CommandHistory& history, PCommand cmd);
		/** helpers, first executes in Scheduler and insterts cmd to CommandHistory */
		extern CommandID executeSynchronouslyAndRemember(CommandHistory& history, PCommand cmd);
	} //namespace actobj
} //namespace faif

#endif //FAIF_COMMAND_HISTORY_H
