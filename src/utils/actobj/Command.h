#ifndef FAIF_COMMAND_H
#define FAIF_COMMAND_H

/*
  base classes for command
*/

#if defined(_MSC_VER) && (_MSC_VER >= 1400)
//msvc9.0 garbage warnings for boost::date_time
#pragma warning(disable:4244)
#pragma warning(disable:4512)
//msvc9.0 garbage warnings for throw specification
#pragma warning(disable:4290)
#endif


#include <boost/noncopyable.hpp>
#include <boost/intrusive_ptr.hpp>
#include <boost/thread.hpp>
#include "../../ExceptionsFaif.h"

#include "Progress.h"

namespace faif {
    namespace actobj {
		/* exception thrown when request to break the execution of command
		   e.g. after calling the 'cmd->halt' */
		class UserBreakException : public FaifException {
		public:
			UserBreakException(){}
			virtual ~UserBreakException() throw() {}
			virtual const char *what() const throw(){ return "UserBreak exception"; }
		};

		/** forward declaration */
		class Scheduler;

		/** The basic class of command, which is executed by scheduler. */
		class Command : boost::noncopyable {
		public:
			Command();
			/** destructor */
			virtual ~Command() { }

			/** the method called by scheduler. It calls the operator() of command. */
			void execute();

			/** returns the command actual state and progress  */
			CommandDesc getDescriptor() const;

			/** mutator - change the command progress (called by Progress::setProgress() )*/
			void setProgress(double new_progress);

			/** mutator - change the command state (called by Scheduler ) */
			void setState(CommandDesc::State new_state);

			/** mutator - change the commmand id (called by Scheduler ) */
			void setId(long new_id);

			/** Suggest the command to break execution (ie sets the flags) */
			void halt();

			/** Check the halt flag and throws the exception if set */
			void checkHaltFlag() const throw (UserBreakException);

			/** the accessor to the counter */
			int getCounter() const { return counter_; }

			/** add observer (add observer to command) */
			void attach(PCommandObserver observer) { progress_.attach(observer); }

			/** for intrusive_ptr */
			friend void intrusive_ptr_add_ref(Command* ptr);
			friend void intrusive_ptr_release(Command* ptr);
		protected:
			mutable boost::mutex m_;
		private:
			/** the method which implements the specific task of command.
				The private members can be overlapped */
			virtual void operator()(Progress& p) = 0;

			CommandDesc descriptor_; //!< the command state, progress and id

			int counter_; /** counter for intrusive ptr */
			volatile bool haltFlag_; //!< flag set if the command should stop its execution

			Progress progress_; //!< progress for given command
		};

		/** read the command state (helper) */
		extern CommandDesc::State getState(const Command& cmd);

		extern void intrusive_ptr_add_ref(Command* ptr);
		extern void intrusive_ptr_release(Command* ptr);

		/** the smart pointer to command.
			intrusive_ptr because of multithread env (mutex blocks the counter )
		*/
		typedef boost::intrusive_ptr<Command> PCommand;


		/**
		   \brief holder with the type and pointer to command as well as the base class of command (smart pointer)
		*/
		template<typename T> class CommandHolder {
		public:
			//! the command type
			typedef T CommandType;

			CommandHolder(T* cmd) : obsCmd_(cmd), cmd_(cmd) { }

			T* getObsCmd() { return obsCmd_; }
			PCommand get() { return cmd_; }
		private:
			T* obsCmd_;
			PCommand cmd_;
		};

	} //namespace actobj
} //namespace faif

#endif
