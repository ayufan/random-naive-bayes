#ifndef FAIF_SHEDULER_H
#define FAIF_SHEDULER_H

#include <boost/scoped_ptr.hpp>

#include "Command.h"

namespace faif {
    namespace actobj {
		/**
		 *  Sheduler, the singleton containing the execution queue, and thread pool.
		 *  it executes commands (synchronically or asynchronically)
		 */
		class Scheduler
		{
		public:
			/**
			   singleton
			*/
			static Scheduler& getInstance();

			/**
			   synchronically executes the command. Immediately executes the command
			   in the calling thread context, so after return the flag 'done' is always set to true
			   \return  CommandId of command
			*/
			CommandID executeSynchronously(PCommand cmd);

			/**
			   asynchronically executes the command. Do not break execution of the calling thread.
			   \return  CommandId of command
			*/
			CommandID executeAsynchronously(PCommand cmd);

			/**
			   asynchronically executes the command. Command is inserted into command queue,
			   it waits till free working thread is found, then it is executed.
			   The calling thread waits (on condition variable) untill the command is not finished.
			   \return  CommandId of command
			*/
			CommandID executeAsynchronouslyAndWait(PCommand cmd);


			/** signal to finish all threads after current command */
			void finishAll();

			/** wait for ending threads in pool */
			void join();
		private:
			struct Impl;
			boost::scoped_ptr<Impl> impl_;

			Scheduler(int num_thrd); //the constructor
			Scheduler(const Scheduler&); //noncopyable
			Scheduler& operator=(const Scheduler&); //noncopyable
			~Scheduler();
		};
	} //namespace actobj
}
#endif

