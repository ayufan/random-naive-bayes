#if defined(_MSC_VER) && (_MSC_VER >= 1400)
//msvc9.0 warning about assignment operator
#pragma warning(disable:4512)
#endif

#include <cassert>
#include <algorithm>
#include <boost/ref.hpp>
#include <boost/bind.hpp>

#include "Progress.h"
#include "Command.h"

using namespace std;
using namespace boost;

namespace faif {
    namespace actobj {
		//! creates sub-progress, the notification of the same observers,
		// but the output progress is recalulated (0% is the 'st', 100% is the 'fi')
		Progress::Progress( const Progress& parent, double st, double fi)
			: command_(parent.command_), observers_(parent.observers_)
		{
			double wsp = parent.finish_ - parent.start_;
			start_ = parent.start_ + st * wsp;
			finish_ = parent.start_ + fi * wsp;

			assert( start_ >= 0 && start_ <= 1 && finish_ >= 0 && finish_ <= 1);
			assert( start_ <= finish_);
		}

		/* \brief notify all observers about the progress.
		   The parameter current from 0 to 1 (100%) is recalculated to notify about progress from 'start' to 'finish'.
		*/
		void Progress::setProgress( double current ) {
			command_.checkHaltFlag();
			command_.setProgress( start_ + (finish_ - start_)*current );
			for_each( observers_.begin(), observers_.end(),
					  bind(&CommandObserver::notifyProgress, _1, cref(command_), start_ + (finish_ - start_)*current ) );
		}

		//! notify all observers, increase the progress
		void Progress::step() {
			command_.checkHaltFlag();
			for_each( observers_.begin(), observers_.end(),
					  bind(&CommandObserver::notifyStep, _1, cref(command_) ) );
		}

		//! notify all observers, change command state
		void Progress::changeState(CommandDesc::State state) {
			for_each( observers_.begin(), observers_.end(),
					  bind(&CommandObserver::notifyState, _1, cref(command_), state ) );
		}
	} //namespace actobj
} //namespace faif
