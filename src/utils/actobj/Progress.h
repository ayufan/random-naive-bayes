#ifndef FAIF_COMMAND_PROGRESS_H
#define FAIF_COMMAND_PROGRESS_H

/*
  module with progress object (used inside commands) and progress observers
*/

#include <cassert>
#include <vector>
#include <boost/smart_ptr.hpp>

#include "CommandDesc.h"

namespace faif {
    namespace actobj {
		/*! \brief
		  class to set the progress of given command. Subject in Obverver design pattern.
		  It also check the 'halt-command' flag and if it is set it throws exception UserBreakException.
		*/
		class Progress {
		public:
			//constructor, progress from 0 to 100% for given command
        Progress( Command& cmd ) : command_(cmd), start_(0), finish_(1)
			{
				assert( start_ >= 0 && start_ <= 1 && finish_ >= 0 && finish_ <= 1);
				assert( start_ <= finish_);
			}
			//copy constructor
        Progress( const Progress& p )
            : command_(p.command_), observers_(p.observers_), start_(p.start_), finish_(p.finish_) {}

			/** creates sub-progress, the notification of the same observers,
				but the output progress is recalulated (0% is the 'st', 100% is the 'fi') */
			Progress( const Progress& parent, double st, double fi);

			/* \brief change the command progress (stored in Command::CommandDescription)
			   Notify all observers about the progress.
			   The parameter current from 0 to 1 (100%) is recalculated to notify about progress from 'start' to 'finish'.
			*/
			void setProgress( double current );

			//! notify all observers, increase the progress
			void step();

			/* \brief notify all observers about the new state of command
			 */
			void changeState( CommandDesc::State state );

			//! add observer
			void attach(PCommandObserver observer) { observers_.push_back(observer); }
		private:
			Progress& operator=(const Progress& prog);      //<! assign not allowed

			Command& command_; //!< command for which the progress is calculated

			std::vector<PCommandObserver> observers_; //<! observers

			double start_;   //<! starting progress
			double finish_;  //<! finishing progress
		};
	} //namespace actobj
} // namespace faif


#endif // FAIF_COMMAND_PROGRESS_H */

