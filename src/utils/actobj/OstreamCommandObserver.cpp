#include "OstreamCommandObserver.h"

using namespace std;
using namespace boost;

namespace faif {
    namespace actobj {

		/** new progress */
		void OstreamCommandObserver::notifyProgress(const Command&, double progress) {
			boost::mutex::scoped_lock scoped_lock(out_mutex);
			out_ << static_cast<int>(progress * 100) << '%' << std::endl;
		}
        /** next step */
		void OstreamCommandObserver::notifyStep(const Command&) {
			boost::mutex::scoped_lock scoped_lock(out_mutex);
			out_ << '.' << std::flush;
		}
        /** change command state */
		void OstreamCommandObserver::notifyState(const Command&, CommandDesc::State) { }
	} //namespace actobj
} //namespace faif
