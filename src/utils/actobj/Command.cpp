#include "Command.h"

#if defined(_MSC_VER) && (_MSC_VER >= 1400)
//msvc9.0 warning about *this
#pragma warning(disable:4355)
#endif

using namespace std;
using namespace boost;

namespace faif
{
    namespace actobj {
		Command::Command() : counter_(0), haltFlag_(false), progress_(*this)
		{ }

		/** the method called by scheduler. It calls the operator() of command. */
		void Command::execute() {
			try {
				setState(CommandDesc::PENDING);
				operator()(progress_);
				setState(CommandDesc::DONE);
			} catch(UserBreakException&) {
				setState(CommandDesc::INTERRUPTED);
			} catch(...) {
				setState(CommandDesc::EXCEPTION);
			}
		}


		/** returns the command actual state */
		CommandDesc Command::getDescriptor() const {
			mutex::scoped_lock lock(m_);
			return CommandDesc(descriptor_);
		}

		/** mutator - change the command state */
		void Command::setState(CommandDesc::State s) {
			{
				mutex::scoped_lock lock(m_);
				descriptor_.state_ = s;
			}
			progress_.changeState( s );
		}

		/** mutator - change the command progress */
		void Command::setProgress(double new_progress) {
			mutex::scoped_lock lock(m_);
			descriptor_.progress_ = new_progress;
		}

		/** mutator - change the commmand id */
		void Command::setId(long new_id) {
			mutex::scoped_lock lock(m_);
			descriptor_.id_ = new_id;
		}

		/** Suggest the command to break execution (ie sets the flags) */
		void Command::halt() {
			haltFlag_ = true;
		}

		/** Check the halt flag and throws the exception if set */
		void Command::checkHaltFlag() const throw (UserBreakException) {
			if(haltFlag_) throw UserBreakException();
		}

		/** read the command state (helper) */
		CommandDesc::State getState(const Command& cmd) {
			return cmd.getDescriptor().state_;
		}

		void intrusive_ptr_add_ref(Command* cmd) {
			mutex::scoped_lock lock(cmd->m_); //critical section
			++(cmd->counter_);
		}

		void intrusive_ptr_release(Command* cmd) {
			bool del = false;
			{
				mutex::scoped_lock lock(cmd->m_); //critical section
				del = ! --(cmd->counter_);
			}
			if(del)
				delete cmd;
		}

	} //namespace actobj
} //namespace faif


