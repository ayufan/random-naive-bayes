#include "CommandHistory.h"
#include "Scheduler.h"

using namespace std;
using namespace boost;

namespace faif {
    namespace actobj {
		/** add the command to collection with given ID  */
		void CommandHistory::insert(CommandID id, PCommand cmd) {
			mutex::scoped_lock lock(m_);
			history_.insert( make_pair( id, cmd ) );
		}

		/** try to find the Command of CommandId. If there is no command it returns the null pointer */
		PCommand CommandHistory::find(CommandID id) const {
			mutex::scoped_lock lock(m_);
			std::map<CommandID, PCommand>::const_iterator i = history_.find(id);
			if(i != history_.end() )
				return i->second;
			else
				return PCommand(0L);
		}

		/** clear the CommandHistory */
		void CommandHistory::clear() {
			history_.clear();
		}

		/** \brief find the command descriptor of given ID.
			If the command is not found in history returns default Descriptor (id=0, status=NONE)
		*/
		CommandDesc findCommandDescriptor(const CommandHistory& history, CommandID id) {
			PCommand cmd = history.find(id);
			if(cmd.get() != 0L)
				return cmd->getDescriptor();
			else
				return CommandDesc();
		}

		/** helpers, first executes in Scheduler and insterts cmd to CommandHistory */
		CommandID executeAsynchronouslyAndRemember(CommandHistory& history, PCommand cmd) {
			Scheduler& scheduler = Scheduler::getInstance();
			CommandID id = scheduler.executeAsynchronously(cmd);
			history.insert(id, cmd);
			return id;
		}

		/** helpers, first executes in Scheduler and insterts cmd to CommandHistory */
		CommandID executeSynchronouslyAndRemember(CommandHistory& history, PCommand cmd) {
			Scheduler& scheduler = Scheduler::getInstance();
			CommandID id = scheduler.executeSynchronously(cmd);
			history.insert(id, cmd);
			return id;
		}
	} //namespace actobj
} //namespace faif

