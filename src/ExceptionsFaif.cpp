#if defined(_MSC_VER)
//  msvc warning 'std::copy deprecated'
#pragma warning(disable:4996)
#endif

#include "ExceptionsFaif.h"
#include <cstring>

using namespace std;

namespace faif {

	namespace {
		void strncopyn(char* dst, const char* src, size_t size) {
			strncpy(dst,src,size);
			dst[size-1]='\0';
		}
	}

	std::ostream& FaifException::print(std::ostream& os) const throw() {
		os << what() << std::endl;
		return os;
	}

	NotFoundException::NotFoundException(const char* domain_id) {
		strncopyn(domainID_, domain_id, SIZE );
	}

	std::ostream& NotFoundException::print(std::ostream& os) const throw() {
		os << "Attribute in domain " << domainID_ << " not found";
		return os;
	}

	std::ostream& operator<<(std::ostream& os, const FaifException& ex) {
		ex.print(os);
		return os;
	}

} //namespace faif
