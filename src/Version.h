#ifndef FAIF_VERSION_H
#define FAIF_VERSION_H

//the version of faif library

/** \brief the namespace of library
 */
namespace faif {

	//! \brief major version of library
	extern int getVersionMajor();

	//! \brief minor version of library
    extern int getVersionMinor();

	//! \brief compilation version of library
    extern int getVersionCompilation();

} //namespace faif

#endif //FAIF_VERSION_H
