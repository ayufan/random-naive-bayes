//   Plik zawiera class NaiveBayesian.

#ifndef NBC_NAIVE_BAYESIAN_H_
#define NBC_NAIVE_BAYESIAN_H_

#include <memory>

#include "../ExceptionsFaif.h"
#include "../Attribute.h"
#include "../Domain.h"

#include "Classifier.h"
#include "ExampleTrain.h"

namespace faif {
    namespace ml {

        class NaiveBayesianImpl;

        /** \brief Naive Bayesian Classifier.

			Contains the attributes, attribute values and categories,
			train examples, test examples and classifier methods.
        */
        class NaiveBayesian : public Classifier {
        public:
            NaiveBayesian();
            virtual ~NaiveBayesian();

            /** inserts the new attribute domain or return the existing */
            AttrDomain& insertAttr(const std::string& id);

            /** find the attribute domain of given id or throws NotFoundException */
			AttrDomain& findAttrRef(const std::string& id);

            /** insert the new category or return the existing */
			AttrDomain::iterator insertCategory(const std::string& id);

			/** the clear the learned parameters */
			virtual void reset();

            /** add training example */
            virtual void addTraining(const ExampleTrain&);

            /** classify (Naive Bayes Classifier) */
            virtual AttrVal getCategory(const ExampleTest&) const;

            /** ostream operator */
            friend std::ostream& operator<<(std::ostream&, const NaiveBayesian&);
        private:
            /** copy c-tor not allowed */
            NaiveBayesian(const NaiveBayesian&);
            /** assignment not allowed */
            NaiveBayesian& operator=(const NaiveBayesian&);

            std::auto_ptr<NaiveBayesianImpl> impl_;
        };
    }//namespace faif
} //namespace ml

#endif //NBC_NAIVE_BAYESIAN_H_
