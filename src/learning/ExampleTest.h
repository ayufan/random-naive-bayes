#ifndef FAIF_EXAMPLE_TEST_H_
#define FAIF_EXAMPLE_TEST_H_

#include <iostream>
#include <vector>
#include "../Attribute.h"
#include "../Domain.h"

namespace faif {
    /**
       \brief machine learning algorithms
    */
    namespace ml {

		/** \brief  attribute value representation in learning */
		typedef const AttrNominalString* AttrVal;

		/** \brief the attribute domain for NBC */
		typedef AttrNominalString::DomainType AttrDomain;

		/** \brief collection of attribute values */
		typedef std::vector<AttrVal> Example;

		/** \brief the test example (collection of AttrVal) */
		class ExampleTest {
		public:
			ExampleTest();
			ExampleTest(const std::vector<AttrVal>&);
			ExampleTest(const ExampleTest&);
			ExampleTest& operator=(const ExampleTest&);

			virtual ~ExampleTest();

			/** c-tor with 1,2,3,4,5 parameters */
			ExampleTest(AttrVal va){
				example_.push_back(va);
			}

			/** c-tor with 1,2,3,4,5 parameters */
			ExampleTest(AttrVal va, AttrVal vb) {
				example_.push_back(va);
				example_.push_back(vb);
			};

			/** c-tor with 1,2,3,4,5 parameters */
			ExampleTest(AttrVal va, AttrVal vb, AttrVal vc) {
				example_.push_back(va);
				example_.push_back(vb);
				example_.push_back(vc);
			}

			/** c-tor with 1,2,3,4,5 parameters */
			ExampleTest(AttrVal va, AttrVal vb, AttrVal vc, AttrVal vd) {
				example_.push_back(va);
				example_.push_back(vb);
				example_.push_back(vc);
				example_.push_back(vd);
			}

			/** c-tor with 1,2,3,4,5 parameters */
			ExampleTest(AttrVal va, AttrVal vb, AttrVal vc, AttrVal vd, AttrVal ve) {
				example_.push_back(va);
				example_.push_back(vb);
				example_.push_back(vc);
				example_.push_back(vd);
				example_.push_back(ve);
			}

			/** accesor */
			Example& get() { return example_; }

			/** accesor */
			const Example& get() const { return example_; }

			/** the equal operator */
			bool operator==(const ExampleTest& right) const;

			/** the not equal operator */
			bool operator!=(const ExampleTest& right) const {
				return ! operator==(right);
			}

			/** mutator, add atribute to example */
			void push_back(AttrVal v) {
				example_.push_back(v);
			}

			//** accessor, num attributes in example */
			int size() const { return (int)example_.size(); }

			/** the stream method */
			virtual void write(std::ostream& os) const;
		private:
			Example example_;
		};

		/** stream operator - calls the write method */
		std::ostream& operator<<(std::ostream& os, const ExampleTest& ex);

    } //namespace ml
} //namespace faif

#endif //FAIF_EXAMPLE_H_
