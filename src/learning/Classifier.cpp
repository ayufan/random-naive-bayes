#include "Classifier.h"

#include <algorithm>
#include <functional>
#include <boost/bind.hpp>

using namespace std;

namespace faif {
    namespace ml {

		/** funkcja wywolujaca metode addTraining dla kazdego przykladu z kolekcji */
		void Classifier::addTrainingCollection(const ExamplesTrain& train) {
			for_each( train.begin(), train.end(), boost::bind( &Classifier::addTraining, this, _1 ) );
		}

    } //namespace ml
} //namespace faif

