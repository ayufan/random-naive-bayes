#ifndef FAIF_VALIDATOR_H_
#define FAIF_VALIDATOR_H_

//   Plik zawiera klasy i funkcje badajace jakosc klasyfikacji.

#include <boost/noncopyable.hpp>
#include <boost/scoped_ptr.hpp>

#include "ExampleTrain.h"
#include "Classifier.h"

namespace faif {
	namespace ml {

        /**
           \brief check the classifier
		   \return the number of test examples correctly classified

           \param test examples
           \param classifier
        */
        int checkClassifier(const ExamplesTrain& test, Classifier& classifier );

        /** \brief cross validator

			test examples are divided on k sets (random shuffle, equal size). One is the testing set,
			othrs are training sets. The procedure of train and check is performed k times
			(each time the differnet set is the testing set). Return the average error.
		*/
        class CrossValidator
            : public boost::noncopyable  {
        public:
            CrossValidator();
            ~CrossValidator();

            /** \brief check method in cross validator

               \param e set of examples (train and test)
               \param k number of sets in cross-validation
               \param classifier
               \return probability of proper classification
            */
            double checkCross( const ExamplesTrain& e, int k, Classifier& classifier);
        };

    } //namespace ml
} //namespace faif

#endif //FAIF_VALIDATOR_H_
