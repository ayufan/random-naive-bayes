/** implementacja klasy CrossValidator.
 */

#if defined(_MSC_VER) && (_MSC_VER >= 1400)
//msvc9.0 generuje smieci dla boost/date_time
#pragma warning(disable:4244)
//msvc9.0 generuje smieci dla boost/random
#pragma warning(disable:4512)

#endif

#include "../Validator.h"

#include <ctime>
#include <cassert>
#include <vector>
#include <boost/lambda/lambda.hpp>

#include "../../utils/Random.h"

using namespace std;

namespace faif {
    namespace ml {


		CrossValidator::CrossValidator() { }

		CrossValidator::~CrossValidator() { }

		namespace {

			/** pomocniczy funktor */
			struct ShuffleFunctor {
				RandomInt& r_;
				ShuffleFunctor(RandomInt& r)
					: r_(r) { }
				int operator()(int){ return r_(); }
			private:
				ShuffleFunctor& operator=(const ShuffleFunctor&); //zabronione przypisanie bo skladowe referencyjne
			};

			/** pomocniczy typ, przechowuje przyklady trenujace, ktore sa losowo mieszane */
			typedef vector<const ExampleTrain*> TrainCollection;

			/** f. pomocnicza, zwraca liczbe prawidlowo zaklasyfikowanych przykladow z tcollect.
				Przyklady testujace to <start_idx, end_idx). Reszta - to przyklady trenujace. */
			int testRange(TrainCollection& tcollect, int start_idx, int end_idx, Classifier& classifier ) {

				ExamplesTrain test;
				ExamplesTrain train;

				TrainCollection::iterator it = tcollect.begin();
				for(int idx=0; it != tcollect.end(); ++it, ++idx ) {
					if( idx < start_idx )
						train.push_back( **it );
					else if( idx < end_idx )
						test.push_back( **it );
					else
						train.push_back( **it );
				}

				classifier.reset();
				classifier.addTrainingCollection(train);
				return checkClassifier( test, classifier );
			}

		} //namespace

		/**
		   bada klasyfikator (badanie krzyzowe), zwraca prawdopodobienstwo prawidlowej klasyfikacji.
		   Przyklady dzielone sa na k rownych podzbiorow (przypozadkowanie: losowe). Jeden z nich to podzbior
		   testujace, pozostale k-1 to sa trenujace. Procedura trenowania i testowania powtarzana jest k razy
		   (za kazdym razem inny przedzial jest trenujacym). Zwracany wynik jest usredniany.

		   \param examples przyklady trenujace (czesc z nich jest testujaca)
		   \param k liczba przedzialow przy walidacji krzyzowej.
		   \param classifier badany klasyfikator
		   \return prawdopodobienstwo poprawnej klasyfikacji.
		*/
		double CrossValidator::checkCross( const ExamplesTrain& examples, int k, Classifier& classifier) {

			int n = (int)examples.size();

			assert( k > 0 );
			assert( n >= k );

			TrainCollection tcollect( n );
			transform( examples.begin(), examples.end(), tcollect.begin(), & boost::lambda::_1 );
			RandomInt gen(0, n-1);
			ShuffleFunctor shuffleFunctor( gen );
			random_shuffle( tcollect.begin(), tcollect.end(), shuffleFunctor );

			int start_idx = 0;
			int end_idx = 0;

			int num_proper = 0; //liczba prawidlowo zaklasyfikowanych przykladow testujacych

			for(int i = 0; i < k; i++ ) {
				start_idx = end_idx;
				end_idx = (n * (i + 1) )/k;
				//k-ty przedzial jest przedzialem testujacym
				num_proper += testRange(tcollect, start_idx, end_idx, classifier );
			}

			int num_all = (int)tcollect.size();
			assert( num_all > 0 );
			return num_proper /(double)num_all ;
		}

    } //namespace ml
} //namespace faif
