/** plik zawiera implementacje funkcje uczaca klasyfikator a nastepnie
    zlicza prawidlowo zaklasyfikowane przyklady testujace
*/

#include "../Validator.h"
#include <algorithm>

using namespace std;

namespace faif {
    namespace ml {


		namespace {
			/** pomocniczy predykat, zwraca true jezeli klasyfikacja przykladu jest zgodna z jego kategoria */
			struct CheckExampleFunctor {
				Classifier& cl_;
				CheckExampleFunctor(Classifier& c)
					: cl_(c) {
				}
				bool operator()(const ExampleTrain& example) {
					return cl_.getCategory(example) == example.getCategory();
				}
			private:
				CheckExampleFunctor& operator=(const CheckExampleFunctor&); //zabronione przyp bo skladowe referencyjne
			};
		} //namespace

		/**
		   bada klasyfikator,
		   zwraca liczbe przykladow testujacych, ktore zostaly prawidlowo klasyfikowane.

		   \param test przyklady testujace
		   \param classifier badany klasyfikator
		   \return liczba przykladow testujacych, ktore zostaly prawidlowo klasyfikowane.
		*/

		int checkClassifier(const ExamplesTrain& test, Classifier& classifier ) {
			CheckExampleFunctor checkFunctor(classifier);
			return (int)count_if( test.begin(), test.end(), checkFunctor );
		}

    } //namespace ml
} //namespace faif
