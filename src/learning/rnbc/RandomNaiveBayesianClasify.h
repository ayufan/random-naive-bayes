#ifndef RNBC_RANDOM_NAIVE_CLASSIFY_H
#define RNBC_RANDOM_NAIVE_CLASSIFY_H

#include <map>

#include "../ExampleTest.h"
#include "RandomNaiveBayesianRoot.h"

namespace faif {
	namespace ml {

		/** typ definiujacy t�umacza warto�ci mi�dzy implementacj� losow� a naiwn� */
		typedef std::map<AttrVal, AttrVal> AttrMapper;

		class NaiveBayesian;

		/** implementacja naiwnego klasyfikatora Bayesowskiego.
			Klasa konkretna: klasyfikator, ktory klasyfikuje (nie uczy sie). */
		class RandomNaiveBayesianClasify : public RandomNaiveBayesianRoot {
			NaiveBayesian* bags_; //!< lista klasyfikator�w
			AttrMapper* mapper_; //!< lista t�umaczy
			int bagsCount_; //!< ilo�� toreb

		private:
			/** noncopyable */
			RandomNaiveBayesianClasify(const RandomNaiveBayesianClasify&);
			/** noncopyable */
			RandomNaiveBayesianClasify& operator=(const RandomNaiveBayesianClasify&);

			/** tworzy list� toreb na podstawie kt�rych b�dzie wykonywana losowa klasyfikacja 
			*/
			void createBags();

		public:
			RandomNaiveBayesianClasify(RandomNaiveBayesianImpl& impl);
			~RandomNaiveBayesianClasify();

			/** metoda dodaje przyklad trenujacy.
				Tutaj powosduje przeladowanie klasyfikatora na obiekt, ktory sie uczy */
			void addTraining(const ExampleTrain& train);

			/** metoda klasyfikuje podany przyklad algorytmem losowego naiwnego klasyfikatora Bayesowskiego
				na podstawie dostarczonych przykladow.
			*/
			AttrVal getCategory(const ExampleTest& test) const;
		};
	}
}

#endif
