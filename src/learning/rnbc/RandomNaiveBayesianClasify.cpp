#include "RandomNaiveBayesianImpl.h"
#include "RandomNaiveBayesianClasify.h"
#include "../NaiveBayesian.h"
#include "../../utils/Random.h"

using namespace std;
using namespace faif::ml;

typedef pair<string, const AttrDomain*> AttribDomainMapping;

struct RemapBayesianCategoriesFunctor {
	NaiveBayesian& bayesian_;
	AttrMapper& mapper_;

	RemapBayesianCategoriesFunctor(NaiveBayesian& bayesian, AttrMapper& mapper) : bayesian_(bayesian), mapper_(mapper) {}

	void operator ()(const AttrDomain::Container::value_type& category) {
		mapper_[&category] = &*bayesian_.insertCategory(category.get());
	}
};

struct EnumerateAttribsFunctor {
	vector<AttribDomainMapping >& attribs_;

	EnumerateAttribsFunctor(vector<AttribDomainMapping >& attribs) : attribs_(attribs) {}

	void operator ()(const AttrDomain& attrib) {
		attribs_.push_back(make_pair(attrib.getId(), &attrib));
	}
};

struct RemapBayesianAttribsFunctor {
	NaiveBayesian& bayesian_;
	AttrMapper& mapper_;
	AttrDomain* domain_;

	RemapBayesianAttribsFunctor(NaiveBayesian& bayesian, AttrMapper& mapper) : bayesian_(bayesian), mapper_(mapper) {}

	void operator ()(const AttribDomainMapping& attrib) {
		domain_ = &bayesian_.insertAttr(attrib.first);
		for_each(attrib.second->begin(), attrib.second->end(), *this);
	}

	void operator ()(const AttrDomain::Container::value_type& value) {
		AttrVal val = &*domain_->insert(value.get());
		mapper_[&value] = val;
	}
};

RandomNaiveBayesianClasify::RandomNaiveBayesianClasify(RandomNaiveBayesianImpl& impl) : RandomNaiveBayesianRoot(impl) { 
	createBags(); 
}

RandomNaiveBayesianClasify::~RandomNaiveBayesianClasify() {
	delete [] bags_;
	delete [] mapper_;
}

void RandomNaiveBayesianClasify::createBags() {
	bags_ = new NaiveBayesian[impl_.K];
	mapper_ = new AttrMapper[impl_.K];
	bagsCount_ = impl_.K;

	for ( unsigned i = 0; i < impl_.K; ++i) {
		NaiveBayesian& bayesian = bags_[i];
		AttrMapper& mapper = mapper_[i];

		vector<AttribDomainMapping > attribs;

		for_each(impl_.domains_.begin(), impl_.domains_.end(), RemapBayesianCategoriesFunctor(bayesian, mapper));
		for_each(impl_.attribs_.begin(), impl_.attribs_.end(), EnumerateAttribsFunctor(attribs));

		// remove random attributes
		while(attribs.size() != impl_.m) {
			attribs.erase(attribs.begin() + (faif::RandomInt(0, attribs.size()-1)() % attribs.size()));
		}

		for_each(attribs.begin(), attribs.end(), RemapBayesianAttribsFunctor(bayesian, mapper));

		// remap trainings
		faif::RandomInt randomEx(0, impl_.examples_.size()-1);

		for(unsigned i = 0; i < impl_.examples_.size(); ++i) {
			ExampleTrain& train2 = impl_.examples_[ randomEx() % impl_.examples_.size() ];
			if(mapper.find(train2.getCategory()) == mapper.end()) {
				continue; // nieznana kategoria
			}

			ExampleTrain train(train2, mapper[train2.getCategory()]);

			for(int j = train.get().size(); j-- > 0; ) {
				AttrVal& val = train.get()[j];

				if(mapper.find(val) == mapper.end())
					train.get().erase(train.get().begin() + j);
				else
					val = mapper[val];
			}

			bayesian.addTraining(train);
		}
	}

}

void RandomNaiveBayesianClasify::addTraining(const ExampleTrain& train) {
	impl_.switchAndTrain(train);
}

AttrVal RandomNaiveBayesianClasify::getCategory(const ExampleTest& test) const {
	typedef map<AttrVal, int> RateMap;

	RateMap rate;

	for ( int i = 0; i< bagsCount_; ++i) {
		NaiveBayesian& bayesian = bags_[i];
		AttrMapper& mapper = mapper_[i];

		ExampleTest test2(test);

		for(int j = test2.get().size(); j-- > 0; ) {
			AttrVal& val = test2.get()[j];

			if(mapper.find(val) == mapper.end())
				test2.get().erase(test2.get().begin() + j);
			else
				val = mapper[val];
		}

		AttrVal val = bayesian.getCategory(test2);
		if(!val) 
			continue;
		val = &*impl_.domains_.find(val->get());
		if(!val) 
			continue;
		++rate[val];
	}

	if(rate.empty())
		throw NotFoundException("classifier");
	
	RateMap::const_iterator temp = rate.begin(), result;
	for (result = temp++ ; temp != rate.end(); ++temp)
		if (result->second < temp->second) 
			result = temp;

	return result->first;
}
