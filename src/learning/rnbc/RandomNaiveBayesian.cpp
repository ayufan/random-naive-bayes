/**
   Plik zawiera class RandomNaiveBayesian.
*/

#include <algorithm>

#include "../ExampleTest.h"
#include "../RandomNaiveBayesian.h"
#include "RandomNaiveBayesianImpl.h"

using namespace std;
using namespace faif::ml;

RandomNaiveBayesian::RandomNaiveBayesian()
	: impl_(new RandomNaiveBayesianImpl() ) {
}

RandomNaiveBayesian::~RandomNaiveBayesian() {
}

/** metoda definiuje nowy atrybut lub zwraca istniejacy */
AttrDomain& RandomNaiveBayesian::insertAttr(const std::string& id) {

	Attributes& attribs = impl_->getAttributes();
	AttrDomain tmp(id);
	Attributes::iterator found = std::find(attribs.begin(), attribs.end(), tmp );
	if(found != attribs.end() )
		return *found; //jezeli znalazl - to zwraca odp. iterator

	//jezeli nie znalazl - to wstawia na koniec
	attribs.push_back(tmp);
	return attribs.back();
}

/** metoda znajduje atrybut o danym identyfikatorze.
	Jezeli nie istnieje generuje wyjatek NotFoundException */
AttrDomain& RandomNaiveBayesian::findAttrRef(const std::string& id) {
	Attributes& attribs = impl_->getAttributes();
	AttrDomain tmp(id);
	Attributes::iterator found = std::find(attribs.begin(), attribs.end(), tmp );
	if(found == attribs.end() )
		throw NotFoundException("classifier");
	return *found;
}


/** metoda definiuje nowa kategorie lub zwraca istniejaca */
AttrDomain::iterator RandomNaiveBayesian::insertCategory(const std::string& id) {
	return impl_->getCategories().insert(id);
}

void RandomNaiveBayesian::reset() {
	return impl_->reset();
}

/** metoda dodaje przyklad trenujacy */
void RandomNaiveBayesian::addTraining(const ExampleTrain& example) {
	impl_->addTraining(example);
}

/** metoda klasyfikuje podany przyklad algorytmem naiwnego klasyfikatora Bayesowskiego
	na podstawie dostarczonych przykladow
*/
AttrVal RandomNaiveBayesian::getCategory(const ExampleTest& example) const {
	return impl_->getCategory(example);
}

void RandomNaiveBayesian::setParameters(unsigned K, unsigned m) {
	impl_->setParameters(K, m);
}
