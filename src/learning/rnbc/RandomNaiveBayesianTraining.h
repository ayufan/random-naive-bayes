#ifndef RNBC_BAYESIAN_TRAINING_H
#define RNBC_BAYESIAN_TRAINING_H

#include <map>

#include "../ExampleTest.h"
#include "RandomNaiveBayesianRoot.h"

namespace faif {
	namespace ml {

		/** implementacja losowego naiwnego klasyfikatora Bayesowskiego.
			Klasa konkretna: klasyfikator, ktory sie uczy. */
		class RandomNaiveBayesianTraining : public RandomNaiveBayesianRoot {
		public:
			RandomNaiveBayesianTraining(RandomNaiveBayesianImpl& impl) : RandomNaiveBayesianRoot(impl) {}

			/** metoda dodaje przyklad trenujacy */
			void addTraining(const ExampleTrain& train);

			/** metoda klasyfikuje podany przyklad algorytmem losowego naiwnego klasyfikatora Bayesowskiego
				na podstawie dostarczonych przykladow.
				Tutaj powoduje przeladowanie klasyfikatora na obiekt, ktory klasyfikuje (ten tutaj sie uczy )
			*/
			AttrVal getCategory(const ExampleTest& test) const;
		};

	}
}

#endif
