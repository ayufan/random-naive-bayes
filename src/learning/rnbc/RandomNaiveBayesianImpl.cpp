#include <cassert>
#include <iterator>
#include <stdexcept>

#include "RandomNaiveBayesianImpl.h"
#include "RandomNaiveBayesianTraining.h"
#include "RandomNaiveBayesianClasify.h"

using namespace std;
using namespace faif::ml;

RandomNaiveBayesianImpl::RandomNaiveBayesianImpl() : K(0), m(0) {
	classifier_.reset(new RandomNaiveBayesianTraining(*this));
}

RandomNaiveBayesianImpl::~RandomNaiveBayesianImpl() {
}

AttrDomain& RandomNaiveBayesianImpl::insertAttr(const std::string& id) {
		AttrDomain tmp(id);
		Attributes::iterator found = std::find(attribs_.begin(), attribs_.end(), tmp );
		if(found != attribs_.end() )
			return *found; //jezeli znalazl - to zwraca odp. iterator

		//jezeli nie znalazl - to wstawia na koniec
		attribs_.push_back(tmp);
		return attribs_.back();
}

AttrDomain& RandomNaiveBayesianImpl::findAttrRef(const std::string& id) {
		AttrDomain tmp(id);
		Attributes::iterator found = std::find(attribs_.begin(), attribs_.end(), tmp );
		if(found == attribs_.end() )
			throw NotFoundException("classifier");
		return *found;
}

AttrDomain::iterator RandomNaiveBayesianImpl::insertCategory(const std::string& id) {
	return domains_.insert(id);
}

void RandomNaiveBayesianImpl::addTraining(const ExampleTrain& train) {
	classifier_->addTraining(train);
}

void RandomNaiveBayesianImpl::reset() {
	domains_ = AttrDomain();
	examples_.clear();
	attribs_.clear();
	classifier_.reset(new RandomNaiveBayesianTraining(*this));
}

void RandomNaiveBayesianImpl::switchAndTrain(const ExampleTrain& example) {
	classifier_.reset(new RandomNaiveBayesianTraining(*this));
	classifier_->addTraining(example);
}
	
AttrVal RandomNaiveBayesianImpl::switchAndClasify(const ExampleTest& example) {
	if(K < 1 || m < 1 || m > attribs_.size())
		throw invalid_argument("invalid classify parameters");
	classifier_.reset(new RandomNaiveBayesianClasify(*this));
	return classifier_->getCategory(example);
}

AttrVal RandomNaiveBayesianImpl::getCategory(const ExampleTest& train) const {
	return classifier_->getCategory(train);
}

void RandomNaiveBayesianImpl::setParameters(unsigned Kv, unsigned mv) {
	if(Kv < 1)
		throw invalid_argument("K");
	if(mv > attribs_.size() || mv < 1)
		throw invalid_argument("m");
	K = Kv; 
	m = mv; 
	classifier_.reset(new RandomNaiveBayesianTraining(*this));
}
