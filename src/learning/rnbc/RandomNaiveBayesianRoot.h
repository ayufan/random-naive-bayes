#ifndef RNBC_RANDOM_BAYESIAN_ROOT_H
#define RNBC_RANDOM_BAYESIAN_ROOT_H

#include "../ExampleTrain.h"

namespace faif {
    namespace ml {

		class RandomNaiveBayesianImpl;

		/** klasa bazowa dla klas implementujacych losowy naiwny klasyfikator Bayesowski.
			Klasa dostarcza danych. Klasy konkretne to RandomNaiveBayesianTraining, RandomNaiveBayesianClassifier
		*/
		class RandomNaiveBayesianRoot { 
		protected:
			RandomNaiveBayesianImpl& impl_;
		public:
			RandomNaiveBayesianRoot(RandomNaiveBayesianImpl& impl) : impl_(impl) {}
			virtual ~RandomNaiveBayesianRoot() {}

			/** metoda dodaje przyklad trenujacy. */
			virtual void addTraining(const ExampleTrain& train) = 0;

			/** metoda klasyfikuje podany przyklad */
			virtual AttrVal getCategory(const ExampleTest&) const = 0;

		private:
			/** noncopyable */
			RandomNaiveBayesianRoot(const RandomNaiveBayesianRoot&);
			/** noncopyable */
			RandomNaiveBayesianRoot& operator=(const RandomNaiveBayesianRoot&);
		};

		}
}

#endif