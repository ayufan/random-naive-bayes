#ifndef RNBC_RANDOM_BAYESIAN_IMPL_H
#define RNBC_RANDOM_BAYESIAN_IMPL_H

#include <memory>
#include <list>
#include <vector>

#include "../ExampleTrain.h"
#include "../Classifier.h"
#include "../nbc/BayesCategories.h"
#include "../nbc/NaiveBayesianImpl.h"
#include "RandomNaiveBayesianRoot.h"

namespace faif {
	namespace ml {

		/** klasa dostarczajaca implementacji losowego naiwnego klasyfikatra Bayesowskiego.
			Agreguje atrybuty, wartosci i kategorie oraz jeden z obiektow przetwarzajacych
			informacje (implementujacy interfejs RootNaiveBayesianRoot )
		*/
		class RandomNaiveBayesianImpl : public Classifier {
			std::auto_ptr<RandomNaiveBayesianRoot> classifier_; //!< obiekt przetwarzajacy informacje
			AttrDomain domains_; //!< przechowuje kategorie
			Attributes attribs_; //!< przechowuje atrybuty i wartosci
			std::vector<ExampleTrain> examples_; //!< przechowuje przyklady testowe
			unsigned K, m; //!< przechowuje wartosc parametry K i m

		public:
			RandomNaiveBayesianImpl();
			virtual ~RandomNaiveBayesianImpl();

			/** accessor, dostarcza atrybutow */
			Attributes& getAttributes(){ return attribs_; }
			/** accessor, dostarcza atrybutow */
			AttrDomain& getCategories(){ return domains_; }

			AttrDomain& insertAttr(const std::string& id);

			AttrDomain& findAttrRef(const std::string& id);

			AttrDomain::iterator insertCategory(const std::string& id);

			/** metoda dodaje przyklad trenujacy. */
			void addTraining(const ExampleTrain& train);

			/** metoda zmienia stan klasyfikatora na taki, jaki byl przed nauczeniem sie czegokolwiek */
			void reset();

			/** zmienia obiekt przetwarzajacy na klasyfikator uczacy sie i dodaje przyklad */
			void switchAndTrain(const ExampleTrain& example);

			/** zmienia obiekt przetwarzajacy na klasyfikator rozpoznajacy i klasyfikuje */
			AttrVal switchAndClasify(const ExampleTest& example);

			/** metoda klasyfikuje podany przyklad */
			AttrVal getCategory(const ExampleTest& train) const;

			/** set's random naive bayesian parameters
				* @params K number of bags
				* @params m number of features selected
				* @throw invalid_argument if K=0 or m=0 or m>number of attribs
				*/
			void setParameters(unsigned K, unsigned m);

			friend class RandomNaiveBayesianClasify;
			friend class RandomNaiveBayesianTraining;
			friend class RandomNaiveBayesian;
		};

	}
}

#endif
