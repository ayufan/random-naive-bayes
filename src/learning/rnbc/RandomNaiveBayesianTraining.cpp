#include "RandomNaiveBayesianImpl.h"
#include "RandomNaiveBayesianTraining.h"

using namespace std;
using namespace faif::ml;

void RandomNaiveBayesianTraining::addTraining(const ExampleTrain& train) {
	impl_.examples_.push_back(train);
}

AttrVal RandomNaiveBayesianTraining::getCategory(const ExampleTest& test) const {
	return impl_.switchAndClasify(test);
}
