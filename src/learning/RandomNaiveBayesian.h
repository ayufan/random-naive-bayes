//   Plik zawiera class RandomNaiveBayesian.

#ifndef NBC_RANDOM_NAIVE_BAYESIAN_H_
#define NBC_RANDOM_NAIVE_BAYESIAN_H_

#include <memory>

#include "../ExceptionsFaif.h"
#include "../Attribute.h"
#include "../Domain.h"

#include "Classifier.h"
#include "ExampleTrain.h"

namespace faif {
    namespace ml {

        class RandomNaiveBayesianImpl;

        /** \brief Naive Bayesian Classifier.

			Contains the attributes, attribute values and categories,
			train examples, test examples and classifier methods.
        */
        class RandomNaiveBayesian : public Classifier {
        public:
            RandomNaiveBayesian();
            virtual ~RandomNaiveBayesian();

            /** inserts the new attribute domain or return the existing */
            AttrDomain& insertAttr(const std::string& id);

            /** find the attribute domain of given id or throws NotFoundException */
						AttrDomain& findAttrRef(const std::string& id);

            /** insert the new category or return the existing */
						AttrDomain::iterator insertCategory(const std::string& id);

						/** the clear the learned parameters */
						virtual void reset();

            /** add training example */
            virtual void addTraining(const ExampleTrain&);

            /** classify (Naive Bayes Classifier) */
            virtual AttrVal getCategory(const ExampleTest&) const;

						/** set's random naive bayesian parameters
						  * @params K number of bags
							* @params m number of features selected
							* @throw invalid_argument if K=0 or m=0 or m>number of attribs
							*/
						void setParameters(unsigned K, unsigned m);

        private:
            /** copy c-tor not allowed */
            RandomNaiveBayesian(const RandomNaiveBayesian&);
            /** assignment not allowed */
            RandomNaiveBayesian& operator=(const RandomNaiveBayesian&);

            std::auto_ptr<RandomNaiveBayesianImpl> impl_;
        };
    }//namespace faif
} //namespace ml

#endif //NBC_RANDOM_NAIVE_BAYESIAN_H_
