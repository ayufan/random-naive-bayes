#ifndef FAIF_EXAMPLE_TRAIN_H_
#define FAIF_EXAMPLE_TRAIN_H_

#include <iostream>
#include <vector>
#include "ExampleTest.h"

namespace faif {
    namespace ml {

		/** \brief the train example (test example and the category) */
		class ExampleTrain : public ExampleTest {
		public:
			ExampleTrain();
			ExampleTrain(const ExampleTest& test, AttrVal category_);
			ExampleTrain(const ExampleTrain& train);
			ExampleTrain& operator=(const ExampleTrain& right);

			virtual ~ExampleTrain();

			AttrVal getCategory() const { return category_; }

			/** \brief write the attributes to ostream */
			virtual void write(std::ostream& os) const;
		private:
			AttrVal category_;
		};

		/** \brief the examples train contener */
		typedef std::vector<ExampleTrain> ExamplesTrain;
    } //namespace ml
} //namespace faif

#endif //FAIF_EXAMPLE_TRAIN_H_
