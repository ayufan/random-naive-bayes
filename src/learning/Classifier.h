#ifndef FAIF_CLASIFIER_H_
#define FAIF_CLASIFIER_H_

#include "ExampleTest.h"
#include "ExampleTrain.h"

namespace faif {
    namespace ml {

		/** \brief the clasiffier abstract interface
		 */
		class Classifier {
		public:
			virtual ~Classifier(){}

			/** \brief the clasiffier will have no knowledge */
			virtual void reset() = 0;

			/** \breif add the training example (learn) */
			virtual void addTraining(const ExampleTrain&) = 0;

			/** \brief add the collection of training examples (learn) */
			void addTrainingCollection(const ExamplesTrain&);

			/** \brief classify */
			virtual const AttrNominalString* getCategory(const ExampleTest&) const = 0;
		};

    } //namespace ml
} //namespace faif

#endif //FAIF_CLASIFIER_H_
