#include "ExampleTrain.h"

#include <cassert>
#include <algorithm>
#include <iterator>

using namespace faif;
using namespace std;

namespace faif {
    namespace ml {


		ExampleTrain::ExampleTrain()
			: ExampleTest(), category_(0L) {
		}

		ExampleTrain::ExampleTrain(const ExampleTest& test, AttrVal cat)
			: ExampleTest(test), category_(cat) {
		}

		ExampleTrain::ExampleTrain(const ExampleTrain& train)
			: ExampleTest(train), category_(train.category_) {
		}

		ExampleTrain& ExampleTrain::operator=(const ExampleTrain& right) {
			ExampleTest::operator=(right);
			category_ = right.category_;
			return *this;
		}

		ExampleTrain::~ExampleTrain() {
		}

		/** ostream operator */
		void ExampleTrain::write(std::ostream& os) const {
			ExampleTest::write(os);
			assert( category_ != 0L );
			os << ": " << *category_;
		}
    } //namespace ml
} //namespace faif
