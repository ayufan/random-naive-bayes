/** plik zawiera funkcje pomocnicze, sluzace do drukowania wartosci licznikow.
    Uzywane przez NaiveBayesianTraining oraz NaiveBayesianClasify aby wyprodukowac debug_string-a
*/

#ifndef NBC_PRINT_COUNTERS_INFORMATION_H
#define NBC_PRINT_COUNTERS_INFORMATION_H

#include <iostream>
#include <algorithm>
#include "NaiveBayesianImpl.h"

namespace faif {
    namespace ml {

        /** \brief class to show the classifier state, print the attribs and counters */
        template<class Categories>
		struct PrintCountAttrFunctor {
			std::ostream& os_;
			AttrVal catVal_;
			const Categories& categories_;

			PrintCountAttrFunctor(std::ostream& os, AttrVal cat_val, const Categories& categories)
				: os_(os), catVal_(cat_val), categories_(categories) {
			}
			void operator()(const AttrDomain& attr) {
				for(AttrDomain::const_iterator ii = attr.begin(); ii != attr.end(); ++ii ) {
					AttrVal val = &(*ii);
					// drukuje licznik dla danej wartosci w danej kategorii
					os_ << val->get() << "(" << categories_.getCategoryValCounter(catVal_, val) << "),";
				}
				os_ << std::endl;
			}
		private:
			PrintCountAttrFunctor& operator=(const PrintCountAttrFunctor&); //not allwed because references
		};

        /** \brief print the cauters for given category */
        template<class Categories>
		struct PrintCountersFunctor {
			std::ostream& os_;
			const Attributes& attributes_;
			const Categories& categories_;

			PrintCountersFunctor( std::ostream& os, const Attributes& attrib, const Categories& categories)
				: os_(os), attributes_(attrib), categories_(categories) {
			}

			void operator()(const AttrNominalString& cat_val) {
				os_ << cat_val << "(" << categories_.getCategoryCounter(&cat_val) << "):" << std::endl;
				PrintCountAttrFunctor<Categories> printCountAttr(os_, &cat_val, categories_);
				std::for_each( attributes_.begin(), attributes_.end(), printCountAttr );
			}
		private:
			PrintCountersFunctor& operator=(const PrintCountersFunctor&); //zabronione przypisanie bo skladowe referencyjne
		};

    } //namespace ml
} //namespace faif

#endif //NBC_PRINT_COUNTERS_INFORMATION_H
