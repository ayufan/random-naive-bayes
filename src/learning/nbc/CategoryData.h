#ifndef NBC_CATEGORY_DATA_H_
#define NBC_CATEGORY_DATA_H_

#include <map>
#include "../ExampleTrain.h"

namespace faif {
    namespace ml {

		/** \brief internal class to connect category and counter or probability

			the category collection; connection between category and attribute value.
			In learining mode used to count train examples,
			in classifier mode used to calculate probability
		*/
		template<class T> class CategoryData {
		public:
			typedef std::map<AttrVal,T> AttrData;

			CategoryData()
				: data_(0), attrData_() {
			}
			CategoryData(const T& d)
				: data_(d), attrData_() {
			}
			CategoryData(const T& d, const AttrData& ad)
				: data_(d), attrData_(ad) {
			}
			CategoryData(const CategoryData& cd)
				: data_(cd.data_), attrData_(cd.attrData_) {
			}
			CategoryData& operator=(const CategoryData& cd) {
				data_ = cd.data_;
				attrData_ = cd.attrData_;
				return *this;
			}
			~CategoryData(){}

			T data_;
			AttrData attrData_;
		};
    } //namespace ml
} //namespace faif

#endif //NBC_CATEGORY_DATA_H_
