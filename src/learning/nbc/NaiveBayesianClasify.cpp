#include "NaiveBayesianClasify.h"
#include "NaiveBayesianImpl.h"
#include "PrintCountersInformation.h"

#include <cmath>
#include <cassert>
#include <utility>
#include <iterator>
#include <functional>
#include <algorithm>
#include <numeric>

using namespace std;

namespace faif {
    namespace ml {

		NaiveBayesianClasify::NaiveBayesianClasify(NaiveBayesianImpl& parent )
			: NaiveBayesianRoot(parent) {

			calcProbabilities();
		}

		NaiveBayesianClasify::~NaiveBayesianClasify() {
		}

		/** metoda dodaje przyklad trenujacy.
			Tutaj powosduje przeladowanie klasyfikatora na obiekt, ktory sie uczy */
		void NaiveBayesianClasify::addTraining(const ExampleTrain& example) {
			parent_.switchAndTrain( example );
		}

		/** metoda klasyfikuje podany przyklad algorytmem naiwnego klasyfikatora Bayesowskiego
			na podstawie dostarczonych przykladow.
		*/
		AttrVal NaiveBayesianClasify::getCategory(const ExampleTest& example) {
			assert(! probabl_.empty());
			//przeglada kolejne kategorie i znajduje log. prawdopodobienstwa wyst. przykladu w danej kategorii
			Categories::const_iterator ii = probabl_.begin();
			AttrVal cat_val_max = (*ii).first;
			double max_prob = calcProbabilityForExample(example.get(), cat_val_max);
			++ii;
			for( ; ii != probabl_.end(); ++ii ) {
				AttrVal cat_val = (*ii).first;
				double prob = calcProbabilityForExample(example.get(), cat_val);
				if( prob > max_prob ) {
					max_prob = prob;
					cat_val_max = cat_val;
				}
			}
			assert( cat_val_max );
			return cat_val_max;
		}

		/** zwraca prawdopodobienstwo dla danej kategorii */
		double NaiveBayesianClasify::getCategoryCounter(AttrVal cat_val) const {
			return exp( getCategoryCounterLog(cat_val) );
		}

		/** zwraca prawdopodobienstwo dla danej kategorii i okreslonej wartosci atrybutu */
		double NaiveBayesianClasify::getCategoryValCounter(AttrVal cat_val, AttrVal value) const {
			return exp( getCategoryValCounterLog(cat_val,value) );
		}

		double NaiveBayesianClasify::getCategoryCounterLog(AttrVal cat_val) const {
			Categories::const_iterator ii = probabl_.find(cat_val);
			if( ii != probabl_.end() )
				return (*ii).second.data_;
			else
				return 0.0;
		}

		/** zwraca prawdopodobienstwo dla danej kategorii i okreslonej wartosci atrybutu */
		double NaiveBayesianClasify::getCategoryValCounterLog(AttrVal cat_val, AttrVal value) const {
			Categories::const_iterator ii = probabl_.find(cat_val);
			if( ii == probabl_.end() )
				return 0.0;

			const Counters& counters = (*ii).second.attrData_;
			//przeszukuje dana kategorie;
			Counters::const_iterator jj = counters.find(value);
			if( jj != counters.end() )
				return (*jj).second;
			else
				return 0.0;
		}

		/** dostarcza napisu pokazujacego stan */
		void NaiveBayesianClasify::debugString(std::ostream& os) const {
			os << "CLASIFY:" << endl;
			PrintCountersFunctor<NaiveBayesianClasify> printCounters(os, parent_.getAttributes(), *this );
			std::for_each(parent_.getCategories().begin(), parent_.getCategories().end(), printCounters );
		}

		namespace {

			/** obolicza prawdopodobienstwo wystapienia danej wartosci.
				Liczba przykladow wspierajacych ta wartosc  dzielona na liczba przykladow w danej kategorii.
				Dodatkowo m-szacowanie */
			double calcProbability( int val_count, int count_all, int val_size) {
				return log((val_count + 1)/ (double)(count_all + val_size ));
			}

			/** oblicza sume */
			int calcSum(const BayesCategories& cat) {
				int sum = 0;
				for(AttrDomain::const_iterator ii = cat.begin(); ii != cat.end(); ++ii ) {
					AttrVal cat_val = &(*ii);
					sum += cat.getCategoryCounter(cat_val);
				}
				return sum;
			}

			/** tworzy wartosc licznika prawdopodobienstw dla okreslonej wartosci atrybutu  */
			struct CalcAttrValFunctor :
				public unary_function<const AttrNominalString&, NaiveBayesianClasify::Counters::value_type> {

				const BayesCategories& categ_;
				int valSize_;
				AttrVal catVal_;
				int catCount_;

				CalcAttrValFunctor(const BayesCategories& c, int val_size, AttrVal cat_val, int cat_count)
					: categ_(c), valSize_(val_size), catVal_(cat_val), catCount_(cat_count) {
				}

				result_type operator()(argument_type value) {
					double value_prob = calcProbability( categ_.getCategoryValCounter(catVal_, &value),
														 valSize_, catCount_ );
					return result_type(&value, value_prob);
				}
			private:
				CalcAttrValFunctor& operator=(const CalcAttrValFunctor&); //zabronione przypisanie bo skladowe referencyjne
			};

			/** dodaje do licznikow prawdopodobienstw prawdopodobienstwa wartosci danego atrybutu */
			struct CalcAttrFunctor : public unary_function<const AttrDomain&, void> {

				const BayesCategories& categ_;
				NaiveBayesianClasify::Counters& counters_;
				AttrVal catVal_;
				int catCounter_;

				CalcAttrFunctor(const BayesCategories& c, NaiveBayesianClasify::Counters& counters,
								AttrVal cat_val, int cat_count)
					: categ_(c), counters_(counters), catVal_(cat_val), catCounter_(cat_count) {
				}

				result_type operator()(argument_type attr) {
					int value_size = attr.getSize(); //liczba wartosci atrybutow (do m-szacowania)
					CalcAttrValFunctor calcAttrValFunctor(categ_, value_size, catVal_, catCounter_ );
					transform(attr.begin(), attr.end(),
							  insert_iterator<NaiveBayesianClasify::Counters>(counters_, counters_.begin() ),
							  calcAttrValFunctor );
				}
			private:
				CalcAttrFunctor& operator=(const CalcAttrFunctor&); //zabronione przypisanie bo skladowe referencyjne
			};

			/** tworzy element opisujacy kategorie w klasyfikatorze na podstawie danych trenujacych */
			struct CalcCategoryFunctor : public unary_function<const AttrNominalString&,
															   NaiveBayesianClasify::Categories::value_type> {

				typedef NaiveBayesianClasify::Counters Counters;

				const BayesCategories& categ_;
				const Attributes& attribs_;
				int sum_;
				int catSize_;

				CalcCategoryFunctor(const BayesCategories& c, const Attributes& attribs, int sum, int cat_size)
					: categ_(c), attribs_(attribs), sum_(sum), catSize_(cat_size) {
				}

				result_type operator()(argument_type cat_val) {
					int cat_counter = categ_.getCategoryCounter(&cat_val);
					double cat_prob = calcProbability( cat_counter, catSize_, sum_ );
					Counters counters;
					CalcAttrFunctor calcAttrFunctor(categ_, counters, &cat_val, cat_counter );
					for_each( attribs_.begin(), attribs_.end(), calcAttrFunctor );
					return result_type(&cat_val, CategoryData<double>(cat_prob, counters) );
				}
			private:
				CalcCategoryFunctor& operator=(const CalcCategoryFunctor&); //zabronione przyp, bo skladowe referencyjne
			};

		} //namespace

		/** oblicza prawdopodobie�stwa (log. prawdopodobie�stw) dla kategorii i atrybut�w.
			Przechowuje wyniki w probabl_. */
		void NaiveBayesianClasify::calcProbabilities() {

			const BayesCategories& categ = parent_.getCategories();
			int sumTraining = calcSum(categ);
			CalcCategoryFunctor calcFunctor(categ, parent_.getAttributes(), sumTraining, categ.getSize() );
			std::transform( categ.begin(), categ.end(),
							insert_iterator<Categories>(probabl_, probabl_.begin() ),
							calcFunctor );
		}

		/** metoda pomocnicza, oblicza log. prawdopodobienstwa wystapienia przykladu w danej kategorii
			(zakladajac warunkowa niezaleznosc atrybutow - naiwny klasyfikator bayesowski!) */
		double NaiveBayesianClasify::calcProbabilityForExample(const Example& example, AttrVal cat_val) {

			double prob = getCategoryCounterLog(cat_val);
			for(Example::const_iterator ii = example.begin(); ii !=  example.end(); ++ii )
				prob += getCategoryValCounterLog(cat_val, *ii );
			return prob;
		}
    } //namespace ml
} //namespace faif

