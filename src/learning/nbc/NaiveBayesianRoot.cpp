#include "NaiveBayesianRoot.h"
#include "NaiveBayesianImpl.h"

namespace faif {
    namespace ml {

		NaiveBayesianRoot::NaiveBayesianRoot(NaiveBayesianImpl& parent )
			: parent_(parent) {
		}

		NaiveBayesianRoot::~NaiveBayesianRoot() {
		}

		/** funkcja globalna wyswietlajaca stan obiektu */
		std::ostream& operator<<(std::ostream& os, const NaiveBayesianRoot& nb) {
			nb.debugString(os);
			return os;
		}

    } //namespace ml
} //namespace faif
