
#include "NaiveBayesianTraining.h"
#include "NaiveBayesianImpl.h"
#include "PrintCountersInformation.h"

#include <algorithm>

using namespace std;

namespace faif {
    namespace ml {


		NaiveBayesianTraining::NaiveBayesianTraining( NaiveBayesianImpl& parent )
			: NaiveBayesianRoot(parent) {
		}

		NaiveBayesianTraining::~NaiveBayesianTraining() {
		}

		/** metoda dodaje przyklad trenujacy */
		void NaiveBayesianTraining::addTraining(const ExampleTrain& example) {
			parent_.getCategories().add(example);
		}

		/** metoda klasyfikuje podany przyklad algorytmem naiwnego klasyfikatora Bayesowskiego
			na podstawie dostarczonych przykladow.
			Tutaj powoduje przeladowanie klasyfikatora na obiekt, ktory klasyfikuje (ten tutaj sie uczy )
		*/
		AttrVal NaiveBayesianTraining::getCategory(const ExampleTest& example) {
			return parent_.switchAndClasify(example);
		}

		/** dostarcza napisu pokazujacego stan */
		void NaiveBayesianTraining::debugString(std::ostream& os) const {
			os << "TRAINING: " << std::endl;
			PrintCountersFunctor<BayesCategories> printCounters(os, parent_.getAttributes(), parent_.getCategories() );
			std::for_each(parent_.getCategories().begin(), parent_.getCategories().end(), printCounters );
		}


    } //namespace ml
} //namespace faif
