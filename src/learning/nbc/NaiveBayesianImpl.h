#ifndef NBC_NAIVE_BAYESIAN_IMPL_H_
#define NBC_NAIVE_BAYESIAN_IMPL_H_

#include <memory>
#include <list>

#include "BayesCategories.h"
#include "NaiveBayesianRoot.h"

namespace faif {
	namespace ml {

		//musi zapewnic niezmiennosc wskaznikow na elementy
		typedef std::list<AttrDomain> Attributes;

		/** klasa dostarczajaca implementacji naiwnego klasyfikatra Bayesowskiego.
			Agreguje atrybuty, wartosci i kategorie oraz jeden z obiektow przetwarzajacych
			informacje (implementujacy interfejs NaiveBayesianRoot )
		*/
		class NaiveBayesianImpl
		{
		public:
			NaiveBayesianImpl();
			~NaiveBayesianImpl();

			/** accessor, dostarcza atrybutow */
			Attributes& getAttributes(){ return attributes_; }
			/** accessor, dostarcza atrybutow */
			BayesCategories& getCategories(){ return categories_; }

			/** zmienia obiekt przetwarzajacy na klasyfikator uczacy sie i dodaje przyklad */
			void switchAndTrain(const ExampleTrain& example);
			/** zmienia obiekt przetwarzajacy na klasyfikator rozpoznajacy i klasyfikuje */
			AttrVal switchAndClasify(const ExampleTest& example);

			/** metoda zmienia stan klasyfikatora na taki, jaki byl przed nauczeniem sie czegokolwiek */
			void reset();

			/** metoda dodaje przyklad trenujacy. */
			void addTraining(const ExampleTrain& example);

			/** metoda klasyfikuje podany przyklad */
			AttrVal getCategory(const ExampleTest& example);

			/** globalna funkcja, wypisuje zawartosc klasyfikatora */
			friend std::ostream& operator<<(std::ostream& os, const NaiveBayesianImpl& nb);
		private:
			/** noncopyable */
			NaiveBayesianImpl(const NaiveBayesianImpl&);
			/** noncopyable */
			NaiveBayesianImpl& operator=(const NaiveBayesianImpl&);

			Attributes attributes_; //!< przechowuje atrybuty i wartosci
			BayesCategories categories_; //!< przechowuje kategorie i odp. liczniki
			std::auto_ptr<NaiveBayesianRoot> classifier_; //!< obiekt przetwarzajacy informacje
		};
	} //namespace ml
} //namespace faif

#endif //NBC_NAIVE_BAYESIAN_IMPL_H_
