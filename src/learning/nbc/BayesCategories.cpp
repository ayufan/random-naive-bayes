#include <utility>
#include <algorithm>
#include <cassert>

#include "BayesCategories.h"

using namespace std;

namespace faif {
    namespace ml {

		/** klasa agreguje kategorie (wraz z odp. licznikami) dla klasyfikatora bayesowskiego */
		BayesCategories::BayesCategories(const std::string& id)
			: domain_(id) {
		}

		BayesCategories::BayesCategories(const BayesCategories& right)
			: domain_(right.domain_), categories_(right.categories_ ) {
		}

		BayesCategories& BayesCategories::operator=(const BayesCategories& right) {
			domain_ = right.domain_;
			categories_ = right.categories_;
			return *this;
		}

		BayesCategories::~BayesCategories() { }

		/** tworzy nowa wartosc dla atrybutu (kategorii), lub zwraca istniejaca */
		AttrDomain::iterator BayesCategories::insert(const std::string& id) {

			//wstawia wartosc atrybutu
			return domain_.insert(id);

			//UWAGA! nie wstawia do licznikow
		}

		/** usuwa wartosc dla atrybutu (kategorii) */
		void BayesCategories::remove(AttrVal value) {
			domain_.remove(value->get() );
			categories_.erase(value);
		}

		/** zwraca wartosc licznika dla danej kategorii */
		int BayesCategories::getCategoryCounter(AttrVal cat_val) const {
			Categories::const_iterator ii = categories_.find(cat_val);
			if( ii != categories_.end() )
				return (*ii).second.data_;
			else
				return 0;
		}

		/** zwraca wartosc odp. licznika (dla danej kategorii i okreslonej wartosci atrybutu) */
		int BayesCategories::getCategoryValCounter(AttrVal cat_val, AttrVal value) const {
			Categories::const_iterator ii = categories_.find(cat_val);
			if( ii == categories_.end() )
				return 0;

			const Counters& counters = (*ii).second.attrData_;
			//przeszukuje dana kategorie;
			Counters::const_iterator jj = counters.find(value);
			if( jj != counters.end() )
				return (*jj).second;
			else
				return 0;
		}

		namespace {
			/** dodaje wartosc z przykladu trenujacego do odpowiednich licznikow */
			struct AddExampleValFunctor {
				BayesCategories::Counters& counters_;

				AddExampleValFunctor(BayesCategories::Counters& counters)
					: counters_(counters) {
				}
				void operator()(AttrVal value) {
					BayesCategories::Counters::iterator ii = counters_.find(value);
					if( ii != counters_.end() )
						++(*ii).second; //zwieksza odpowiedni licznik
					else
						counters_.insert( pair<AttrVal,int>(value,1) );
				}
			private:
				AddExampleValFunctor& operator=(const AddExampleValFunctor&); //nie mozna przyp., bo skl. referencyjne
			};
		}

		/** dodaje przyklad trenujacy do danej kategorii */
		void BayesCategories::add(const ExampleTrain& example) {

			AttrVal cat_val = example.getCategory();
			Categories::iterator ii = categories_.find(cat_val);
			if( ii != categories_.end() )
				++(*ii).second.data_;
			else
				ii = categories_.insert( Categories::value_type(cat_val,1) ).first;
			assert( ii != categories_.end() );
			Counters& counters = (*ii).second.attrData_;
			AddExampleValFunctor addValFunctor(counters);
			for_each(example.get().begin(), example.get().end(), addValFunctor );
		}

		/** usuwa wszelkie dane zapamietane podczas uczenia sie */
		void BayesCategories::reset() {
			categories_.clear();
		}


		/** ostream operator */
		std::ostream& operator<<(std::ostream& os, const BayesCategories& bc) {
			os << bc.domain_;
			return os;
		}


    } //namespace ml
} //namespace faif
