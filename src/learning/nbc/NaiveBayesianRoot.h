#ifndef NBC_NAIVE_BAYESIAN_ROOT_H_
#define NBC_NAIVE_BAYESIAN_ROOT_H_

#include <iostream>
#include "../ExampleTest.h"
#include "../ExampleTrain.h"

namespace faif {
	namespace ml {

		class NaiveBayesianImpl;

		/** klasa bazowa dla klas implementujacych naiwny klasyfikator Bayesowski.
			Klasa dostarcza danych. Klasy konkretne to NaiveBayesianTraining, NaiveBayesianClassifier
		*/
		class NaiveBayesianRoot {
		public:
			NaiveBayesianRoot(NaiveBayesianImpl& parent );
			virtual ~NaiveBayesianRoot();

			/** metoda dodaje przyklad trenujacy. */
			virtual void addTraining(const ExampleTrain& example) = 0;

			/** metoda klasyfikuje podany przyklad */
			virtual AttrVal getCategory(const ExampleTest& example) = 0;

			/** dostarcza napisu pokazujacego stan */
			virtual void debugString(std::ostream& os) const = 0;
		protected:
			NaiveBayesianImpl& parent_;
		private:
			/** noncopyable */
			NaiveBayesianRoot(const NaiveBayesianRoot&);
			/** noncopyable */
			NaiveBayesianRoot& operator=(const NaiveBayesianRoot&);
		};

		/** funkcja globalna wyswietlajaca stan obiektu */
		std::ostream& operator<<(std::ostream& os, const NaiveBayesianRoot& nb);

	} //namespace ml
} //namespace faif

#endif //NBC_NAIVE_BAYESIAN_ROOT_H_
