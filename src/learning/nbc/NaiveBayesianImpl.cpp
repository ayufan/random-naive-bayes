// Implementacja naiwnego klasyfikatra Bayesowskiego.

#include <cassert>
#include <iterator>

#include "NaiveBayesianImpl.h"
#include "NaiveBayesianTraining.h"
#include "NaiveBayesianClasify.h"

using namespace std;

namespace faif {
    namespace ml {


		NaiveBayesianImpl::NaiveBayesianImpl()
			: attributes_(),
			  categories_("")
			  //classifier_(new NaiveBayesianTraining(*this) )
		{
			classifier_.reset( new NaiveBayesianTraining(*this) );
		}

		NaiveBayesianImpl::~NaiveBayesianImpl() {
		}

		/** zmienia obiekt przetwarzajacy na klasyfikator uczacy sie */
		void NaiveBayesianImpl::switchAndTrain(const ExampleTrain& example) {
			classifier_.reset( new NaiveBayesianTraining(*this) );
			addTraining( example );
		}

		/** zmienia obiekt przetwarzajacy na klasyfikator rozpoznajacy */
		AttrVal NaiveBayesianImpl::switchAndClasify(const ExampleTest& example) {
			classifier_.reset( new NaiveBayesianClasify(*this) );
			return getCategory( example );
		}

		/** metoda zmienia stan klasyfikatora na taki, jaki byl przed nauczeniem sie czegokolwiek.
			Zostaja tylko nazwy kategorii. */
		void NaiveBayesianImpl::reset() {
			classifier_.reset( new NaiveBayesianTraining(*this) );
			categories_.reset();
		}

		/** metoda dodaje przyklad trenujacy. */
		void NaiveBayesianImpl::addTraining(const ExampleTrain& example) {
			classifier_->addTraining(example);
		}

		/** metoda klasyfikuje podany przyklad */
		AttrVal NaiveBayesianImpl::getCategory(const ExampleTest& example) {
			return classifier_->getCategory(example);
		}

		/** globalna funkcja, wypisuje zawartosc klasyfikatora */
		std::ostream& operator<<(std::ostream& os, const NaiveBayesianImpl& nb) {

			os << "Categories(" << nb.categories_.getSize() << ")" << nb.categories_ << std::endl;

			os << "Attributes(" << (int)nb.attributes_.size() << "):" << std::endl;
			std::copy(nb.attributes_.begin(), nb.attributes_.end(), std::ostream_iterator<AttrDomain>(os,"\n") );

			os << "State: " << *(nb.classifier_) << std::endl;
			return os;
		}


    } //namespace ml
} //namespace faif
