#ifndef NBC_BAYES_CATEGORIES_H_
#define NBC_BAYES_CATEGORIES_H_

#include <map>
#include "../ExampleTrain.h"
#include "CategoryData.h"

namespace faif {
    namespace ml {


		/** \brief categories with counters for Naive Bayes Classifier */
		class BayesCategories {
		public:
			/** the category type */
			typedef std::map<AttrVal,CategoryData<int> > Categories;
			typedef CategoryData<int>::AttrData Counters;

			BayesCategories(const std::string&);
			BayesCategories(const BayesCategories&);
			BayesCategories& operator=(const BayesCategories&);
			~BayesCategories();

			/** tworzy nowa wartosc dla atrybutu (kategorii), lub zwraca istniejaca */
			AttrDomain::iterator insert(const std::string& id);

			/** usuwa wartosc dla atrybutu (kategorii) */
			void remove(AttrVal cat_val);

			/** zwraca wartosc licznika dla danej kategorii */
			int getCategoryCounter(AttrVal cat_val) const;

			/** zwraca wartosc odp. licznika (dla danej kategorii i okreslonej wartosci atrybutu) */
			int getCategoryValCounter(AttrVal cat_val, AttrVal value) const;

			/** dodaje przyklad trenujacy do danej kategorii */
			void add(const ExampleTrain& example);

			/** usuwa wszelkie dane zapamietane podczas uczenia sie */
			void reset();

			/** accessor */
			int getSize() const { return domain_.getSize(); }

			/** \brief accessor */
			AttrDomain::const_iterator begin() const { return domain_.begin(); }
			/** \brief accessor */
			AttrDomain::const_iterator end() const { return domain_.end(); }

			/** ostream operator */
			friend std::ostream& operator<<(std::ostream& os, const BayesCategories& bc);
		private:
			AttrDomain domain_;

			/** przechowuje dodatkowe informacje - liczniki dla wystapienia kazdego atrybutu w danej kategorii */
			Categories categories_;
		};

		/** ostream operator */
		std::ostream& operator<<(std::ostream& os, const BayesCategories& bc);

    } //namespace ml
} //namespace faif

#endif //NBC_BAYES_CATEGORIES_H_
