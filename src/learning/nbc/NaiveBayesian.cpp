/**
   Plik zawiera class NaiveBayesian.
*/

#include <algorithm>

#include "../NaiveBayesian.h"
#include "NaiveBayesianImpl.h"

namespace faif {
    namespace ml {


		NaiveBayesian::NaiveBayesian()
			: impl_(new NaiveBayesianImpl() ) {
		}

		NaiveBayesian::~NaiveBayesian() {
		}

		/** metoda definiuje nowy atrybut lub zwraca istniejacy */
		AttrDomain& NaiveBayesian::insertAttr(const std::string& id) {

			Attributes& attribs = impl_->getAttributes();
			AttrDomain tmp(id);
			Attributes::iterator found = std::find(attribs.begin(), attribs.end(), tmp );
			if(found != attribs.end() )
				return *found; //jezeli znalazl - to zwraca odp. iterator

			//jezeli nie znalazl - to wstawia na koniec
			attribs.push_back(tmp);
			return attribs.back();
		}

		/** metoda znajduje atrybut o danym identyfikatorze.
			Jezeli nie istnieje generuje wyjatek NotFoundException */
		AttrDomain& NaiveBayesian::findAttrRef(const std::string& id) {
			Attributes& attribs = impl_->getAttributes();
			AttrDomain tmp(id);
			Attributes::iterator found = std::find(attribs.begin(), attribs.end(), tmp );
			if(found == attribs.end() )
				throw NotFoundException("classifier");
			return *found;
		}


		/** metoda definiuje nowa kategorie lub zwraca istniejaca */
		AttrDomain::iterator NaiveBayesian::insertCategory(const std::string& id) {
			return impl_->getCategories().insert(id);
		}

		void NaiveBayesian::reset() {
			return impl_->reset();
		}

		/** metoda dodaje przyklad trenujacy */
		void NaiveBayesian::addTraining(const ExampleTrain& example) {
			impl_->addTraining(example);
		}

		/** metoda klasyfikuje podany przyklad algorytmem naiwnego klasyfikatora Bayesowskiego
			na podstawie dostarczonych przykladow
		*/
		const AttrNominalString* NaiveBayesian::getCategory(const ExampleTest& example) const {
			return impl_->getCategory(example);
		}

		/** globalna funkcja, wypisuje zawartosc klasyfikatora */
		std::ostream& operator<<(std::ostream& os, const NaiveBayesian& nb) {
			os << (*nb.impl_);
			return os;
		}

    } //namespace ml
} //namespace faif
