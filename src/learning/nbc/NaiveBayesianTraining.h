#ifndef NBC_NAIVE_BAYESIAN_TRAINING_H
#define NBC_NAIVE_BAYESIAN_TRAINING_H

#include "NaiveBayesianRoot.h"

namespace faif {
	namespace ml {

		/** implementacja naiwnego klasyfikatora Bayesowskiego.
			Klasa konkretna: klasyfikator, ktory sie uczy. */
		class NaiveBayesianTraining : public NaiveBayesianRoot {

		public:
			NaiveBayesianTraining(NaiveBayesianImpl& parent );
			virtual ~NaiveBayesianTraining();

			/** metoda dodaje przyklad trenujacy */
			void addTraining(const ExampleTrain& example);

			/** metoda klasyfikuje podany przyklad algorytmem naiwnego klasyfikatora Bayesowskiego
				na podstawie dostarczonych przykladow.
				Tutaj powoduje przeladowanie klasyfikatora na obiekt, ktory klasyfikuje (ten tutaj sie uczy )
			*/
			virtual AttrVal getCategory(const ExampleTest& example);

			/** dostarcza napisu pokazujacego stan */
			virtual void debugString(std::ostream& os) const;
		private:
			/** noncopyable */
			NaiveBayesianTraining(const NaiveBayesianTraining&);
			/** noncopyable */
			NaiveBayesianTraining& operator=(const NaiveBayesianTraining&);
		};

	} //namespace ml
} //namespace faif

#endif //NBC_NAIVE_BAYESIAN_TRAINING_H
