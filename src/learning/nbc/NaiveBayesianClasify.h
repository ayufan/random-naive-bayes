#ifndef NBC_NAIVE_BAYESIAN_CLASIFY_H
#define NBC_NAIVE_BAYESIAN_CLASIFY_H

#include <map>
#include <cmath>

#include "NaiveBayesianRoot.h"
#include "CategoryData.h"

namespace faif {
	namespace ml {

		/** implementacja naiwnego klasyfikatora Bayesowskiego.
			Klasa konkretna: klasyfikator, ktory klasyfikuje (nie uczy sie). */
		class NaiveBayesianClasify : public NaiveBayesianRoot {

		public:
			typedef std::map<AttrVal,CategoryData<double> > Categories;
			typedef CategoryData<double>::AttrData Counters;

			NaiveBayesianClasify(NaiveBayesianImpl& parent );
			virtual ~NaiveBayesianClasify();

			/** metoda dodaje przyklad trenujacy.
				Tutaj powosduje przeladowanie klasyfikatora na obiekt, ktory sie uczy */
			void addTraining(const ExampleTrain& example);

			/** metoda klasyfikuje podany przyklad algorytmem naiwnego klasyfikatora Bayesowskiego
				na podstawie dostarczonych przykladow.
			*/
			virtual AttrVal getCategory(const ExampleTest& example);

			/** zwraca prawdopodobienstwo dla danej kategorii. Wykorzystuje getCategoryCounterLog */
			double getCategoryCounter(AttrVal cat_val) const;

			/** zwraca prawdopodobienstwo dla danej kategorii i okreslonej wartosci atrybutu.
				Wykorzystuje getCategoryValCounterLog */
			double getCategoryValCounter(AttrVal cat_val, AttrVal value) const;

			/** zwraca log. prawdopodobienstwa. Szybsza, bo przechowuje logarytmy. */
			double getCategoryCounterLog(AttrVal cat_val) const;

			/** zwraca log. prawdopodobienstwa. Szybsza, bo przechowuje logarytmy. */
			double getCategoryValCounterLog(AttrVal cat_val, AttrVal value) const;

			/** dostarcza napisu pokazujacego stan */
			virtual void debugString(std::ostream& os) const;
		private:
			/** noncopyable */
			NaiveBayesianClasify(const NaiveBayesianClasify&);
			/** noncopyable */
			NaiveBayesianClasify& operator=(const NaiveBayesianClasify&);

			/** oblicza prawdopodobie�stwa (log. prawdopodobie�stw) dla kategorii i atrybut�w.
				Przechowuje wyniki w probabl_. */
			void calcProbabilities( );

			/** metoda pomocnicza, oblicza log. prawdopodobienstwa wystapienia przykladu w danej kategorii
				(zakladajac warunkowa niezaleznosc atrybutow - naiwny klasyfikator bayesowski!) */
			double calcProbabilityForExample(const Example& example, AttrVal cat_val);

			/** przechowuje logarytmy prawdopodobie�stw odp. kategorii */
			Categories probabl_;
		};

	} //namespace ml
} //namespace faif

#endif //NBC_NAIVE_BAYESIAN_CLASIFY_H
