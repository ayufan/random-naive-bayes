#include "ExampleTest.h"

#include <algorithm>
#include <iterator>

using namespace std;

namespace faif {
    namespace ml {


		namespace {
			//nie moge tego znalezc, ale pewnie jest w std lub boost
			template <class T> const T& point2ref(const T* x){ return *x; }
		}


		/** klasa reprezentuje przyklad testujacy (kolekcja wartosci atrybutow) */
		ExampleTest::ExampleTest()
			: example_() {
		}

		ExampleTest::ExampleTest(const std::vector<AttrVal>& exampl)
			: example_(exampl) {
		}

		ExampleTest::ExampleTest(const ExampleTest& right)
			: example_(right.example_) {
		}

		ExampleTest& ExampleTest::operator=(const ExampleTest& right) {
			example_ = right.example_;
			return *this;
		}

		ExampleTest::~ExampleTest() {
		}

		bool ExampleTest::operator==(const ExampleTest& right) const {
			// equal porownuje zgodnosc poczatkowych wyrazow, wiec musze badac zgodnosc rozmiaru
			if( example_.size() != right.example_.size() )
				return false;

			return equal( example_.begin(), example_.end(), right.example_.begin() );
		}

		/** pisze do strumienia zawartosc (poszczegolne wartosci atrybutow) */
		void ExampleTest::write(std::ostream& os) const {
			std::transform(example_.begin(), example_.end(),
						   std::ostream_iterator<AttrNominalString>(os," "), point2ref<AttrNominalString> );
		}


		/** dla wygody - operator strumieniowy (wola metode write) */
		ostream& operator<<(std::ostream& os, const ExampleTest& ex) {
			ex.write(os);
			return os;
		}

    } //namespace ml
} //namespace faif

