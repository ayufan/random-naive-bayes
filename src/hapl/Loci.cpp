#include "Loci.h"

#include <boost/lexical_cast.hpp>

using namespace std;
using namespace boost;

namespace faif {
    namespace hapl {


		/** \brief ostream operator */
		std::ostream& operator<<(std::ostream& os, const AlleleImpl& a) {
			os << a.first;
			if(a.second)
				os  << "(silent)";
			return os;
		}


        /** \brief accessor */
        std::string getName(const Allele& a) {
            return a.get().first;
        }

        /** \brief accessor */
        bool isSilent(const Allele& a) {
            return a.get().second;
        }

        namespace {

            /** \brief generates the allele name */
            string generateAlleleName(const string& locus_name, int allele_number) {
                return locus_name + lexical_cast<string>(allele_number);
            }

        }



        /** \brief helping method - create the entire locus, with number variants.

            Variants name are the locus name and the variant number,
            i.e. silent variant has name 'name0', first non silent variant has name 'name1' etc.
        */
        Locus createLocus(const std::string& name, int num_variants, bool has_silent) {

            Locus l(name);
            int num = 0;
            //the silent allele
            if(has_silent)
                l.insert( AlleleImpl(generateAlleleName(name, num), true) );

            //not sillent alleles
            for(num = 1; num < num_variants; num++) {
                l.insert( AlleleImpl( generateAlleleName(name, num), false) );
            }

            //if the locus does not have silent allele, another allele (because always create num alleles)
            if(!has_silent)
                l.insert( AlleleImpl( generateAlleleName(name, num), false) );

            return l;
        }

        /** \brief helping, accessor
            \throws NotFoundException if locus does not have allele of given name
        */
        Allele& findRef(Locus& locus, const std::string& name) {
            return findRef( locus, AlleleImpl(name) );
        }


		namespace {

			//search the given locus to find the allele
			struct FoundFunctor {
				//the place for found pointer
				Allele** found_allele_;
				AlleleImpl pattern_;

				FoundFunctor(const std::string& pattern_id, Allele** found_allele)
					: found_allele_(found_allele), pattern_(pattern_id) {}

				bool operator()(Locus& locus) {
					Locus::iterator it = locus.find( pattern_);
					if( it != locus.end() ) {
						*found_allele_ = &(*it);
						return true;
					}
					*found_allele_ = 0L;
					return false;
				}
			};

		} //namespace


		/** \brief search all loci for allele of given name.
			return nullptr if not found
		*/
        Allele* Loci::findAllele(const std::string& str) {
			Allele* found_allele = 0L;
			FoundFunctor fun( str, &found_allele);
			find_if( begin(), end(), fun );
			return found_allele;
        }

		/** \brief search all loci for allele of given name.
			\throws NotFoundException
		*/
        Allele& Loci::findAlleleRef(const std::string& str) {
			Allele* allele = findAllele(str);
			if(allele == 0L)
				throw NotFoundException(str.c_str());
			else
				return *allele;
        }

		/** operator strumieniowy dla loci */
		std::ostream& operator<<(std::ostream& os, const Loci& loci) {
			copy(loci.begin(), loci.end(), std::ostream_iterator<Locus>(cout,";") );
			return os;
		}

    } //namespace hapl
} //namespace faif

