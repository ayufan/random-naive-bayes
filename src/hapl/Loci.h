#ifndef FAIF_HAPL_LOCI_H
#define FAIF_HAPL_LOCI_H

#include <vector>

#include "../Attribute.h"
#include "../Domain.h"

namespace faif {

    /** \brief Population genetics (haplotype and marker analyzis) primitives and algorithms  */
    namespace hapl {

        /** \brief the helping structure, implements the Allele members */
        struct AlleleImpl : public std::pair<std::string, bool> {

            /** \brief c-tor */
            AlleleImpl(const std::string& name = std::string(""), bool is_silent = false)
                : std::pair<std::string,bool>(name, is_silent) {}

            /** \brief equality comparison */
            bool operator==(const AlleleImpl& a) const {
                return first == a.first;
            }

            /** \brief non-equality comparison */
            bool operator!=(const AlleleImpl& a) const {
                return first != a.first;
            }
        };

		/** \brief ostream operator */
		std::ostream& operator<<(std::ostream& os, const AlleleImpl& a);

        /** \brief Allele, variant of a gene */
        typedef AttrNominal<AlleleImpl> Allele;

        /** \brief accessor */
        std::string getName(const Allele& a);

        /** \brief accessor */
        bool isSilent(const Allele& a);

        /** \brief Locus, place on DNA contatining the Allele */
        typedef DomainEnumerate<Allele> Locus;

        /** \brief helping method - create the entire locus, with number variants.

            Variants name are the locus name and the variant number,
            i.e. silent variant has name 'name0', first non silent variant has name 'name1' etc.
        */
        Locus createLocus(const std::string& name, int num_variants, bool has_silent);

        /** \brief helping, accessor
            \throws NotFoundException if locus does not have allele of given name
        */
        Allele& findRef(Locus& locus, const std::string& name);

        /** \brief Loci, the collection(sequence) of Locus, e.g. genotype */
        struct Loci : public std::vector<Locus> {

            /** \brief search all loci for allele of given name.
                return nullptr if not found
            */
			Allele* findAllele(const std::string& str);

            /** \brief search all loci for allele of given name.
                \throws NotFoundException when not found
            */
            Allele& findAlleleRef(const std::string& str);

        };

        /** ostream operator for loci */
        std::ostream& operator<<(std::ostream& os, const Loci& hapl);

    } //namespace hapl
} //namespace faif

#endif
