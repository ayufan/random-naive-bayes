#ifndef FAIF_EXCEPTIONS_H_
#define FAIF_EXCEPTIONS_H_

//   the base exception class for faif library

#include <exception>
#include <iostream>

namespace faif {

    /** \brief the base exception class for faif library */
    class FaifException : public std::exception {
	public:
		FaifException(){}
		virtual ~FaifException() throw() {}
		virtual const char *what() const throw() { return "FaifException"; }

		/** \brief the exception info written to ostream */
		virtual std::ostream& print(std::ostream& os) const throw();
	};

	/** \brief the exception thrown when the value for given attribute is not found  */
	class NotFoundException : public FaifException {
	public:
		NotFoundException(const char* domain_id);
		virtual ~NotFoundException() throw() {}
		virtual const char *what() const throw(){ return "NotFoundException"; }
		virtual std::ostream& print(std::ostream& os) const throw();
	private:
		// the std::string is not used
		static const int SIZE = 30;
		char domainID_[SIZE];
	};

	//the global function called ex.print(os)
	std::ostream& operator<<(std::ostream& os, const FaifException& ex);

} //namespace faif

#endif //FAIF_EXCEPTIONS_H_
