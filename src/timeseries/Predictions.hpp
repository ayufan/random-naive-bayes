#ifndef FAIF_TS_PREDICTIONS
#define FAIF_TS_PREDICTIONS

// file with predictions -> calculates the digit time series based on history

#include <ostream>
#include <vector>

#include <boost/noncopyable.hpp>
#include <boost/tuple/tuple.hpp>

#include "TimeSeries.h"

namespace faif {
    namespace timeseries {

        /* forward declaration */
        class PredictionVisitor;

        /** base class to comp block */
        class Prediction : boost::noncopyable {
        public:
            Prediction(const TimeSeriesDigit& history) : history_(history), default_(0, 0.0) { }

            virtual ~Prediction(){ }

            /** accessor */
            const TimeSeriesDigit& getHistory() const { return history_; }

            /** the visitor pattern for Prediction hierarchy */
            virtual void accept(PredictionVisitor& v) const = 0;

			/** find the value for t (the lower_bound) */
			const TimeValueDigit& getHistoricalValue(DigitTime t) const;

            /** calculate the prediction for period <from, to>,
                values for negative timestamp are readed from the history
            */
            TimeSeriesDigit calculatePrediction(DigitTime from, DigitTime to);
        private:
			TimeSeriesDigit history_; // \brief historic data
			TimeValueDigit default_; //default value

            /** calculate the prediction for f(t), f(t+1), ... f(t+n), calculates colection of probes.
                It isa always prediction, because the period is <0,n> */
            virtual TimeSeriesDigit doCalcPrediction(DigitTime n) = 0;
        };

        /* forward declaration */
        class PredictionAR;
        /* forward declaration */
        class PredictionKNN;

        /** base visitor of prediction hierarchy */
        class PredictionVisitor {
        public:
            virtual void visit(const PredictionAR& v) = 0;
            virtual void visit(const PredictionKNN& v) = 0;
        };

        //ostream operator (to debug)
        std::ostream& operator<<(std::ostream& os, const Prediction& pred);

		/**
		   \brief the AR parameter collection
		   f(t) = a[0]f(t-1) + a[1]f(t-2) + ... a[n-1]f(t-n)
		*/
		typedef std::vector<double> ARDef;

		/**
		   \brief the KNN parameters, k = num_neighbours, ref_size = size of reference block
		*/
		struct KNNDef : public boost::tuple<int,size_t> {

			/** c-tor */
			KNNDef(int k, size_t ref_size) : boost::tuple<int,size_t>(k, ref_size) {}
			/** accessor */
			int getK() const { return get<0>(); }
			/** accessor */
			size_t getRefSize() const { return get<1>(); }
		};


		/** the auto-regressive (AR) computation block

			f(t+1) = a(t)f(t) + a(t-1)f(t-1) + ... + a(t-n+1)f(t-n+1)
			or
			f(t+1) = a[0]f(t) + a[1]f(t-1) + ... a[n-1]f(t-n+1)
		 */
		class PredictionAR : public Prediction {
		public:
			/** c-tor */
			PredictionAR(const TimeSeriesDigit& history, const ARDef& definition)
				: Prediction(history), definition_(definition)
			{ }

			/** d-tor */
			virtual ~PredictionAR() {}

			/** visitor pattern */
			virtual void accept(PredictionVisitor& v) const { v.visit(*this); }

			/** accessor */
			const ARDef& getARDef() const { return definition_; }
		private:
			ARDef definition_; //opis modelu auto-regresywnego (wspolczynniki)
			TimeSeriesDigit predictions_; //the prediction

			/** return the TimeValue for time t. The ranges are not checked (if bad -- assertion) */
			const TimeValueDigit& get(DigitTime t) const;

			/** prediction calculated by AR (n >= 0) */
			TimeValueDigit calculateAR(DigitTime time) const;

			/** calculate the prediction for f(t), f(t+1), ... f(t+n), calculates colection of probes  */
			virtual TimeSeriesDigit doCalcPrediction(DigitTime n);
		};

		/** to debug */
		std::ostream& operator<<(std::ostream& os, const PredictionAR& ar);

		/** memory based predictor */
		class PredictionKNN : public Prediction {
		public:
			/* c-tor */
			PredictionKNN(const TimeSeriesDigit& history, const KNNDef& definition);
			/* d-tor */
			virtual ~PredictionKNN() {}
			/** accessor */
			const KNNDef& getKNNDef() const { return definition_; }

			// \brief visitor pattern
			virtual void accept(PredictionVisitor& v) const { return v.visit(*this); }
		private:
			KNNDef definition_; //KNN definition
			TimeSeriesDigit reference_; //KNN reference block
			TimeSeriesDigit predictions_; //the prediction

			/** return the TimeValue for time t. The ranges are not checked (if bad -- assertion) */
			const TimeValueDigit& get(DigitTime t) const;

			/** calculate the prediction for f(t), f(t+1), ... f(t+n), calculates colection of probes  */
			virtual TimeSeriesDigit doCalcPrediction(DigitTime n);
		};

		/** to debugging */
		std::ostream& operator<<(std::ostream& os, const PredictionKNN& knn);

	} //namespace timeseries
} //namespace faif


#endif //FAIF_TS_PREDICTIONS
