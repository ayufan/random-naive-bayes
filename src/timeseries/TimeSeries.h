#ifndef FAIF_TIME_SERIES
#define FAIF_TIME_SERIES

// the file with timeseries tools

#if defined(_MSC_VER) && (_MSC_VER >= 1400)
//msvc8.0 warnings for boost::date_time
#pragma warning(disable:4100)
#pragma warning(disable:4512)
#pragma warning(disable:4127)
#pragma warning(disable:4245)
//msvc8.0 warnings for std::transform
#pragma warning(disable:4996)
#endif

#include <ostream>
#include <vector>
#include <cmath>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace faif {
	/** \brief TimeSeries (collection of triples<timestamp, value, quality>) tools
	 */
    namespace timeseries {

        /** \brief the real time type */
        typedef boost::posix_time::ptime RealTime;
        /** \brief the real time duration */
        typedef boost::posix_time::time_duration RealDuration;

        /** \brief convert from RealTime to posix_t (num of seconds from 1970) */
        long to_time_t(const RealTime& real_time);

        /** \brief digit time type.
            DigitTime < 0 past, DigitTime >= 0 future, DigitTime == 0 now. */
        typedef int DigitTime;
        /** \brief value - real number */
        typedef double Value;
        /** \brief quality - real  0..1 */
        typedef double Quality;

        /** \brief timeseries value, single value in given real time. Plain old data */
        struct TimeValueReal : public boost::tuple<RealTime,Value,Quality> {

            /** c-tor */
			TimeValueReal(RealTime t, const Value& v, const Quality& q = 0.0)
				: boost::tuple<RealTime,Value,Quality>(t,v,q)
            {}
            /** accessor */
            const RealTime& getTime() const { return get<0>(); }
            /** accessor */
            const Value& getValue() const { return get<1>(); }
            /** accessor */
            const Quality& getQuality() const { return get<2>(); }

        };
        /** (for debugging) */
        std::ostream& operator<<(std::ostream& os, const TimeValueReal& time_val);


        /** \brief timeseries - time hold as RealTime */
		class TimeSeriesReal : public std::vector<TimeValueReal> {
		public:
			typedef std::vector<TimeValueReal>::iterator iterator;
			typedef std::vector<TimeValueReal>::const_iterator const_iterator;

            /** \brief empty collection */
            TimeSeriesReal(){}

            /** \brief copy c-tor */
			TimeSeriesReal(const TimeSeriesReal& t) : std::vector<TimeValueReal>(t) {}

            /** \brief c-tor from a range */
			TimeSeriesReal(const TimeValueReal* begin, const TimeValueReal* end)
				: std::vector<TimeValueReal>(begin,end) {}
			/** \brief c-tor from a range */
			TimeSeriesReal(const_iterator begin, const_iterator end)
				: std::vector<TimeValueReal>(begin,end) {}

			/** \brief c-tor from a C style table of timestamps (as long - posix time), and C style table of values */
			TimeSeriesReal(const long* time_begin, const long* time_end, const Value* value_begin);

			/** \brief c-tor from C style table of values.
				The timestamps are calculated from start_time and delta */
			TimeSeriesReal(const RealTime& start_time, const RealDuration& delta,
						   const Value* value_begin, const Value* value_end );

            /** \brief c-tor from timeseries - the timestams are modified (the offset is added) */
            TimeSeriesReal(const TimeSeriesReal& ts, const RealTime& offset);
            /** operator = */
            TimeSeriesReal& operator=(const TimeSeriesReal& t){
                std::vector<TimeValueReal>::operator=(t);
                return *this;
            }
			/** d-tor */
            ~TimeSeriesReal(){}

            /** the sum of values of timeseries */
            double getSum() const;

            /** the average of values of timeseries, 0.0 if the timeseries is empty. */
            double getAvg() const { return empty() ? 0.0 : getSum()/size(); }

			/** the integral (the area under the line) for timeseries */
			double getIntegral() const;
		};



        /** (for debugging) */
        std::ostream& operator<<(std::ostream& os, const TimeSeriesReal& time_series);

        /** the timeseries value, the time is the offset (DigitTime), plain old data. */
        struct TimeValueDigit : public boost::tuple<DigitTime,Value,Quality> {

            /** c-tor */
			TimeValueDigit(DigitTime t = 0, const Value& v = 0.0, const Quality& q = 0.0)
				: boost::tuple<DigitTime,Value,Quality>(t,v,q)
            {}
            /** accessor */
            const DigitTime& getTime() const { return get<0>(); }
            /** accessor */
            const Value& getValue() const { return get<1>(); }
            /** accessor */
            const Quality& getQuality() const { return get<2>(); }
        };
        /** for debugging */
        std::ostream& operator<<(std::ostream& os, const TimeValueDigit& time_val);

        /** the timeseries (collection), time as DigitTime */
        class TimeSeriesDigit : public std::vector<TimeValueDigit> {
        public:
			typedef std::vector<TimeValueDigit>::iterator iterator;
			typedef std::vector<TimeValueDigit>::const_iterator const_iterator;

            /** \brief c-tor, empty timeseries */
            TimeSeriesDigit(){}
            /** \brief c-tor, from range */
			TimeSeriesDigit(const TimeValueDigit* begin, const TimeValueDigit* end)
				: std::vector<TimeValueDigit>(begin,end) {}
			/** \brief c-tor, from range */
			TimeSeriesDigit(const_iterator begin, const_iterator end)
				: std::vector<TimeValueDigit>(begin,end) {}
            /** \brief, c-tor from C style table of values. The timestamps are 0,1,..,n */
            TimeSeriesDigit(const Value* begin, const Value* end);

            /** \brief copy c-tor */
			TimeSeriesDigit(const TimeSeriesDigit& t) : std::vector<TimeValueDigit>(t) {}

            /** \brief c-tor from timeseries, the timestamps are modified, the offset is added */
            TimeSeriesDigit(const TimeSeriesDigit& ts, const DigitTime& offset);

            /** operator = */
            TimeSeriesDigit& operator=(const TimeSeriesDigit& t){
                std::vector<TimeValueDigit>::operator=(t);
                return *this;
            }
            ~TimeSeriesDigit(){}

			/** the autocorelation of timeseries. The average is not substracted.
				out(0) = sum(i=0, i<n) in(i)*in(i)
				out(1) = sum(i=0, i<n-1) in(i)*in(i+1)
				out(2) = sum(i=0, i<n-2) in(i)*in(i+2)
				...
			 */
			TimeSeriesDigit autoCorrelationE(int scope) const;

            /** the sum of values of timeseries */
            double getSum() const;

			/** the sum of square of values of timeseries */
			double getSumSquared() const;

            /** the average of values of timeseries or 0.0 if the timeseries is empty */
            double getAvg() const { return empty() ? 0.0 : getSum()/size(); }

            /** the square of variation */
			double getSigmaSquared() const { return size()*getSumSquared() - getSum()*getSum(); }

            /** the variation (slower calculation) */
			double getSigma() const { return empty()? 0.0 : std::sqrt( getSigmaSquared() )/ size(); }

            //      TimeSeriesDigit operator+(const TimeSeriesDigit& ts) const;

        };

		/** the correlation of timeserieses in range 0..scope */
		TimeSeriesDigit correlation(const TimeSeriesDigit& ts1, const TimeSeriesDigit& ts2, int scope);

		/** the average of difference for two timeserieses */
		double getAvgAbsDiff(const TimeSeriesDigit& ts1, const TimeSeriesDigit& ts2);
		/** the average of difference (relative) for two timeserieses */
		double getAvgRelDiff(const TimeSeriesDigit& ts1, const TimeSeriesDigit& ts2);

        /** for debugging */
        std::ostream& operator<<(std::ostream& os, const TimeSeriesDigit& time_series);

    } //namespace timeseries
} //namespace faif

#endif //FAIF_TIME_SERIES
