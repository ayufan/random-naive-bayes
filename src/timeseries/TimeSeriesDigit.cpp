/** implementacja TimeSeriesDigit */
#include <algorithm>
#include <numeric>
#include <iterator>
#include <functional>
#include <boost/bind.hpp>

#include "TimeSeries.h"

using namespace std;
using namespace boost;

namespace faif {
    namespace timeseries {

        //  const Quality TimeValueDigit::DEFAULT_QUALITY = 0.0;

        /** drukuje pojedyncza time_value (do debuggowania) */
        ostream& operator<<(std::ostream& os, const TimeValueDigit& time_val) {
            os << '(' << time_val.getTime() << ',' << time_val.getValue() << ')';
            return os;
        }

        namespace {
            /** funkcja, ktora przesuwa czas o dany offset w probce */
            TimeValueDigit moveFun(const TimeValueDigit& v, const DigitTime& offset) {
                return TimeValueDigit(v.getTime() + offset, v.getValue(), v.getQuality() );
            }

            /** pomocniczy funktor - tworzy TimeValueDigit dla kol. punktow o ustalonej jakosci dla podanych wartosci */
            struct NextStepFunctor {

                NextStepFunctor() : curr_(-1) { }
                TimeValueDigit operator()(const Value& v) { return TimeValueDigit(++curr_, v); }
                DigitTime curr_;
            };
        }


        /** tworzy szereg czasowy na podstawie tablicy wartosci */
        TimeSeriesDigit::TimeSeriesDigit(const Value* begin, const Value* end) {
            NextStepFunctor functor;
            transform( begin, end, back_inserter(*this), functor );

        }

        /** tworzy na podstawie innej timeseries, przesuwa czas */
        TimeSeriesDigit::TimeSeriesDigit(const TimeSeriesDigit& ts, const DigitTime& offset) {
            reserve(ts.size());
            transform( ts.begin(), ts.end(), back_inserter(*this), boost::bind(&moveFun, _1, offset ) );
        }

        namespace {
            typedef TimeSeriesDigit::const_iterator TSIter;
            //oblicza iloczyn skalarny dwu szeregow (z uwzglednieniem offsetu)
            struct CalcCorrelationFunctor {
                CalcCorrelationFunctor(const TimeSeriesDigit& ts1, const TimeSeriesDigit& ts2,
                                       double avg1 = 0.0, double avg2 = 0.0, double sig1 = 1.0, double sig2 = 1.0)
                    : ts1_(ts1), ts2_(ts2), offset_(0), avg1_(avg1), avg2_(avg2), sig1_(sig1), sig2_(sig2)
                { }

                //oblicza iloczyn dla danego offsetu
                TimeValueDigit operator()() {
                    Value val = 0.0;
                    TSIter it1 = ts1_.begin(); //znajduje pierwszy czas, ktory jest i tu i tu
                    TSIter it2 = ts2_.begin();
                    while( it1 != ts1_.end() && it2 != ts2_.end() && it1->getTime() != (it2->getTime() + offset_) ) {
                        if( it1->getTime() < (it2->getTime() + offset_) ) ++it1;
                        else ++it2;
                    }
					int count = 0;
                    //teraz ustawione tak ze t0 = max( t1, t2 + offset )
                    for(; it1 != ts1_.end() && it2 != ts2_.end(); ++it1, ++it2 ) {
                        val += it1->getValue() * it2->getValue();
						++count;
					}

					val -= count * avg1_* avg2_;
					if(count > 1)
						val /= (count - 1)* sig1_ * sig2_;
					else
						val /= sig1_ * sig2_;
                    TimeValueDigit out(offset_, val);
                    ++offset_;
                    return out;
                }
                const TimeSeriesDigit& ts1_;
                const TimeSeriesDigit& ts2_;
                DigitTime offset_;
                double avg1_;
                double avg2_;
				double sig1_;
				double sig2_;
            };

        } //namespace


        /** oblicza autokorelacje, nie usuwa sredniej
            wy(0) = sum(i=0, i<n) we(i)*we(i)
            wy(1) = sum(i=0, i<n-1) we(i)*we(i+1)
            wy(2) = sum(i=0, i<n-2) we(i)*we(i+2)
        */
        TimeSeriesDigit TimeSeriesDigit::autoCorrelationE(int scope) const {
            TimeSeriesDigit out;
            CalcCorrelationFunctor gen(*this, *this);
            generate_n(back_inserter(out), scope, gen );
            return out;
        }


        /** oblicza korelacje dwu serii czasowych */
        TimeSeriesDigit correlation(const TimeSeriesDigit& ts1, const TimeSeriesDigit& ts2, int scope) {
            TimeSeriesDigit out;
            CalcCorrelationFunctor gen(ts1, ts2, ts1.getAvg(), ts2.getAvg(), ts1.getSigma(), ts2.getSigma() );
            generate_n(back_inserter(out), scope, gen );
            return out;
        }

        /** oblicza sume wyrazow szeregu czasowego */
        double TimeSeriesDigit::getSum() const {
            return accumulate( begin(), end(), 0.0, bind( plus<double>(), _1, bind(&TimeValueDigit::getValue, _2) ) );
        }

			/** oblicza sume kwadratow wyrazow szeregu czasowego */
		double TimeSeriesDigit::getSumSquared() const {
			return accumulate( begin(), end(), 0.0, bind( plus<double>(), _1,
														  bind( multiplies<double>(),
																bind(&TimeValueDigit::getValue, _2),
																bind(&TimeValueDigit::getValue, _2) )
														  )
							   );
		}


        /** oblicza srednia roznice dla dwu szeregow czasowych */
        double getAvgAbsDiff(const TimeSeriesDigit& ts1, const TimeSeriesDigit& ts2) {
			TSIter it1 = ts1.begin();
			TSIter it2 = ts2.begin();

			double sum_abs = 0.0;
			int count = 0;
			//przynajmniej jeden do rozpatrzenia
			while( it1 != ts1.end() || it2 != ts2.end() ) {
				++count;
				//druga seria juz rozpatrzona lub wyraz w it1 istnieje i jest mlodszy niz w it2
				if(it2 == ts2.end() || (it1 != ts1.end() && it1->getTime() < it2->getTime() ) ) {
					sum_abs += abs(it1->getValue() );
					++it1;
				} else if(it1 == ts1.end() || it2->getTime() < it1->getTime() ) { //tutaj it2 != end
					sum_abs += abs(it2->getValue() );
					++it2;
				} else  { //tutaj it1 != end() oraz it2 != end() oraz it1->getTime() == it2->getTime()
					sum_abs += abs(it1->getValue() - it2->getValue() );
					++it1;
					++it2;
				}
			}
			return count > 0 ? sum_abs/(double)count : 0.0;
		}

        /** oblicza srednia roznice (wzgledna) dla dwu szeregow czasowych */
		//TODO: podobna do poprzedniej - wyeliminowac Copy Paste Programming
		double getAvgRelDiff(const TimeSeriesDigit& ts1, const TimeSeriesDigit& ts2) {
			TSIter it1 = ts1.begin();
			TSIter it2 = ts2.begin();

			double sum_rel = 0.0;
			int count = 0;
			//przynajmniej jeden do rozpatrzenia
			while( it1 != ts1.end() || it2 != ts2.end() ) {
				//druga seria juz rozpatrzona lub wyraz w it1 istnieje i jest mlodszy niz w it2
				if(it2 == ts2.end() || (it1 != ts1.end() && it1->getTime() < it2->getTime() ) ) {
					++count;
					sum_rel += 1.0;
					++it1;
				} else if(it1 == ts1.end() || it2->getTime() < it1->getTime() ) { //tutaj it2 != end
					++count;
					sum_rel += 1.0;
					++it2;
				} else  { //tutaj it1 != end() oraz it2 != end() oraz it1->getTime() == it2->getTime()
					double m = ( it1->getValue() + it2->getValue() ) / 2.0;
					//pomija 'smieci', ale uwzglednia NaN
					if( ( m > (2 * std::numeric_limits<double>::epsilon() ) )
						|| ( m != m ) ) {
						++count;
						sum_rel += abs( (it1->getValue() - it2->getValue() )/ m );
					}
					++it1;
					++it2;
				}
			}
			return count > 0 ? sum_rel/(double)count : 0.0;
		}


        /** drukuje zawartosc serii (do debuggowania) */
        std::ostream& operator<<(std::ostream& os, const TimeSeriesDigit& time_series) {
            copy( time_series.begin(), time_series.end(), ostream_iterator<TimeValueDigit>(os," ") );
            return os;
        }
    } //namespace timeseries
} //namespace faif


