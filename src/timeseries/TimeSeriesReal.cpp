/** implementacja TimeSeriesReal */
#include "TimeSeries.h"

#include <algorithm>
#include <iterator>
#include <numeric>
#include <functional>
#include <boost/bind.hpp>
#include <cmath>

using namespace std;
using namespace boost;

namespace faif {
    namespace timeseries {

        /** konwersja z RealTime na longa (liczba sekund) - brak tego w boost */
        long to_time_t(const RealTime& t) {
            posix_time::ptime start(gregorian::date(1970,1,1),posix_time::time_duration(0,0,0));
            return (t-start).total_seconds();
        }

        namespace {

            struct FromTimestampValueGenerator {

                TimeValueReal operator()(const long& time, const double& value) {
                    return TimeValueReal( posix_time::from_time_t(time), value );
                }

            };

            //generator kolejnych timestamp-ow
            struct FromValueGenerator {
                FromValueGenerator(const RealTime& t, const RealDuration& d)
                    : curr_(t), delta_(d) {}

                //generuje kolejna probke czasu
                TimeValueReal operator()(const double& value) {
                    RealTime out = curr_;
                    curr_ += delta_;
                    return TimeValueReal(out, value);
                }
                RealTime curr_;
                RealDuration delta_;
            };
        } //namespace



		/** tworzy szereg czasowy na podstawie tablicy timestamp (posix) oraz tablicy wartosci */
		TimeSeriesReal::TimeSeriesReal(const long* begin, const long* end, const Value* input2) {
			FromTimestampValueGenerator gen;
			transform( begin, end, input2, back_inserter(*this), gen );
		}

		/** tworzy szereg czasowy na podstawie tablicy wartosci, czas dla pierwszego pomiaru, delta */
		TimeSeriesReal::TimeSeriesReal(const RealTime& start_time, const RealDuration& delta,
									   const Value* value_begin, const Value* value_end ) {

			FromValueGenerator gen(start_time, delta);
			transform( value_begin, value_end, back_inserter(*this), gen );

		}

		/** returns the sum of values of given time series */
		double TimeSeriesReal::getSum() const {
            return accumulate( begin(), end(), 0.0, bind( plus<double>(), _1, bind(&TimeValueReal::getValue, _2) ) );
		}

		namespace {
			typedef std::vector<TimeValueReal>::const_iterator RealConstIter;

			//the trapezium square equation
			double trapeziumSquare(double a, double b, double h) {
				return (a+b)*h/2.0;
			}

			//the it+1 must be valid
			double trapeziumSquare(const std::vector<TimeValueReal>::const_iterator it) {
				RealConstIter it_next(it);
				++it_next;
				long len_sec = posix_time::time_period(it_next->getTime(), it->getTime()).length().total_seconds();
				return trapeziumSquare(it->getValue(), it_next->getValue(), abs(len_sec) );
			}
		}

		/** oblicza calke (pole pod krzywa) dla danego szeregu czasowego */
		double TimeSeriesReal::getIntegral() const {
			if( size() < 2)
				return 0.0;

			std::vector<TimeValueReal>::const_iterator last = end();
			--last; //mozna, bo size() >= 2
			double sum = 0.0;
			for(RealConstIter it = begin(); it != last; ++it ) {
				sum += trapeziumSquare( it );
			}
			return sum;
		}

		/** drukuje pojedyncza time_value (do debuggowania) */
		ostream& operator<<(std::ostream& os, const TimeValueReal& time_val) {
			os << '(' << time_val.getTime() << ',' << time_val.getValue() << ')';
			return os;
		}

		/** drukuje zawartosc serii (do debuggowania) */
		std::ostream& operator<<(std::ostream& os, const TimeSeriesReal& time_series) {
			copy( time_series.begin(), time_series.end(), ostream_iterator<TimeValueReal>(os," ") );
			return os;
		}

	} //namespace timeseries
} //namespace faif


