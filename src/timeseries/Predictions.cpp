#include "Predictions.hpp"
#include "TimeseriesExceptions.hpp"

#include <algorithm>
#include <numeric>
#include <utility>
#include <iterator>
#include <cassert>
#include <boost/bind.hpp>

using namespace std;
using namespace boost;

namespace faif {
    namespace timeseries {


		const TimeValueDigit& Prediction::getHistoricalValue(DigitTime t) const {

			if( history_.size() == 0)
				return default_;

			//TODO: there is workaroud of msvc 9.0 debug (no lower_bound template with different types)
			TimeValueDigit v(t,0.0); //TODO: temporary object, workaround of bug in msvc 9.0 debug

			TimeSeriesDigit::const_iterator it =
				lower_bound( history_.begin(), history_.end(), v,
							 boost::bind(&TimeValueDigit::getTime, _1) < boost::bind(&TimeValueDigit::getTime, _2) );

			if( it != history_.end() ) {
				return *it;
			}
			else {
				return history_.back(); //history is not empty (it was checked before)
			}
		}

		/** calculate the prediction for period <from, to>,
			values for negative timestamp are readed from the history
		*/
        TimeSeriesDigit Prediction::calculatePrediction(DigitTime from, DigitTime to) {

			if(from > to)
				throw PredictionRangeException(from, to); //bad range

			TimeSeriesDigit out;

			DigitTime history_last = min(to + 1, 0);

			//copy history (if needed)
			for(DigitTime t = from; t < history_last; ++t)
				out.push_back( getHistoricalValue(t) );

			//calculate prediction (if needed)
			if(to >= 0) {
				TimeSeriesDigit pred = doCalcPrediction(to);
				TimeSeriesDigit::const_iterator first = pred.begin();
				if(from > 0)
					advance(first, from);
				for(;first != pred.end(); ++first)
					out.push_back( *first );
			}
			return out;
        }

        namespace {
			// \brief print visitor - used in print operator
            struct PrintVisitor : public PredictionVisitor {

                PrintVisitor(ostream& os) : os_(os) {}

                virtual void visit(const PredictionAR& v) { os_  << v; }
                virtual void visit(const PredictionKNN& v) { os_ << v; }

                ostream& os_;
            };

        }//namespace

		// \brief print operator (to debug)
        std::ostream& operator<<(std::ostream& os, const Prediction& pred) {
            PrintVisitor v(os);
			pred.accept(v);
            return os;
        }

		/** return the TimeValue for time t. The ranges are not checked (if bad -- assertion) */
        const TimeValueDigit& PredictionAR::get(DigitTime t) const {
			if(t < 0) {//find in history
				return getHistoricalValue(t);
			}
			else { //return from prediction
				assert( t < static_cast<int>(predictions_.size()) );
				return predictions_[t];
			}
        }

		/** prediction calculated by AR (n >= 0) */
		TimeValueDigit PredictionAR::calculateAR(DigitTime time) const {
			Value val = 0.0;
			for(DigitTime i=0;i<static_cast<int>(definition_.size());++i) {
				val += definition_[i] * get(time - i - 1).getValue();
			}
			//cout << "calculate AR t=" << time << " val = " << val << endl;
			return TimeValueDigit(time, val);
		}

		/**
		   calculate the prediction for f(t), f(t+1), ... f(t+n), calculates colection of probes
		 */
		TimeSeriesDigit PredictionAR::doCalcPrediction(DigitTime n) {

			DigitTime last_predicted = -1;
			if( ! predictions_.empty() )
				last_predicted = predictions_.back().getTime();

			if(n > last_predicted) { //check if the prediction should be calculated
				for(DigitTime t = last_predicted + 1; t <= n; ++t ) {
					//LOG4CXX_INFO( logger(), "AR::calcPrediction t = " << t << " pred " << calculateAR(t) );
					predictions_.push_back( calculateAR(t) );
				}
			}
            return predictions_;
		}

		/** for debugging */
		std::ostream& operator<<(std::ostream& os, const PredictionAR& ar) {
			os << "AR:";
			copy(ar.getARDef().begin(), ar.getARDef().end(), ostream_iterator<double>(os,",") );
			os << ';';
			return os;
		}


        PredictionKNN::PredictionKNN(const TimeSeriesDigit& history, const KNNDef& definition)
            : Prediction(history), definition_(definition)
        {
            //LOG4CXX_INFO( logger(), "KNN::KNN c-tor");
            const TimeSeriesDigit& historical = getHistory();
            TimeSeriesDigit::const_iterator reference_begin = historical.end();
            TimeSeriesDigit::const_iterator reference_end  = historical.end();

            int ref_size = static_cast<int>(definition.getRefSize() );

            //set the begin iterator to first available for reference block
            if( distance(historical.begin(), historical.end() ) > ref_size )
                advance( reference_begin, - ref_size);
            else
                reference_begin = historical.begin();

            reference_ = TimeSeriesDigit(reference_begin, reference_end);
            //LOG4CXX_INFO( logger(), "KNN::KNN reference_ = " << reference_ );
        }

        /** return the TimeValue for time t. The ranges are not checked (if bad -- assertion) */
        const TimeValueDigit& PredictionKNN::get(DigitTime t) const {
            if(t < 0)
                return getHistoricalValue(t);
            else {
                assert( t < static_cast<int>(predictions_.size()) );
                return predictions_[t];
            }
        }
        namespace {

            typedef TimeSeriesDigit::const_iterator TSIter;

            //the distance between two time value digit elements of the same timestamp
            Value element_distance(const TimeValueDigit& a, const TimeValueDigit& b) {
                //cout << "element distance " << a.getValue() << "," << b.getValue() << endl;
                return abs(a.getValue() - b.getValue() );
            }

            // the sum of element_distances, distance between two timeseries
            Value series_distance(TSIter begin1, TSIter end1, TSIter begin2, TSIter end2) {
                if( distance(begin1, end1) > distance(begin2, end2) ) {
                    //cout << "dist " << distance(begin1, end1) << " dist " << distance(begin2, end2) << endl;
                    return 0.0;
                }
                //cout << "dist beg1:" << begin1->getValue() << " beg2: " << begin2->getValue() << endl;
                //cout << "size1 " << distance(begin1,end1) << " size2 " << distance(begin2,end2) << endl;
                return inner_product( begin1, end1, begin2, 0.0, plus<double>(), bind(&element_distance, _1, _2 ) );
            }

            struct BestRangesFunctor : noncopyable {
                //initializes the associate table to the best offsets
                BestRangesFunctor(TSIter begin, TSIter end, TSIter last_begin,
                                  const TimeSeriesDigit& reference,
                                  int num_neighbours)

                    : NUM_NEIGHBOURS(num_neighbours)
                {
                    for(; begin != last_begin; ++begin) {
                        Value dist = series_distance( reference.begin(), reference.end(), begin, end );
                        add( dist, begin );
                    }
                }
                //adds the new element to collection (if it is good enough)
                void add(Value dist, TSIter it) {
					//LOG4CXX_INFO( logger(), "add dist= " << dist << " for time=" << it->getTime() );
                    best_.insert( make_pair(dist, it) );
                    if( best_.size() > NUM_NEIGHBOURS )
                        best_.erase(--best_.end() );
                }

                Value avg(DigitTime offset, int reference_length, TSIter last_iter) const {
                    Value v = 0.0;
                    for(BestIter ii = best_.begin(); ii != best_.end(); ++ii ) {
                        TSIter it_best = ii->second;
						if( distance(it_best, last_iter) > (reference_length + offset) ) {
							advance(it_best, reference_length + offset );
							v += it_best->getValue();
						}
                    }
                    if(!best_.empty())
                        return v/best_.size();
                    else
                        return 0.0;

                }

                const unsigned int NUM_NEIGHBOURS; //size of collection

                //collection: distance(value) and the iterator to the begin of block
                multimap<Value,TSIter> best_;
                typedef multimap<Value,TSIter>::const_iterator BestIter;
            };

            ostream& operator<<(ostream& os, const BestRangesFunctor& b) {
                for( BestRangesFunctor::BestIter ii = b.best_.begin(); ii != b.best_.end(); ++ii ) {
                    os << (ii->second)->getTime() << ',';
                }
                return os;
            }

        } //namespace

        /** calculate the prediction for f(t), f(t+1), ... f(t+n), calculates colection of probes  */
        TimeSeriesDigit PredictionKNN::doCalcPrediction(DigitTime n) {
			//std::cout << " reference: " << reference_ << std::endl;
            //LOG4CXX_INFO( logger(), "KNN::calcPredictions n=" << n);
            predictions_.clear();

            // dostarcza danych historycznych, ktore sa porownywane blokiem referencyjnym
            const TimeSeriesDigit& ts = getHistory();

            //musi miec wielkosc bloku (reference_.size) oraz jeszcze przynajmniej jeden pomiar
			int min_hist_size = static_cast<int>( reference_.size() ) + 1;
            TSIter last_begin = ts.end();
            if(min_hist_size < static_cast<int>( ts.size() ) )
                advance(last_begin, -min_hist_size);
            else
                last_begin = ts.begin();

			//          LOG4CXX_INFO( logger(), "distance (begin, last_begin) " << distance(ts.begin(), last_begin ) );
			//          LOG4CXX_INFO( logger(), "distance (last_begin, end) " << distance(last_begin, ts.end() ) );

            BestRangesFunctor best(ts.begin(), ts.end(), last_begin, reference_, definition_.getK() );
			//std::cout << " best " << best << std::endl;

            for(DigitTime t = 0; t <= n; ++t ) {
                Value v = best.avg(t, definition_.getRefSize(), ts.end() );
				//std::cout << " result: t=" << t << " v=" << v << std::endl;
                //LOG4CXX_INFO( logger(), "result: t = " << t << " v = " << v );
                predictions_.push_back( TimeValueDigit(t,v) );
            }

            return predictions_;
        }


        /** do debuggowania */
        std::ostream& operator<<(std::ostream& os, const PredictionKNN& knn) {
            os << "KNN: k=" << knn.getKNNDef().getK() << " ref_size= " << knn.getKNNDef().getRefSize() << ';';
            return os;
        }


    } //namespace timeseries
} //namespace faif

