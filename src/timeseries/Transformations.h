#ifndef FAIF_TS_TRANSFORMATIONS
#define FAIF_TS_TRANSFORMATIONS

// file with transformations -> changest real time series into digit one and digit time series into real one

#include <ostream>
#include "TimeSeries.h"

namespace faif {
    namespace timeseries {

		/** \brief transformation - class to change timeseries stored in TimeSeriesDigit and/or TimeSeriesReal
		 */
		class Transformation {
		public:
			Transformation(const RealTime& present, const RealDuration& delta)
				: present_(present), delta_(delta) {}
			Transformation(const Transformation& t)
				: present_(t.present_), delta_(t.delta_) {}
			~Transformation() {}

			Transformation& operator=(const Transformation& t) {
				present_ = t.present_;
				delta_ = t.delta_;
				return *this;
			}

            /** accessor - reference point in time */
            const RealTime& getPresent() const { return present_; }
            /** accessor - distance between probes */
            const RealDuration& getDelta() const { return delta_; }

			/** transform digit time to real time (gives the reference points in time) */
			RealTime toReal(const DigitTime& d) const;

			/** transform real time to digit time
				D = toDigit(R), then
				toReal(D) in <R - delta/2;R + delta/2)
				-- includes R - delta/2
				-- excludes R + delta/2
			*/
			DigitTime toDigit(const RealTime& r) const;
		private:
			RealTime present_;
			RealDuration delta_;
		};

		/** creates digit time series from real time series.
			Linear resampling: arythmetic everage for aggregation,
			the linear approxymation for missing data.
		*/
		TimeSeriesDigit create(const TimeSeriesReal& in, const Transformation& transformation);

		/** creates real time series from digit time series.
		*/
		TimeSeriesReal create(const TimeSeriesDigit& in, const Transformation& transformation);

        /** for debugging */
        std::ostream& operator<<(std::ostream& os, const Transformation& time_series);

    } //namespace timeseries
} //namespace faif

#endif //FAIF_TS_TRANSFORMATIONS
