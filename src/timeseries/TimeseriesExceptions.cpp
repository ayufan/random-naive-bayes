#include "TimeseriesExceptions.hpp"

namespace faif {
    namespace timeseries {

		//! print
		std::ostream& PredictionRangeException::print(std::ostream& os) const throw () {
			os << "Prediction range error: from = '" << from_ << "' to = '" << to_ << "'";
			return os;
		}

	} //namespace timeseries
} //namespace faif
