#ifndef FAIF_ATTRIBUTE_H_
#define FAIF_ATTRIBUTE_H_

#if defined(_MSC_VER) && (_MSC_VER >= 1400)
//msvc9.0 warnings for boost::concept_check
#pragma warning(disable:4100)
#endif


#include <ostream>
#include <boost/concept/assert.hpp>
#include <boost/concept_check.hpp>

namespace faif {

	/** \brief nominal attribute trait (equality comparable)*/
	struct nominal_tag {};

	/** \brief ordered attribute trait (equality comparable, less than comparable ) */
	struct ordered_tag : public nominal_tag {};

	/** \brief interval attribute trait (equality comparable, less than comparable, distance) */
	struct interval_tag : public ordered_tag {};

	/** \brief nominal attribute trait (equality comparable, less than comparable, distance, continuous) */
	struct continuous_tag : public interval_tag {};

	/** \brief forward declaration */
	template <typename Attr> class DomainEnumerate;

	/** \brief nominal attribute template (equality comparable)
	 */
	template <typename Val> class AttrNominal {
	public:
		BOOST_CONCEPT_ASSERT(( boost::EqualityComparable<Val> ));
		BOOST_CONCEPT_ASSERT(( boost::DefaultConstructible<Val> ));
		BOOST_CONCEPT_ASSERT(( boost::CopyConstructible<Val> ));
		BOOST_CONCEPT_ASSERT(( boost::Assignable<Val> ));

		/** \brief the attribute trait */
		typedef nominal_tag AttributeType;

		typedef Val Value;

		typedef DomainEnumerate<AttrNominal<Value> > DomainType;

		/** \brief c-tor */
		AttrNominal() : domain_(0L) {}

		/** \brief c-tor */
		AttrNominal(const Value& val, DomainType* d) : val_(val), domain_(d) {}

		/** \brief c-tor */
		AttrNominal(const AttrNominal& a) : val_(a.val_), domain_(a.domain_) {}

		/** \brief assign operator */
		AttrNominal& operator=(const AttrNominal& a) {
			val_ = a.val_;
			domain_ = a.domain_;
		}

		/** \brief d-tor */
		~AttrNominal(){}

		/** \brief accessor  */
		Value& get() { return val_; }

		/** \brief accessor  */
		const Value& get() const { return val_; }

		/** \brief accessor - domain */
		const DomainType* getDomain() const { return domain_; }

		/** \brief the equality comparison */
		bool operator==(const AttrNominal& v) const { return val_ == v.val_ && domain_ == v.domain_; }

		/** \brief the equality comparison */
		bool operator!=(const AttrNominal& v) const { return val_ != v.val_ || domain_ != v.domain_; }
	private:
		Value val_;
		DomainType* domain_;
	};

	typedef AttrNominal<std::string> AttrNominalString;

} //namespace faif

#endif //FAIF_ATTR_NOMINAL_H_
