#ifndef FAIF_DOMAIN_H_
#define FAIF_DOMAIN_H_

#include <ostream>
#include <string>
#include <list>
#include <iterator>
#include <algorithm>
#include <boost/bind.hpp>

#include "Attribute.h"
#include "ExceptionsFaif.h"

namespace faif {

    /** \breif the domain of nominal attributes.

		Collection of attributes (which are equally comparable).
    */
	template <typename Attr>
	class DomainEnumerate {
	public:
		typedef typename Attr::AttributeType AttributeType;

		typedef std::list<Attr> Container;
		typedef typename Container::iterator iterator;
		typedef typename Container::const_iterator const_iterator;

        DomainEnumerate(const std::string& id = "") : id_(id) {}

		/** \brief copy c-tor (transform the parent pointers  */
        DomainEnumerate(const DomainEnumerate& d);

		/** \brief assignment (transform the parent pointers  */
        DomainEnumerate& operator=(const DomainEnumerate&);

		~DomainEnumerate() {}

        /** \brief accessor */
        std::string getId() const { return id_; }

        /** \brief accessor */
        int getSize() const { return values_.size(); }

        /** the domain equality operator (the id is compared, not all attributes) */
        bool operator==(const DomainEnumerate& d) const { return id_ == d.id_; }

        /** the domain not equality operator (the id is compared, not all attributes) */
        bool operator!=(const DomainEnumerate& d) const { return id_ != d.id_;}

        /** \brief accessor */
        iterator begin() { return values_.begin(); }
        /** \brief accessor */
        iterator end() { return values_.end(); }

        /** \brief accessor */
        const_iterator begin() const { return values_.begin(); }
        /** \brief accessor */
        const_iterator end() const { return values_.end(); }

		/** \brief search for attribute equal to given. If found return the iterator,
			otherwise insert new into collection and return the inserted attribute */
		iterator insert(const typename Attr::Value& value);

		/** \brief erase the attribute of given iterator */
		void erase(iterator iter);

        /** \brief remove all the attrib equal to given */
        void remove(const typename Attr::Value& value);

        /** \brief return the first iterator to attribute equal to given */
        const_iterator find(const typename Attr::Value& value) const;

        /** \brief return the first iterator to attribute equal to given */
        iterator find(const typename Attr::Value& value);
    private:
        std::string id_; //!< the domain id
		std::list<Attr> values_;
    };

	/** \brief find the attribute equal to given value in domain.
		\return found attribute
		\throws NotFoundException
	*/
	template <typename Attr>
	Attr& findRef(DomainEnumerate<Attr>& domain, const typename Attr::Value& value) {
		typename DomainEnumerate<Attr>::iterator it = domain.find(value);
		if( it == domain.end() )
			throw NotFoundException( domain.getId().c_str() );
		else
			return *it;
	}


	/** the ostream operator for attribute, defined here because it uses the domain method */
	template <typename Value> std::ostream& operator<<(std::ostream& os, const AttrNominal<Value>& a) {
		os << a.getDomain()->getId() << "=" << a.get();
		return os;
	}

	/** \brief the ostream operator for domain */
	template <typename Attr>
	std::ostream& operator<<(std::ostream& os, const DomainEnumerate<Attr>& domain) {
        std::copy(domain.begin(), domain.end(), std::ostream_iterator<Attr>(os,","));
        return os << ';';
	}

    namespace {

		template <typename Attr>
		Attr TransformFunction(const Attr& a, typename Attr::DomainType* d) {
			return Attr(a.get(),d);
		}

    } //namespace


	/** \brief copy c-tor (transform the parent pointers  */
	template <typename Attr>
	DomainEnumerate<Attr>::DomainEnumerate(const DomainEnumerate& d)
		: id_(d.id_) {

		std::transform(d.values_.begin(), d.values_.end(), std::back_inserter<Container>(values_),
					   boost::bind(&TransformFunction<Attr>, _1, this) );
	}

	/** \brief assignment (transform the parent pointers  */
    template <typename Attr>
	DomainEnumerate<Attr>&
	DomainEnumerate<Attr>::operator=(const DomainEnumerate& d) {
		if(&d == this)
			return *this;

		id_ = d.id_;
		values_.clear();

		std::transform(d.values_.begin(), d.values_.end(), std::back_inserter<Container>(values_),
					   boost::bind(&TransformFunction<Attr>, _1, this) );
		return *this;
	}

		/** search for attribute equal to given. If found return the iterator,
			otherwise insert new into collection and return the inserted attribute */

	template< typename Attr>
	typename DomainEnumerate<Attr>::iterator
	DomainEnumerate<Attr>::insert(const typename Attr::Value& value) {
		iterator found = find( value );
        if( found != values_.end() )
            return found;
		//inserts element on the end (if not found)
		return values_.insert( values_.end(), Attr(value, this) );
	}

	/** return the first iterator to attribute equal to given */
	template< typename Attr>
	typename DomainEnumerate<Attr>::const_iterator
	DomainEnumerate<Attr>::find(const typename Attr::Value& value) const {
		Attr attr(value, this);
		return std::find( values_.begin(), values_.end(), attr );
	}


	/** return the first iterator to attribute equal to given */
	template<typename Attr>
	typename DomainEnumerate<Attr>::iterator
	DomainEnumerate<Attr>::find(const typename Attr::Value& value) {
		Attr attr(value, this);
		return std::find( values_.begin(), values_.end(), attr );
	}

	/** erase the attribute of given iterator */
	template<typename Attr>
	void DomainEnumerate<Attr>::erase(iterator iter) {
		values_.erase( iter );
	}

	/** remove all the attrib equal to given */
	template<typename Attr>
	void DomainEnumerate<Attr>::remove(const typename Attr::Value& value) {
		Attr attr(value, this);
		values_.remove( attr );
	}


} //namespace faif

#endif //FAIF_DOMAIN_H_
