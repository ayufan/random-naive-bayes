#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <boost/algorithm/string.hpp>

#include <fstream>
#include <iostream>
#include <string>
#include <map>

#include "../src/learning/ExampleTest.h"
#include "../src/learning/ExampleTrain.h"
#include "../src/learning/NaiveBayesian.h"
#include "../src/learning/RandomNaiveBayesian.h"

using namespace std;
using namespace faif;
using namespace faif::ml;
using boost::unit_test::test_suite;

BOOST_AUTO_TEST_SUITE( random_naive_bayesian )

BOOST_AUTO_TEST_CASE( classifierAddCategoryAttribTest ) {
	RandomNaiveBayesian rnb;
	rnb.insertCategory("cat1");
	rnb.insertCategory("cat2");
	rnb.insertCategory("cat3");
	
	AttrDomain& at1 = rnb.insertAttr("attr1");
	
	BOOST_REQUIRE(at1 == rnb.findAttrRef("attr1"));

	at1.insert("at1_v1");
	at1.insert("at1_v2");
	at1.insert("at1_v3");

	BOOST_CHECK(at1.getSize() == 3);
	
	AttrDomain& at2 = rnb.insertAttr("attr2");
	at2.insert("at2_v1");
	at2.insert("at2_v2");
	
	BOOST_CHECK(at2.getSize() == 2);
	BOOST_CHECK(at2.find("at2_v1") == at2.insert("at2_v1") );

	at2.erase(at2.find("at2_v2"));

	BOOST_CHECK(at2.getSize() == 1);

	BOOST_CHECK_THROW( rnb.findAttrRef("attr3"), NotFoundException );

	
} // classifierAddCategoryAttribTest


BOOST_AUTO_TEST_CASE( StandardClassifierTest) {		
	RandomNaiveBayesian n;
	AttrVal c1 = &(*n.insertCategory("good"));
	AttrVal c0 = &(*n.insertCategory("bad"));

	AttrDomain& a_aura = n.insertAttr("aura");
	AttrVal as = &(*a_aura.insert("slon"));
	AttrVal ap = &(*a_aura.insert("poch"));
	AttrVal ad = &(*a_aura.insert("desz"));

	AttrDomain& a_temp = n.insertAttr("temp");
	AttrVal tc = &(*a_temp.insert("cie"));
	AttrVal tu = &(*a_temp.insert("umi"));
	AttrVal tz = &(*a_temp.insert("zim"));

	AttrDomain& a_wilg = n.insertAttr("wilg");
	AttrVal wd = &(*a_wilg.insert("duza"));
	AttrVal wn = &(*a_wilg.insert("normalna"));

	AttrDomain& a_wiat = n.insertAttr("wiat");
	AttrVal vm = &(*a_wiat.insert("slaby"));
	AttrVal vd = &(*a_wiat.insert("silny"));

	ExampleTrain e01( ExampleTest( as, tc, wd, vm), c0 ); n.addTraining(e01);
	ExampleTrain e02( ExampleTest( as, tc, wd, vd), c0 ); n.addTraining(e02);
	ExampleTrain e03( ExampleTest( ap, tc, wd, vm), c1 ); n.addTraining(e03); 
	ExampleTrain e04( ExampleTest( ad, tu, wd, vm), c1 ); n.addTraining(e04);
	ExampleTrain e05( ExampleTest( ad, tz, wn, vm), c1 ); n.addTraining(e05);
	ExampleTrain e06( ExampleTest( ad, tz, wn, vd), c0 ); n.addTraining(e06);
	ExampleTrain e07( ExampleTest( ap, tz, wn, vd), c1 ); n.addTraining(e07);
	ExampleTrain e08( ExampleTest( as, tu, wd, vm), c0 ); n.addTraining(e08);
	ExampleTrain e09( ExampleTest( as, tz, wn, vm), c1 ); n.addTraining(e09);
	ExampleTrain e10( ExampleTest( ad, tu, wn, vm), c1 ); n.addTraining(e10);
	ExampleTrain e11( ExampleTest( as, tu, wn, vd), c1 ); n.addTraining(e11);
	ExampleTrain e12( ExampleTest( ap, tu, wd, vd), c1 ); n.addTraining(e12);
	ExampleTrain e13( ExampleTest( ap, tc, wn, vm), c1 ); n.addTraining(e13);
	ExampleTrain e14( ExampleTest( ad, tu, wd, vd), c0 ); n.addTraining(e14);

	ExampleTest et( as, tu, wd, vm);

	int i = 0;
	for(unsigned K=4; K <= 20; K+= 4) {
		for (unsigned m = 3 ; m <= 4; ++m) {
			n.setParameters(K,m);
			BOOST_CHECK_MESSAGE( n.getCategory(et) == c0, "Wrong classify with parameters K: " << K << " and m: " << m );
			++i;
		}
	}
	BOOST_CHECK_THROW(n.setParameters(0,5), exception);

} // StandardClassifierTest


void loadExamples( map<string, AttrVal>& car_cat, vector<map<string, AttrVal> >& car_attr, RandomNaiveBayesian& cars,
				   vector<pair<vector<AttrVal>, string> >& loaded_examples) {
	char buff[256];
	const char* filename = "data/car.data.txt";

	car_cat.insert(pair<string, AttrVal>("unacc", &(*cars.insertCategory("unacc"))));
	car_cat.insert(pair<string, AttrVal>("acc", &(*cars.insertCategory("acc"))));
	car_cat.insert(pair<string, AttrVal>("good", &(*cars.insertCategory("good"))));
	car_cat.insert(pair<string, AttrVal>("vgood", &(*cars.insertCategory("vgood"))));

	AttrDomain& buying = cars.insertAttr(string("buying"));
	map<string, AttrVal> map_temp;
	map_temp.insert(pair<string, AttrVal>("vhigh",&(*buying.insert("vhigh"))));
	map_temp.insert(pair<string, AttrVal>("high",&(*buying.insert("high"))));
	map_temp.insert(pair<string, AttrVal>("med",&(*buying.insert("med"))));
	map_temp.insert(pair<string, AttrVal>("low",&(*buying.insert("low"))));
	
	car_attr.push_back(map_temp);
	map_temp.clear();

	AttrDomain& maint = cars.insertAttr(string("maint"));
	map_temp.insert(pair<string, AttrVal>("vhigh",&(*maint.insert("vhigh"))));
	map_temp.insert(pair<string, AttrVal>("high",&(*maint.insert("high"))));
	map_temp.insert(pair<string, AttrVal>("med",&(*maint.insert("med"))));
	map_temp.insert(pair<string, AttrVal>("low",&(*maint.insert("low"))));

	car_attr.push_back(map_temp);
	map_temp.clear();

	AttrDomain& doors = cars.insertAttr(string("doors"));
	map_temp.insert(pair<string, AttrVal>("2",&(*doors.insert("2"))));
	map_temp.insert(pair<string, AttrVal>("3",&(*doors.insert("3"))));
	map_temp.insert(pair<string, AttrVal>("4",&(*doors.insert("4"))));
	map_temp.insert(pair<string, AttrVal>("5more",&(*doors.insert("5more"))));

	car_attr.push_back(map_temp);
	map_temp.clear();

	AttrDomain& persons = cars.insertAttr(string("persons"));
	map_temp.insert(pair<string, AttrVal>("2",&(*persons.insert("2"))));
	map_temp.insert(pair<string, AttrVal>("4",&(*persons.insert("4"))));
	map_temp.insert(pair<string, AttrVal>("more",&(*persons.insert("more"))));

	car_attr.push_back(map_temp);
	map_temp.clear();

	AttrDomain& lug_boot = cars.insertAttr(string("lug_boot"));
	map_temp.insert(pair<string, AttrVal>("small",&(*lug_boot.insert("small"))));
	map_temp.insert(pair<string, AttrVal>("med",&(*lug_boot.insert("med"))));
	map_temp.insert(pair<string, AttrVal>("big",&(*lug_boot.insert("big"))));


	car_attr.push_back(map_temp);
	map_temp.clear();

	AttrDomain& safety = cars.insertAttr(string("safety"));
	map_temp.insert(pair<string, AttrVal>("low",&(*safety.insert("low"))));
	map_temp.insert(pair<string, AttrVal>("med",&(*safety.insert("med"))));
	map_temp.insert(pair<string, AttrVal>("high",&(*safety.insert("high"))));
	
	car_attr.push_back(map_temp);

	ifstream is(filename, ios::in);
  
  BOOST_REQUIRE_MESSAGE(is, "Failed to open file with data"); 

// get examples from file
	typedef vector<map<string, AttrVal> > attrVMap;
	
	vector<AttrVal> attrv_temp;
	attrVMap::iterator vit;
	
	int x;
	is.getline(buff, 256);
	for(x=0; !is.eof(); ++x, is.getline(buff, 256)){
		
		string sbuff(buff), subs;
		int from=0, to;
		attrv_temp.clear();
		for( vit = car_attr.begin(); vit!= car_attr.end(); ++vit) {
			to = sbuff.find_first_of(",", from);
 			subs = sbuff.substr(from, to-from);
			from = to+1;
			attrv_temp.push_back((*vit)[subs]);
		}
		subs = sbuff.substr(from);
		cars.addTraining(ExampleTrain( ExampleTest(attrv_temp), car_cat[subs]));
		loaded_examples.push_back(pair<vector<AttrVal>,string>(attrv_temp, subs));

	}

	BOOST_MESSAGE("Number of loaded training examples: " << x );

	is.close();		// close file

} // loadExamples()


BOOST_AUTO_TEST_CASE( ExtendedClassifierTestCars ) {
	typedef map<string, AttrVal> catMap;
	typedef vector<map<string, AttrVal> > attrVMap;
	typedef vector<pair<vector<AttrVal>, string> > ExamplesSet;

	RandomNaiveBayesian rnb;
	catMap carCat; 
	attrVMap carAttr;
	ExamplesSet examples;

	loadExamples(carCat, carAttr, rnb, examples);
	
	unsigned K = 50, m = 5;
	rnb.setParameters(K,m);
	int i = 0, error = 0, number = 0;
	for ( ExamplesSet::iterator iter = examples.begin(); iter != examples.end(); ++iter ) {
		if ( i%1 == 0 ) {
			if (rnb.getCategory(ExampleTest(iter->first)) != carCat[iter->second]) {
				++error;
			}
			++number;
		}
		++i;
	}
	BOOST_CHECK_MESSAGE(error == 0, "\nNumber of uncorrectly classified examples: " << error <<
		"\nNumber of tested examples: " << number << "\nNumber of training examples: " <<
		"\nParameters for RNBC - K: " << K << " , m: " << m );

}

BOOST_AUTO_TEST_SUITE_END()
