/** plik zawiera test klasyfikatora */
#if defined (_MSC_VER) && (_MSC_VER >= 1400)
//msvc8.0 generuje smieci dla boost::string
#pragma warning(disable:4512)
//msvc9.0 warnings for boost::concept_check
#pragma warning(disable:4100)
#endif

#include <iostream>
#include <sstream>
#include <cassert>

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <boost/algorithm/string.hpp>

#include "../src/learning/ExampleTest.h"
#include "../src/learning/ExampleTrain.h"
#include "../src/learning/NaiveBayesian.h"

using namespace std;
using namespace faif;
using namespace faif::ml;
using boost::unit_test::test_suite;

BOOST_AUTO_TEST_SUITE( FAIF_NBC_classifier_test )

BOOST_AUTO_TEST_CASE( classifierAddCategoryAttribTest ) {
    NaiveBayesian nb;

    nb.insertCategory("one");
    nb.insertCategory("two");
    nb.insertCategory("three");

    AttrDomain& a = nb.insertAttr("a");
    a.insert("A");
    a.insert("B");
    a.insert("C");
    AttrDomain& i = nb.insertAttr("i");
    i.insert("I");
    i.insert("J");
    AttrDomain& p = nb.insertAttr("p");
    p.insert("P");
    BOOST_CHECK( a == nb.findAttrRef("a") );
    BOOST_CHECK( nb.findAttrRef("a").getSize() == 3 );
    BOOST_CHECK( i == nb.findAttrRef("i") );
    BOOST_CHECK( nb.findAttrRef("i").getSize() == 2 );
    BOOST_CHECK( p == nb.findAttrRef("p") );
    BOOST_CHECK( nb.findAttrRef("p").getSize() == 1 );

    bool in = false;
    try {
        nb.findAttrRef("b");
    }
    catch(const NotFoundException& ) {
        //powinien tutaj wejsc!
        in = true;
    }
    BOOST_CHECK( in );
}

void loadSomeExamples(NaiveBayesian& nb) {
    AttrVal cat1 = &(*nb.insertCategory("one"));
    AttrVal cat2 = &(*nb.insertCategory("two"));
    AttrVal cat3 = &(*nb.insertCategory("three"));
    AttrDomain& a = nb.insertAttr("a");
    AttrVal va1 = &(*a.insert("A"));
    AttrVal va2 = &(*a.insert("B"));
    AttrVal va3 = &(*a.insert("C"));
    AttrDomain& i = nb.insertAttr("i");
    AttrVal vi1 = &(*i.insert("I"));
    AttrVal vi2 = &(*i.insert("J"));
    AttrDomain& p = nb.insertAttr("p");
    AttrVal vp1 = &(*p.insert("P"));

    ExampleTrain ex1( ExampleTest(va1, vi1, vp1), cat1);
    ExampleTrain ex2( ExampleTest(va1, vi2, vp1), cat1);
    ExampleTrain ex3( ExampleTest(va3, vi1, vp1), cat3);
    ExampleTrain ex4( ExampleTest(va3, vi1, vp1), cat3);
    ExampleTrain ex5( ExampleTest(va2, vi1, vp1), cat2);

    nb.addTraining( ex1 );
    nb.addTraining( ex2 );
    nb.addTraining( ex3 );
    nb.addTraining( ex4 );
    nb.addTraining( ex5 );
}

BOOST_AUTO_TEST_CASE( classifierCounterTest ) {
	const char* COUNTERS_OUTPUT_STRING =
		"Categories(3)=one,=two,=three,;\n"
		"Attributes(3):\n"
        "a=A,a=B,a=C,;\n"
        "i=I,i=J,;\n"
        "p=P,;\n"
        "State: TRAINING: \n"
        "=one(2):\n"
        "A(2),B(0),C(0),\n"
        "I(1),J(1),\n"
        "P(2),\n"
        "=two(1):\n"
        "A(0),B(1),C(0),\n"
        "I(1),J(0),\n"
        "P(1),\n"
        "=three(2):\n"
        "A(0),B(0),C(2),\n"
        "I(2),J(0),\n"
        "P(2),\n\n";

    NaiveBayesian nb;
    loadSomeExamples(nb);
	stringstream ss;
	ss << nb;
	BOOST_CHECK( ss.str() == string(COUNTERS_OUTPUT_STRING) );

	const char* RESET_OUTPUT_STRING =
		"Categories(3)=one,=two,=three,;\n"
		"Attributes(3):\n"
        "a=A,a=B,a=C,;\n"
        "i=I,i=J,;\n"
        "p=P,;\n"
        "State: TRAINING: \n"
        "=one(0):\n"
        "A(0),B(0),C(0),\n"
        "I(0),J(0),\n"
        "P(0),\n"
        "=two(0):\n"
        "A(0),B(0),C(0),\n"
        "I(0),J(0),\n"
        "P(0),\n"
        "=three(0):\n"
        "A(0),B(0),C(0),\n"
        "I(0),J(0),\n"
        "P(0),";



	nb.reset();
	stringstream st;
	st << nb;
	string s1 = st.str();
	boost::trim(s1);
	BOOST_CHECK( s1 == string(RESET_OUTPUT_STRING) );
}

BOOST_AUTO_TEST_CASE( classifierClasifyTest ) {
    NaiveBayesian nb;
    loadSomeExamples(nb);
	AttrDomain& a = nb.findAttrRef("a");
	AttrVal va2 = &(*a.find("B"));
	AttrDomain& i = nb.findAttrRef("i");
	AttrVal vi2 = &(*i.find("J"));
	AttrDomain& p = nb.findAttrRef("p");
	AttrVal vp1 = &(*p.find("P"));

    ExampleTest ex6( va2, vi2, vp1);
	AttrVal c = nb.getCategory(ex6);
	BOOST_CHECK( c == &(*nb.insertCategory("two")) );
}

BOOST_AUTO_TEST_CASE( weatherClasifierTest ) {
    NaiveBayesian n;
    AttrVal c1 = &(*n.insertCategory("good"));
    AttrVal c0 = &(*n.insertCategory("bad"));

    AttrDomain& a_aura = n.insertAttr("aura");
    AttrVal as = &(*a_aura.insert("slon"));
    AttrVal ap = &(*a_aura.insert("poch"));
    AttrVal ad = &(*a_aura.insert("desz"));

    AttrDomain& a_temp = n.insertAttr("temp");
    AttrVal tc = &(*a_temp.insert("cie"));
    AttrVal tu = &(*a_temp.insert("umi"));
    AttrVal tz = &(*a_temp.insert("zim"));

    AttrDomain& a_wilg = n.insertAttr("wilg");
    AttrVal wd = &(*a_wilg.insert("duza"));
    AttrVal wn = &(*a_wilg.insert("normalna"));

    AttrDomain& a_wiat = n.insertAttr("wiat");
    AttrVal vm = &(*a_wiat.insert("slaby"));
    AttrVal vd = &(*a_wiat.insert("silny"));

	ExampleTrain e01( ExampleTest( as, tc, wd, vm), c0 ); n.addTraining(e01);
    ExampleTrain e02( ExampleTest( as, tc, wd, vd), c0 ); n.addTraining(e02);
    ExampleTrain e03( ExampleTest( ap, tc, wd, vm), c1 ); n.addTraining(e03);
    ExampleTrain e04( ExampleTest( ad, tu, wd, vm), c1 ); n.addTraining(e04);
    ExampleTrain e05( ExampleTest( ad, tz, wn, vm), c1 ); n.addTraining(e05);
    ExampleTrain e06( ExampleTest( ad, tz, wn, vd), c0 ); n.addTraining(e06);
    ExampleTrain e07( ExampleTest( ap, tz, wn, vd), c1 ); n.addTraining(e07);
    ExampleTrain e08( ExampleTest( as, tu, wd, vm), c0 ); n.addTraining(e08);
    ExampleTrain e09( ExampleTest( as, tz, wn, vm), c1 ); n.addTraining(e09);
    ExampleTrain e10( ExampleTest( ad, tu, wn, vm), c1 ); n.addTraining(e10);
    ExampleTrain e11( ExampleTest( as, tu, wn, vd), c1 ); n.addTraining(e11);
    ExampleTrain e12( ExampleTest( ap, tu, wd, vd), c1 ); n.addTraining(e12);
    ExampleTrain e13( ExampleTest( ap, tc, wn, vm), c1 ); n.addTraining(e13);
    ExampleTrain e14( ExampleTest( ad, tu, wd, vd), c0 ); n.addTraining(e14);

	ExampleTest et( as, tc, wd, vm);
	BOOST_CHECK( n.getCategory(et) == c0 );
}



BOOST_AUTO_TEST_CASE( populationClasifierTest ) {

	/** wyniki pewnej ankiety;) Atrybuty: wyksztalcenie (podstawowe, srednie, wyzsze),
		wiek(mlody:0-34,sredni:35-59,duzy:60-), plec(k,m).
	*/

    NaiveBayesian n;
    AttrVal c1 = &(*n.insertCategory("1"));
    AttrVal c0 = &(*n.insertCategory("0"));

    AttrDomain& a_wyk = n.insertAttr("wyk");
    AttrVal wp = &(*a_wyk.insert("podstaw"));
    AttrVal ws = &(*a_wyk.insert("srednie"));
    AttrVal ww = &(*a_wyk.insert("wyzsze"));
	AttrDomain& a_wiek = n.insertAttr("wiek");
	AttrVal w1 = &(*a_wiek.insert("mlody"));
	AttrVal w2 = &(*a_wiek.insert("sredni"));
	AttrVal w3 = &(*a_wiek.insert("duzy"));
	AttrDomain& a_plec = n.insertAttr("plec");
	AttrVal k = &(*a_plec.insert("kobieta"));
	AttrVal m = &(*a_plec.insert("mezszczyzna"));

	//funkcja wynikowa to: c1 <=> (wyk = ww) or (wyk = ws) and (wi = w1) or (wyk = wp) and (wi = w1) and (plec = k)
    ExampleTrain e01( ExampleTest( wp, w1, k ), c1 ); n.addTraining(e01);
    ExampleTrain e02( ExampleTest( wp, w1, m ), c0 ); n.addTraining(e02);
    ExampleTrain e03( ExampleTest( wp, w2, k ), c0 ); // n.addTraining(e03);
    ExampleTrain e04( ExampleTest( wp, w2, m ), c0 ); n.addTraining(e04);
    ExampleTrain e05( ExampleTest( wp, w3, k ), c0 ); n.addTraining(e05);
    ExampleTrain e06( ExampleTest( wp, w3, m ), c0 ); n.addTraining(e06);
    ExampleTrain e07( ExampleTest( ws, w1, k ), c1 ); n.addTraining(e07);
	ExampleTrain e08( ExampleTest( ws, w1, m ), c1 ); n.addTraining(e08);
    ExampleTrain e09( ExampleTest( ws, w2, k ), c0 ); n.addTraining(e09);
    ExampleTrain e10( ExampleTest( ws, w2, m ), c0 ); n.addTraining(e10);
    ExampleTrain e11( ExampleTest( ws, w3, k ), c0 ); // n.addTraining(e11);
    ExampleTrain e12( ExampleTest( ws, w3, m ), c0 ); n.addTraining(e12);
    ExampleTrain e13( ExampleTest( ww, w1, k ), c1 ); // n.addTraining(e13);
    ExampleTrain e14( ExampleTest( ww, w1, m ), c1 ); n.addTraining(e14);
    ExampleTrain e15( ExampleTest( ww, w2, k ), c1 ); // n.addTraining(e15);
    ExampleTrain e16( ExampleTest( ww, w2, m ), c1 ); n.addTraining(e16);
    ExampleTrain e17( ExampleTest( ww, w3, k ), c1 ); n.addTraining(e17);
    ExampleTrain e18( ExampleTest( ww, w3, m ), c1 ); n.addTraining(e18);

	BOOST_CHECK( n.getCategory(e01) == c1 );
	BOOST_CHECK( n.getCategory(e02) == c0 );
	BOOST_CHECK( n.getCategory(e03) == c0 );
	BOOST_CHECK( n.getCategory(e04) == c0 );
	BOOST_CHECK( n.getCategory(e05) == c0 );
	BOOST_CHECK( n.getCategory(e06) == c0 );
	BOOST_CHECK( n.getCategory(e07) == c1 );
	BOOST_CHECK( n.getCategory(e08) == c1 );
	BOOST_CHECK( n.getCategory(e09) == c0 );
	BOOST_CHECK( n.getCategory(e10) == c0 );
	BOOST_CHECK( n.getCategory(e11) == c0 );
	BOOST_CHECK( n.getCategory(e12) == c0 );
	BOOST_CHECK( n.getCategory(e13) == c1 );
	BOOST_CHECK( n.getCategory(e14) == c1 );
	BOOST_CHECK( n.getCategory(e15) == c1 );
	BOOST_CHECK( n.getCategory(e16) == c1 );
	BOOST_CHECK( n.getCategory(e17) == c1 );
	BOOST_CHECK( n.getCategory(e18) == c1 );
}

BOOST_AUTO_TEST_SUITE_END()
