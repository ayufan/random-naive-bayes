/** plik zawiera test klas utils */

#if defined(_MSC_VER) && (_MSC_VER >= 1400)
//msvc8.0 generuje warning dla boost::numeric::matrix
#pragma warning(disable:4127)
#pragma warning(disable:4512)
#pragma warning(disable:4996)
//msvc9.0 generuje smieci dla boost/date_time
#pragma warning(disable:4244)
#endif

#include <iostream>

#include <string>
#include <sstream>

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/blas.hpp>
#include <boost/numeric/ublas/io.hpp>

#include <boost/thread.hpp>

#include "../src/utils/Random.h"
#include "../src/utils/GaussEliminator.h"
#include "../src/utils/Power.hpp"


using namespace std;
using namespace faif;
using namespace boost;
using namespace boost::numeric;
using boost::unit_test::test_suite;

BOOST_AUTO_TEST_SUITE( faif_utils_test )

BOOST_AUTO_TEST_CASE( RandomGeneratorTest ) {

    const int NUM = 1000;

	RandomDouble r;
    double s = 0.0;
    for(int i=0;i<NUM;++i) {
		s += r();
    }

	RandomDouble rr;
	double ss = 0.0;
    for(int i=0;i<NUM;++i) {
		ss += rr();
    }

	BOOST_CHECK( s/NUM > 0.4 && s/NUM < 0.6 );
	BOOST_CHECK( ss/NUM > 0.4 && ss/NUM < 0.6 );
	BOOST_CHECK( s != ss );

	RandomDouble u(-1.0,1.0);
	double mu = 0.0;
	for(int i=0;i<NUM;++i)
		mu += u();

	BOOST_CHECK( mu/NUM > -0.1 && mu/NUM < 0.1 );

	const int MAX = 10;
	RandomInt ri(0,MAX);
	for(int j=0;j<NUM;j++) {
		BOOST_CHECK( ri() >= 0 );
		BOOST_CHECK( ri() <= MAX );
	}

	RandomInt rj(0,0);
	for(int j = 0; j < NUM; j++) {
		BOOST_CHECK( rj() == 0 );
	}

}

BOOST_AUTO_TEST_CASE( RandomNormalDistributionTest ) {

    const int NUM = 1000;

	RandomNormal n(0.0, 1.0);
	double s = 0.0;
    for(int i=0;i<NUM;++i) {
		s += n();
		//cout << n() << endl;
    }
// 	cout << s << endl;
// 	cout << s/NUM << endl;
	BOOST_CHECK( s/NUM >= -0.1 && s/NUM <= 0.1 );

}


namespace {
	//watek, ktory korzysta z generatora liczb losowych
	class ThreadRand {
	public:
		ThreadRand(int num) : num_(num) { }
		//bada kolejne liczby calkowite i zlicza liczby pierwsze.
		void operator()() {
			for(int i=0;i<num_;++i)
				rand_();
		}
	private:
		int num_;
		RandomDouble rand_;
	};
}

BOOST_AUTO_TEST_CASE( RandomMultithreadTest ) {
	const int NUM = 10000;
	const int NUM_THREADS = 10;
	ThreadRand r(NUM);
    boost::thread_group threads;
    for (int i = 0; i < NUM_THREADS; ++i)
        threads.create_thread(r);
    threads.join_all();
}

BOOST_AUTO_TEST_CASE( GaussEliminatorTest ) {

	const int n1 = 20;
	ublas::matrix<double> m1(n1,n1);
	m1 = ublas::identity_matrix<double>(n1);
	ublas::vector<double> y1(n1);
	for(int i = 0; i < n1; ++i )
		y1(i) = i;
	ublas::vector<double> res1 = GaussEliminator(m1,y1);
	for(int i = 0; i < n1; ++i )
		BOOST_CHECK_CLOSE( y1(i), res1(i), 0.1 );

	const int n = 3;
    ublas::matrix<double> m(n,n);
    m(0, 0) = 2; m(0, 1) = 1; m(0, 2) = 3;
    m(1, 0) = 1; m(1, 1) = 3; m(1, 2) = 4;
    m(2, 0) = 3; m(2, 1) = 4; m(2, 2) = 5;
    ublas::vector<double> x(n);
    x(0) = 4;
    x(1) = 5;
    x(2) = 7;
	//nie niszczy m oraz x (ale wolniejsze, bo robi kopie)
	ublas::vector<double> y = GaussEliminator(m,x);

	BOOST_CHECK_CLOSE( y(0), 0.4, 0.1 );
	BOOST_CHECK_CLOSE( y(1), 0.2, 0.1 );
	BOOST_CHECK_CLOSE( y(2), 1.0, 0.1 );
	//niszczy macierz m oraz x
	ublas::vector<double> yy = GaussEliminatorRef(m,x);

	BOOST_CHECK_CLOSE( yy(0), 0.4, 0.1 );
	BOOST_CHECK_CLOSE( yy(1), 0.2, 0.1 );
	BOOST_CHECK_CLOSE( yy(2), 1.0, 0.1 );

}

BOOST_AUTO_TEST_CASE( IntPowerTest ) {

	BOOST_CHECK_CLOSE( int_power<3>(3.0), 27.0, 0.1 );
	BOOST_CHECK_CLOSE( int_power<4>(3.0), 81.0, 0.1 );
	BOOST_CHECK_CLOSE( int_power<1000>(1.1), 2.47e+41, 0.1 );
}

BOOST_AUTO_TEST_SUITE_END()
