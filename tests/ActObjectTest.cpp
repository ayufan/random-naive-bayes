/** plik zawiera test klas utils */

#if defined(_MSC_VER) && (_MSC_VER >= 1400)
//msvc8.0 generuje warning dla boost::numeric::matrix
#pragma warning(disable:4127)
#pragma warning(disable:4512)
#pragma warning(disable:4996)
//msvc9.0 generuje smieci dla boost/date_time
#pragma warning(disable:4244)
#endif

#include <iostream>
#include <string>
#include <sstream>

#include <boost/bind.hpp>

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include <boost/thread/xtime.hpp>

#include "../src/utils/actobj/Scheduler.h"
#include "../src/utils/actobj/OstreamCommandObserver.h"
#include "../src/utils/actobj/CommandHistory.h"

using namespace std;
using namespace faif;
using namespace faif::actobj;
using namespace boost;
using boost::unit_test::test_suite;

BOOST_AUTO_TEST_SUITE( Faif_active_object_test )

namespace {

    struct DummyCmd : public Command {

        DummyCmd() {}
        virtual ~DummyCmd() {}

        virtual void operator()(Progress& progress) {
            progress.setProgress(0.1);
            progress.step();
            progress.step();
            progress.setProgress(0.9);
        }

    };

} //namespace


BOOST_AUTO_TEST_CASE( MonitorTest) {

    DummyCmd cmd;

    ostringstream os;
    OstreamCommandObserver obs(os);
    obs.notifyStep(cmd);
    obs.notifyProgress(cmd, 0.5);

    BOOST_CHECK( os.str() == string(".50%\n") );
}

BOOST_AUTO_TEST_CASE( ProgressTest) {

    DummyCmd cmd;
    ostringstream os;

    Progress progress(cmd);
    progress.attach( PCommandObserver(new OstreamCommandObserver(os) ) );

    progress.setProgress(0.1);
    progress.step();

    BOOST_CHECK( os.str() == string("10%\n.") );

    Progress progress2(progress,0.2,0.7);
    progress2.setProgress(0);
    progress2.setProgress(0.5);
    progress2.setProgress(0.99);

    BOOST_CHECK( os.str() == string("10%\n.20%\n44%\n69%\n") );
}

BOOST_AUTO_TEST_CASE( CommandTest ) {

    DummyCmd cmd;
    ostringstream os;
    cmd.attach( PCommandObserver(new OstreamCommandObserver(os) ) );

    cmd.execute();
    BOOST_CHECK( os.str() == string("10%\n..90%\n") );
}

BOOST_AUTO_TEST_CASE( CommandExecTest ) {

    PCommand cmd( new DummyCmd);
    ostringstream os;
    cmd->attach( PCommandObserver(new OstreamCommandObserver(os) ) );

    Scheduler& scheduler = Scheduler::getInstance();
    scheduler.executeSynchronously(cmd);
    BOOST_CHECK( os.str() == string("10%\n..90%\n") );

    scheduler.executeAsynchronously(cmd);
    boost::this_thread::sleep(boost::posix_time::millisec(100)); //sleep 0.1 s
    BOOST_CHECK( os.str() == string("10%\n..90%\n10%\n..90%\n") );
}

namespace {

    struct TickCommand : public faif::actobj::Command {
        TickCommand() {}
        virtual ~TickCommand() {}

        virtual void operator()(Progress& progress) {
            int STEPS = 50;
            for(int i=0;i<STEPS;++i) {
                boost::this_thread::sleep(boost::posix_time::millisec(20)); //sleep 0.02 s
                progress.setProgress(i/(double)STEPS);
            }
        }
    };
}

BOOST_AUTO_TEST_CASE( CommandStatesTest ) {

    Scheduler& scheduler = Scheduler::getInstance();

    PCommand cmd(new TickCommand );
    BOOST_CHECK_EQUAL(cmd->getDescriptor().state_, CommandDesc::NONE);
    scheduler.executeAsynchronously( cmd );
    for(int i = 0; getState(*cmd) == CommandDesc::QUEUED; ++i) {
        if(i > 50) BOOST_FAIL("Error: Command QUEUED for 5 s");
        boost::this_thread::sleep(boost::posix_time::millisec(100)); //sleep 0.1 s
    }
    for(int i=0; getState(*cmd) == CommandDesc::PENDING; ++i) {
        cmd->halt();
        if( i > 50) BOOST_FAIL("Error: Can not halt the command - tried for 5 s");
        boost::this_thread::sleep(boost::posix_time::millisec(100)); //sleep 0.1 s
    }
    BOOST_CHECK_EQUAL(getState(*cmd), CommandDesc::INTERRUPTED);

    PCommand cmd2(new TickCommand );
    scheduler.executeSynchronously( cmd2 );
    BOOST_CHECK_EQUAL( getState(*cmd2), CommandDesc::DONE );
}

namespace {
    /** abstract class of observer object */
    struct TestObserver : public CommandObserver {

        ostream& os_;
        map<CommandDesc::State,bool> achievedStates_;

        TestObserver(std::ostream& os) : os_(os) {
            achievedStates_[CommandDesc::NONE] = false;
            achievedStates_[CommandDesc::QUEUED] = false;
            achievedStates_[CommandDesc::PENDING] = false;
            achievedStates_[CommandDesc::INTERRUPTED] = false;
            achievedStates_[CommandDesc::EXCEPTION] = false;
            achievedStates_[CommandDesc::DONE] = false;
        }

        virtual void notifyProgress(const Command&, double) {}
        virtual void notifyStep(const Command&) {}
        virtual void notifyState(const Command&, CommandDesc::State s) {
            achievedStates_[s] = true;
        }
    };

} //namespace


BOOST_AUTO_TEST_CASE( CommandStatesForSynchroTest ) {

    Scheduler& scheduler = Scheduler::getInstance();

    TestObserver* obs = new TestObserver(cout);
    //  PCommandObserver obs(test);

    PCommand cmd(new TickCommand );
    cmd->attach( PCommandObserver(obs) ); //tworzy sprytny wskaznik wiec pozniej zwolni obiekt
    scheduler.executeSynchronously( cmd );

    BOOST_CHECK( obs->achievedStates_[CommandDesc::NONE] == false );
    BOOST_CHECK( obs->achievedStates_[CommandDesc::QUEUED] == false );
    BOOST_CHECK( obs->achievedStates_[CommandDesc::PENDING] == true );
    BOOST_CHECK( obs->achievedStates_[CommandDesc::INTERRUPTED] == false );
    BOOST_CHECK( obs->achievedStates_[CommandDesc::EXCEPTION] == false );
    BOOST_CHECK( obs->achievedStates_[CommandDesc::DONE] == true );

}

BOOST_AUTO_TEST_CASE( CommandStatesForAsynchroTest ) {

    Scheduler& scheduler = Scheduler::getInstance();

    TestObserver* obs = new TestObserver(cout);
    //  PCommandObserver obs(test);

    PCommand cmd(new TickCommand );
    cmd->attach( PCommandObserver(obs) ); //the smart ptr is the owner of 'obs'
    scheduler.executeAsynchronously( cmd );

    boost::this_thread::sleep(boost::posix_time::millisec(2000)); //sleep 2 s (comand executes 1s)

    BOOST_CHECK( obs->achievedStates_[CommandDesc::NONE] == false );
    BOOST_CHECK( obs->achievedStates_[CommandDesc::QUEUED] == true );
    BOOST_CHECK( obs->achievedStates_[CommandDesc::PENDING] == true );
    BOOST_CHECK( obs->achievedStates_[CommandDesc::INTERRUPTED] == false );
    BOOST_CHECK( obs->achievedStates_[CommandDesc::EXCEPTION] == false );
    BOOST_CHECK( obs->achievedStates_[CommandDesc::DONE] == true );
}

BOOST_AUTO_TEST_CASE( CommandStatesForAsynchroBreakTest ) {

    Scheduler& scheduler = Scheduler::getInstance();

    TestObserver* obs = new TestObserver(cout);
    //  PCommandObserver obs(test);

    PCommand cmd(new TickCommand );
    cmd->attach( PCommandObserver(obs) ); //tworzy sprytny wskaznik wiec pozniej zwolni obiekt
    scheduler.executeAsynchronously( cmd );
    cmd->halt();

    boost::this_thread::sleep(boost::posix_time::millisec(500)); //sleep 0.5 s

    BOOST_CHECK( obs->achievedStates_[CommandDesc::NONE] == false );
    BOOST_CHECK( obs->achievedStates_[CommandDesc::QUEUED] == true );
    BOOST_CHECK( obs->achievedStates_[CommandDesc::PENDING] == true );
    BOOST_CHECK( obs->achievedStates_[CommandDesc::INTERRUPTED] == true );
    BOOST_CHECK( obs->achievedStates_[CommandDesc::EXCEPTION] == false );
    BOOST_CHECK( obs->achievedStates_[CommandDesc::DONE] == false );

}

namespace {

    struct FinishObserver : public CommandObserver {

        FinishObserver() :done(false) {}

        virtual void notifyProgress(const Command&, double) {}
        virtual void notifyStep(const Command&) {}
        virtual void notifyState(const Command&, CommandDesc::State s) {
            if(s == CommandDesc::DONE) {
                mutex::scoped_lock lock(mut);
                done = true;
                cond.notify_one();
            }
        }

        mutex mut;
        condition_variable cond;
        bool done;
    };

}

BOOST_AUTO_TEST_CASE( CommandIdTest ) {
    Scheduler& scheduler = Scheduler::getInstance();

    PCommand cmd( new DummyCmd);

    BOOST_CHECK_EQUAL( cmd->getDescriptor().id_, 0L );

    CommandID cmd_id1 = scheduler.executeSynchronously( cmd );

    BOOST_CHECK( cmd_id1 != 0L );
    BOOST_CHECK_EQUAL( cmd_id1, cmd->getDescriptor().id_ );


    PCommand cmd2( new DummyCmd );
    FinishObserver* finish = new FinishObserver;
    cmd2->attach( PCommandObserver( finish ) );

    CommandID cmd_id2 = scheduler.executeAsynchronously( cmd2 );

    //czeka na wykonanie danej komendy
    {
        mutex::scoped_lock lock(finish->mut);
        while( ! finish->done ) {
            finish->cond.wait( lock );
        }
    }

    BOOST_CHECK( cmd_id2 != 0L );
    BOOST_CHECK( cmd_id2 != cmd_id1 );
}

BOOST_AUTO_TEST_CASE( ExecuteAsynchronouslyAndWaitTest ) {

    Scheduler& scheduler = Scheduler::getInstance();

    int CMD_NUM = 8;
    vector<PCommand> v;
    for(int i=0;i<CMD_NUM;++i) v.push_back( new TickCommand );
    PCommand last_cmd(new DummyCmd );

    //wykonuje CMD_NUM dlugich komend
    for_each( v.begin(), v.end(), bind( &Scheduler::executeAsynchronously, ref(scheduler), _1 ) );
    //wykonuje jedna krotka (ale na koncu) i czeka na jej zakonczenie;
    scheduler.executeAsynchronouslyAndWait( last_cmd );

    BOOST_REQUIRE( CMD_NUM > 4 );
    BOOST_CHECK_EQUAL( getState(*v[0]), CommandDesc::DONE );
    BOOST_CHECK_EQUAL( getState(*v[1]), CommandDesc::DONE );
    BOOST_CHECK_EQUAL( getState(*v[2]), CommandDesc::DONE );
    BOOST_CHECK_EQUAL( getState(*v[3]), CommandDesc::DONE );

    BOOST_CHECK_EQUAL( getState(*last_cmd), CommandDesc::DONE );
}

BOOST_AUTO_TEST_CASE( CommandHistoryTest ) {

    CommandHistory history;
    //long command
    CommandID id1 = executeAsynchronouslyAndRemember(history, PCommand(new TickCommand) );
    //short command
    CommandID id2 = executeSynchronouslyAndRemember(history, PCommand(new DummyCmd) );
    //short command
    CommandID id3 = Scheduler::getInstance().executeAsynchronouslyAndWait( new DummyCmd );

    CommandDesc::State state1 = history.find(id1)->getDescriptor().state_;

    BOOST_CHECK( state1 == CommandDesc::QUEUED || state1 == CommandDesc::PENDING );
    BOOST_CHECK_EQUAL( findCommandDescriptor(history, id2).state_, CommandDesc::DONE );
    BOOST_CHECK_EQUAL( findCommandDescriptor(history, id3).state_, CommandDesc::NONE );

    boost::this_thread::sleep(boost::posix_time::seconds(2)); //po 2 sekundach powinna sie skonczyc

    BOOST_CHECK_EQUAL( findCommandDescriptor(history, id1).state_, CommandDesc::DONE );

}

BOOST_AUTO_TEST_CASE( CommandHolderTest ) {

	CommandHolder<TickCommand> obs(new TickCommand);
	Scheduler::getInstance().executeAsynchronouslyAndWait( obs.get() );

	//return the full type of command
	TickCommand* c = obs.getObsCmd();

	BOOST_CHECK( c != 0L );

	BOOST_CHECK_EQUAL( getState( *obs.get() ), CommandDesc::DONE );
}

BOOST_AUTO_TEST_CASE( SchedulerFinishAllTest ) {

    //po tym tescie nie mozna niczego juz wykonywac!

    Scheduler& scheduler = Scheduler::getInstance();

    int CMD_NUM = 20;
    vector<PCommand> v;
    for(int i=0;i<CMD_NUM;++i) v.push_back( new TickCommand );

    //wykonuje CMD_NUM dlugich komend
    for_each( v.begin(), v.end(), bind( &Scheduler::executeAsynchronously, ref(scheduler), _1 ) );

    boost::this_thread::sleep(boost::posix_time::seconds(2)); //sleep 2 s

    //break execution of som commands
    scheduler.finishAll();

    BOOST_REQUIRE( CMD_NUM > 10 );

    BOOST_CHECK_EQUAL( getState(*v[0]), CommandDesc::DONE );
    BOOST_CHECK_EQUAL( getState(*v[1]), CommandDesc::DONE );
    BOOST_CHECK_EQUAL( getState(*v[2]), CommandDesc::DONE );
    BOOST_CHECK_EQUAL( getState(*v[3]), CommandDesc::DONE );

    BOOST_CHECK_EQUAL( getState(*v[CMD_NUM-4]), CommandDesc::QUEUED );
    BOOST_CHECK_EQUAL( getState(*v[CMD_NUM-3]), CommandDesc::QUEUED );
    BOOST_CHECK_EQUAL( getState(*v[CMD_NUM-2]), CommandDesc::QUEUED );
    BOOST_CHECK_EQUAL( getState(*v[CMD_NUM-1]), CommandDesc::QUEUED );
}

BOOST_AUTO_TEST_SUITE_END()
