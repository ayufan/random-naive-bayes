// plik zawiera test klas - prymitywow dla biblioteki faif

#if defined(_MSC_VER) && (_MSC_VER >= 1400)
//msvc9.0 generuje smieci dla boost/date_time
#pragma warning(disable:4244)
#endif

#include <iostream>

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <boost/lexical_cast.hpp>

#include "../src/Version.h"
#include "../src/ExceptionsFaif.h"
#include "../src/Attribute.h"
#include "../src/Domain.h"

using namespace std;
using namespace boost;
using boost::unit_test::test_suite;

using namespace faif;

BOOST_AUTO_TEST_SUITE( faif_primitives_test )

BOOST_AUTO_TEST_CASE( TestVersion )
{
//call the library version functions
	BOOST_CHECK( getVersionMajor() >= 0 );

	BOOST_CHECK( getVersionMinor() >= 0 );

	BOOST_CHECK( getVersionCompilation() > 0 );

}

BOOST_AUTO_TEST_CASE( TestAttribInt)
{

	typedef AttrNominal<int> Attr;
	/** without domain */
	Attr x(1, 0L), y(2, 0L);

	BOOST_CHECK_EQUAL( x.get(), 1 );
	BOOST_CHECK_EQUAL( y.get(), 2 );

	BOOST_CHECK( x != y );
	BOOST_CHECK( x == Attr(1, 0L) );

	Attr::DomainType d1("a");
	Attr& a1 = *d1.insert( 1 );
	Attr& a2 = *d1.insert( 2 );
	BOOST_CHECK_EQUAL( d1.getSize(), 2 );

	BOOST_CHECK( a1 != a2 );
	BOOST_CHECK( a2 == findRef( d1, 2 ) );

	BOOST_CHECK_THROW( findRef(d1, 555), NotFoundException );

	Attr::DomainType d2(d1);
	BOOST_CHECK_EQUAL( d2.getSize(), 2 );

 	Attr& a3 = *d2.find( 1 );

 	BOOST_CHECK( a1 != a3 );

    BOOST_CHECK_EQUAL( lexical_cast<string>(a1), string("a=1") );
	BOOST_CHECK_EQUAL( lexical_cast<string>(d2), string("a=1,a=2,;") );

	d2.remove( 1 );
	Attr::DomainType::iterator it = d2.find(1);
	BOOST_CHECK( it == d2.end() );


}

BOOST_AUTO_TEST_CASE( TestAttribString )
{
	typedef AttrNominalString Attr;

	Attr::DomainType d1("a");
	Attr& a1 = *d1.insert( "x" );
	d1.insert( "y" );

	Attr::DomainType d2("b");
 	Attr& a3 = *d2.insert( "z" );

	BOOST_CHECK( ! (a1 == a3) );
	BOOST_CHECK( a1 != a3 );

	d2 = d1;
	BOOST_CHECK_EQUAL( d2.getSize(), 2 );

	BOOST_CHECK_EQUAL( lexical_cast<string>(d2), string("a=x,a=y,;") );

	d2.remove( "z" );
	Attr::DomainType::iterator it = d2.find("x");
	BOOST_CHECK( it != d2.end() );
	d2.erase( it );
	Attr::DomainType::iterator it2 = d2.find("x");
	BOOST_CHECK( it2 == d2.end() );
}

BOOST_AUTO_TEST_SUITE_END()
