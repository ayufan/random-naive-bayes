/** plik zawiera test walidatora */

#if defined(_MSC_VER)
/** wkurzajacy warning na vc++8, ze std::copy deprecated */
#pragma warning(disable:4996)
//warning msvc dla boost::random
#pragma warning(disable:4512)
#endif

#include <cmath>
#include <ctime>
#include <algorithm>
#include <boost/random.hpp>
#include <boost/lambda/lambda.hpp>

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "../src/learning/ExampleTest.h"
#include "../src/learning/ExampleTrain.h"
#include "../src/learning/Classifier.h"
#include "../src/learning/Validator.h"

using namespace std;
using namespace faif;
using namespace faif::ml;
using boost::unit_test::test_suite;

BOOST_AUTO_TEST_SUITE( validator_test )

BOOST_AUTO_TEST_CASE( exampleTest ) {

	AttrDomain a("x");
	AttrVal va1 = &(*a.insert("A") );
	AttrVal va2 = &(*a.insert("B") );
	AttrVal va3 = &(*a.insert("C") );

    ExampleTest exx1;
    BOOST_CHECK( exx1.size() == 0 );
    exx1.push_back(va1); exx1.push_back(va2); exx1.push_back(va3);
    BOOST_CHECK( exx1.size() == 3 );
    ExampleTest exx2 (exx1.get());
    BOOST_CHECK( exx1 == exx2 );
    exx2.push_back(va1); exx2.push_back(va1);
    BOOST_CHECK( exx2.size() == 5 );
    BOOST_CHECK( exx1 != exx2 );
    ExampleTest exx3 = exx1;
    BOOST_CHECK( exx1 == exx3 );
    exx3 = exx2;
    BOOST_CHECK( exx1 != exx3 );
    BOOST_CHECK( exx2 == exx3 );

    stringstream ss;
    ss << exx1;
    BOOST_CHECK( ss.str() == std::string("x=A x=B x=C ") );

    ExampleTrain ex1;
    BOOST_CHECK( ex1.size() == 0 );
    BOOST_CHECK( ex1.getCategory() == 0L );

    ExampleTrain ex2(exx1, va3);
    BOOST_CHECK( ex2.size() == 3 );
    BOOST_CHECK( ex2.getCategory() == va3 );
    BOOST_CHECK( ex2 == exx1 );

    ExampleTrain ex3(ex2);
    BOOST_CHECK( ex3 == ex2 );
    BOOST_CHECK( ex3 != ex1 );

    ex3 = ex1;
    BOOST_CHECK( ex3 != ex2 );
    BOOST_CHECK( ex3 == ex1 );

    stringstream st;
    st << ex2;
    BOOST_CHECK( st.str() == std::string("x=A x=B x=C : x=C") );
}

BOOST_AUTO_TEST_CASE( exampleShortConstructTest ) {
	AttrDomain a("x");

    AttrVal va1 = &(* a.insert("A") );

    ExampleTest ex1(va1);
    BOOST_CHECK( ex1.size() == 1 );
    ExampleTest ex2(va1, va1);
    BOOST_CHECK( ex2.size() == 2 );
    ExampleTest ex3(va1, va1, va1);
    BOOST_CHECK( ex3.size() == 3 );
    ExampleTest ex4(va1, va1, va1, va1);
    BOOST_CHECK( ex4.size() == 4 );
    ExampleTest ex5(va1, va1, va1, va1, va1 );
    BOOST_CHECK( ex5.size() == 5 );

    ExampleTrain e1( ExampleTest(va1, va1, va1), va1 );
    BOOST_CHECK( e1.getCategory() == va1 );
    BOOST_CHECK( e1.size() == 3 );
}

typedef boost::variate_generator<boost::mt19937&, boost::uniform_int<> > BoostRandom;

/**  testy generatora liczb losowych ( z boost-a)  */
BOOST_AUTO_TEST_CASE( boostRandomGeneratorTest ) {

    const int MIN_DIE = 1;
    const int MAX_DIE = 6;
    int tab[MAX_DIE+1] = { 0, 0, 0, 0, 0, 0, 0 };
    const int NUM_GEN = 120;
    const int AVG = NUM_GEN / MAX_DIE;
    const int D = (int)sqrt( ( NUM_GEN * (MAX_DIE - 1)) / (double)(MAX_DIE * MAX_DIE ));

    boost::mt19937 rng;                 // produces randomness out of thin air
    rng.seed( (boost::uint32_t)time( 0L ) );
    boost::uniform_int<> six(MIN_DIE, MAX_DIE);       // distribution that maps to 1..6
    BoostRandom die(rng, six); // glues randomness with mapping
    for(int i = 0; i < NUM_GEN; i++ ) {
        int x = die();
        BOOST_CHECK( x >= MIN_DIE && x <= MAX_DIE );
        tab[x]++;
    }
    for(int j = MIN_DIE; j<=MAX_DIE; j++) {
        BOOST_CHECK( tab[j] > 0 && tab[j] > AVG - 3*D && tab[j] < AVG + 3*D );
    }
}

namespace {

    /** pomocniczy funktor */
    struct ShuffleFunctor {
        BoostRandom& r_;
        ShuffleFunctor(BoostRandom& r)
            : r_(r) {
        }
        int operator()(int){ return r_(); }
    private:
        ShuffleFunctor& operator=(const ShuffleFunctor&); //zabronione przypisanie bo skladowe referencyjne
    };
}

BOOST_AUTO_TEST_CASE( stdRandomShuffleTest ) {

    int TAB[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    const int TAB_SIZE = sizeof(TAB)/sizeof(TAB[0]);

    boost::mt19937 rng;
    rng.seed( (boost::uint32_t)time( 0L ) );
    boost::uniform_int<> mapping(0, TAB_SIZE - 1);
    BoostRandom gen( rng, mapping );
    ShuffleFunctor shuffleFunctor( gen );

    int tab[TAB_SIZE];
    copy(TAB, TAB + TAB_SIZE, tab );
    BOOST_CHECK( equal( TAB, TAB + TAB_SIZE, tab ) );
    random_shuffle( tab, tab + TAB_SIZE, shuffleFunctor );
    BOOST_CHECK( ! equal( TAB, TAB + TAB_SIZE, tab ) );

    int tab2[TAB_SIZE];
    copy(TAB, TAB + TAB_SIZE, tab2 );
    BOOST_CHECK( equal( TAB, TAB + TAB_SIZE, tab2 ) );
    random_shuffle( tab2, tab2 + TAB_SIZE, shuffleFunctor );
    BOOST_CHECK( ! equal( TAB, TAB + TAB_SIZE, tab2 ) );
    BOOST_CHECK( ! equal( tab, tab + TAB_SIZE, tab2 ) );
}


BOOST_AUTO_TEST_CASE( stdCreatePointerTest ) {

    int TAB[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    const int TAB_SIZE = sizeof(TAB)/sizeof(TAB[0]);

    int* tab[TAB_SIZE];
    transform( TAB, TAB + TAB_SIZE, tab, & boost::lambda::_1 );

    stringstream ss;
    std::transform(tab, tab + TAB_SIZE, std::ostream_iterator<int>(ss," "), * boost::lambda::_1 );
    BOOST_CHECK( ss.str() == "1 2 3 4 5 6 7 8 9 " );
}

namespace {

    /** klasa klasyfikatora, do sprawdzanie metod interfejsu.
        Zlicza liczbe przykladow trenujacych.
    */
    class DummyCountClassifier
        : public Classifier {

    public:
        DummyCountClassifier()
            : count_(0) {
        }

        /** metoda zmienia stan klasyfikatora na taki, jaki byl przed nauczeniem sie czegokolwiek */
        virtual void reset() { count_ = 0; }

        /** metoda dodaje przyklad trenujacy (uczy) */
        virtual void addTraining(const ExampleTrain&) { count_++; }

        /** implementuje, bo musi (metoda abstrakcyjna u rodzica */
        virtual AttrVal getCategory(const ExampleTest&) const { return 0L; }

        // podaje zawartosc licznika
        int getCount() const { return count_; }
    private:
        int count_;
    };
} //namespace

BOOST_AUTO_TEST_CASE( classifierInterfaceTest ) {

    AttrDomain a("x");
    AttrVal va1 = &(*a.insert("A"));
    AttrVal va2 = &(*a.insert("B"));
    a.insert("C");

    ExampleTrain e1( ExampleTest(va1, va1), va1 );
    ExampleTrain e2( ExampleTest(va1, va2), va1 );
    ExampleTrain e3( ExampleTest(va2, va1), va1 );
    ExampleTrain e4( ExampleTest(va2, va2), va2 );

    ExamplesTrain ex;
    ex.push_back(e1);
    ex.push_back(e2);
    ex.push_back(e3);
    ex.push_back(e4);

    DummyCountClassifier classifier;
    BOOST_CHECK( classifier.getCount() == 0 );
    classifier.addTraining(e1);
    BOOST_CHECK( classifier.getCount() == 1 );
    classifier.addTrainingCollection(ex);
    BOOST_CHECK( classifier.getCount() == static_cast<int>(ex.size()) + 1 );
    classifier.reset();
    BOOST_CHECK( classifier.getCount() == 0 );
}

namespace {

	/** Klasyfikator do testow walidatora.
		Przy testach dostarcza kategorii, ktora byla kategoria ostatnio dodanego przykladu trenujacego.
	*/
	class DummyClassifier
		: public Classifier {
	public:
		DummyClassifier()
			: cat_(0L) {
		}

		virtual ~DummyClassifier() {}

		virtual void reset(){ cat_ = 0L; }

		/** metoda dodaje przyklad trenujacy (uczy) */
		virtual void addTraining(const ExampleTrain& example) {
			cat_ = example.getCategory();
		}

		/** metoda klasyfikuje podany przyklad */
		virtual AttrVal getCategory(const ExampleTest&) const {
			return cat_;
		}
	private:
		AttrVal cat_;
	};

} //namespace

/** zwraca b. proste przyklady (2 atrybuty, 2 wartosci) przyklady*/
ExamplesTrain getExamplesVerySimple() {

    AttrDomain a("x");
    AttrVal va1 = &(*a.insert("A"));
	AttrVal va2 = &(*a.insert("B"));

    ExampleTrain e1( ExampleTest(va1, va1), va1 );
    ExampleTrain e2( ExampleTest(va1, va2), va1 );
    ExampleTrain e3( ExampleTest(va2, va1), va1 );
    ExampleTrain e4( ExampleTest(va2, va2), va2 );

    ExamplesTrain ex1;
    ex1.push_back(e1);
    ex1.push_back(e2);
    ex1.push_back(e3);
    ex1.push_back(e4);
    return ex1;
}

BOOST_AUTO_TEST_CASE( classifierDummyTest ) {
    ExamplesTrain ex1 = getExamplesVerySimple();
    ExamplesTrain ex2;
    ex2.push_back( ex1.front() );

    DummyClassifier classifier;
    classifier.reset();
    classifier.addTrainingCollection(ex1);
    BOOST_CHECK( checkClassifier(ex1, classifier ) == 1 );
    BOOST_CHECK( checkClassifier(ex2, classifier ) == 0 );

    classifier.reset();
    classifier.addTrainingCollection(ex2);
    BOOST_CHECK( checkClassifier(ex1, classifier ) == 3 );
    BOOST_CHECK( checkClassifier(ex2, classifier ) == 1 );
}

BOOST_AUTO_TEST_CASE( classifierCrossTest ) {
    ExamplesTrain ex1 = getExamplesVerySimple();
    ExamplesTrain ex2;
    ex2.push_back( ex1.front() );

    DummyClassifier classifier;
    CrossValidator valid;

    const int NUM = 100;

    //dla rozwazanego przypadku crossValidator powinien zwracac 0.5 lub 0.75 z tym samym prawdopodobienstwem.
    const int AVG = NUM / 2;
    const int D = (int)sqrt( NUM / 4.0 );

    int val05 = 0;
    int val075 = 0;

    for(int i = 0; i < NUM; i++)
        if(valid.checkCross(ex1, 2, classifier ) > 0.6 )
            val075++;
        else
            val05++;
    BOOST_CHECK( val075 >= AVG - 3*D && val075 <= AVG + 3*D );
    BOOST_CHECK( val05 >= AVG - 3*D && val05 < AVG + 3*D );
}


BOOST_AUTO_TEST_SUITE_END()
