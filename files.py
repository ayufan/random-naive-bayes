class Module:
    path = './src/'
    cpp = [ ]
    head = [ ]
    test = [ ]


#klasy bazowe
pr = Module()
pr.path = ''
src = pr.path
pr.head = [ src+'Version.h', src+'ExceptionsFaif.h', src+'Attribute.h', src+'Domain.h']
pr.cpp = [ src+'Version.cpp', src+'ExceptionsFaif.cpp' ]

#klasy pomocnicze (utils)
utils = Module()
utils.path = 'utils/'
src = utils.path
utils.head = [ src+'Random.h', src+'GaussEliminator.h', src + 'Power.hpp']
utils.cpp = [ src+'Random.cpp' ]

#klasy pomocnicze (command)
actobj = Module()
actobj.path = 'utils/actobj/'
src = actobj.path
actobj.head = [ src+'CommandDesc.h', src+'Progress.h', src+'Command.h', src+'Scheduler.h',
                src+'OstreamCommandObserver.h', src+'CommandHistory.h' ]
actobj.cpp = [ src+'Progress.cpp', src+'Command.cpp', src+'Scheduler.cpp',
               src+'OstreamCommandObserver.cpp', src+'CommandHistory.cpp' ]

#klasy DNA
dna = Module()
dna.path = 'dna/'
src = dna.path
dna.head = [ src+'ExceptionsDna.h', src+'Nucleotide.h', src+'Chain.h', src+'EnergyNucleo.h',
             src+'SecStruct.h',
             #src + 'FoldedMatrix.h', - header usuniety z tej listy, bo nie powinien byc instalowany
             src+'FoldedChain.h', src + 'FoldedPair.h',
             src+'Codon.h', src+'CodonAminoTable.h' ]
dna.cpp = [ src+'Nucleotide.cpp', src+'Chain.cpp', src+'EnergyNucleo.cpp',
            src+'SecStruct.cpp', src + 'FoldedMatrix.cpp', src+'FoldedChain.cpp', src+'FoldedPair.cpp',
            src+'Codon.cpp', src+'CodonAminoTable.cpp' ]

#klasy DNA
hapl = Module()
hapl.path = 'hapl/'
src = hapl.path
hapl.head = [ src+'Loci.h' ]
hapl.cpp = [ src+'Loci.cpp' ]


#klasy timeseries
ts = Module()
ts.path = 'timeseries/'
src = ts.path
ts.head = [ src+'TimeSeries.h', src + 'TimeseriesExceptions.hpp', src + 'Transformations.h', src + 'Predictions.hpp' ]
ts.cpp = [ src+'TimeSeriesDigit.cpp', src+'TimeSeriesReal.cpp', src + 'TimeseriesExceptions.cpp',
           src+'Transformations.cpp', src + 'Predictions.cpp' ]

#klasy learning
learn = Module()
learn.path = 'learning/'
src = learn.path
learn.head = [ src+'ExampleTest.h', src+'ExampleTrain.h', src+'Classifier.h', src+'NaiveBayesian.h',
               src+'RandomNaiveBayesian.h', src + 'Validator.h' ]
learn.cpp = [ src+'ExampleTest.cpp', src+'ExampleTrain.cpp', src+'Classifier.cpp' ]

#klasy walidatora
val = Module()
val.path = 'learning/validator/'
src = val.path
val.head = [ ]
val.cpp =  [ src+'CrossValidator.cpp', src+'CheckClassifier.cpp' ]

#naiwny klasyfikator bayesowski
nbc = Module()
nbc.path = 'learning/nbc/'
src = nbc.path
nbc.head = [ ]
nbc.cpp = [ src+'BayesCategories.cpp', src+'NaiveBayesianRoot.cpp', src+'NaiveBayesianTraining.cpp',
            src+'NaiveBayesianClasify.cpp', src+'NaiveBayesianImpl.cpp', src+'NaiveBayesian.cpp' ]

#losowy klasyfikator bayesowski
rnbc = Module()
rnbc.path = 'learning/rnbc/'
src = rnbc.path
rnbc.head = [ ]
rnbc.cpp = [ src+'RandomNaiveBayesianTraining.cpp', src+'RandomNaiveBayesianClasify.cpp', 
            src+'RandomNaiveBayesianImpl.cpp', src+'RandomNaiveBayesian.cpp' ]

            
#przeszukiwanie
search = Module()
search.path = 'search/'
src = search.path
search.head = [ src+'Node.hpp', src+'Space.hpp',
                src+'TreeNodeImpl.hpp', src+'DepthFirst.h', src+'BreadthFirst.h', src+'UnifiedCost.h', src + 'AStar.h',
                src + 'HillClimbing.hpp', src+'EvolutionaryAlgorithm.hpp', src + 'VectorIndividual.hpp',
                src+'ExpectationMaximization.hpp' ]
search.cpp = [ src + 'VectorIndividual.cpp' ]

modules = [ pr, utils, actobj, dna, hapl, ts, learn, val, nbc, rnbc, search ]
