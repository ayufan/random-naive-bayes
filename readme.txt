FAIF - Fast(Funny) Artificial Intelligence Framework
----------------------------------------------------

This portable C++ library defines basic abstractions (like state, space, attribute etc.)
and implements some of known AI algorithms ( uninformed state space search, heuristic search,
statistical reasoning and symbol based machine learning ).
The FAIF abstractions and algorithms are generic (as STL and boost).

The main goal is education of AI, where simple and portable C++ library is needed.
The library will be used also for broad spectrum of applications
where some artificial intelligence algorithm are needed.



Library components:

1) DNA
   DNA nucleotide, DNA strain,
   DNA secondary structure, Folded DNA strain, Folded DNA pair (two strains) - the Nussinov algorithm,
   DNA Codon, DNA Codon To Amino Conversion
2) timeseries
   TimeSeriesDigit (time as integer, 0 == present),
   TimeSeriesReal (using boost::posix_time),
   linear regression (to create TimeSeriesDigit from TimeSeriesReal)
3) learning
   ExampleTest, ExamleTrain
   Naive Bayes Classifier
   validator, cross-validator
4) utils
   Random - wrapper on boost::random
   Active Object: Command, Scheduler, Progress, OstreamProgressObserver

Implementation:
--------------
C++ ISO99,

Dependencies:
-----------
boost 1.35



